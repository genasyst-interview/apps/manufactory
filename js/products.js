if(typeof $.manufactory !=='object') {
    $.manufactory = {};
}
$.manufactory.Products = {
    Product:{
        details:function (obj, e) {
            var id = $(obj).data('product_id');
            var a_off = $(obj).closest('tr').offset();
            var container = $('body');
            $.get("?module=product&action=details&id="+id,function (data) {
                $('.product-detail').remove();
                $('.product-detail-layer').remove();
                container.append(data);
            });
        },
        delete:function (obj) {
            if(confirm('Вы уверены, что хотите Удалить товар?')) {
                $(obj).attr('href');
                $.get($(obj).attr('href'), function (data) {
                  $('[name="category_id"]').val();
                    window.location.replace('?module=products&category='+ $('[name="category_id"]').val());
                });
            }
            return false;
        }

    },
    getSelectedProducts: function() {
        var product_ids = {};
        $('.product-list').find('.product-select:checked').each(function () {
            var id = $(this).val();
            product_ids[id] = id;
        });
        return product_ids;
    },
    duplicate: function () {
        var data = {};
        data['product_id'] = $.manufactory.Products.getSelectedProducts();
        data['module'] = 'products';
        data['action'] = 'duplicate';
        $.get("?module=products",data,function (response) {
            document.location.reload();
        });
    },
    delete: function () {
        if(confirm('Вы уверены, что хотите удалить товары?')) {
            var data = {};
            data['product_id'] = $.manufactory.Products.getSelectedProducts();
            data['module'] = 'products';
            data['action'] = 'delete';
            $.get("?module=products",data,function (response) {
                var data = response.data;
                for(var k in data) {
                    $('#product-'+data[k]).prev().remove();
                    $('#product-'+data[k]).next().remove();
                    $('#product-'+data[k]).remove();
                }

            });
        }
        return false;
    }
};

$(document).ready(function () {
    $('#actions-menu a').click(function () {
        var action = $(this).closest('li').data('action');
        if($.manufactory.Products.hasOwnProperty(action)) {
           var func =  $.manufactory.Products[action];
            func();
        }
        return false;
    });
    //
    $('.product-delete').click(function () {
        $.manufactory.Products.Product.delete($(this));
        return false;
    });
    $('.add-category').click(function () {
        var id = $(this).closest('a').data('id');

        window.open('?module=products&action=categoryEdit&parent_id='+id);
        return false;
    });
    $('.edit-category').click(function () {
        var id = $(this).closest('a').data('id');
        window.open('?module=products&action=categoryEdit&id='+id);    return false;
    });
  
    $('.toggle').click(function () {
        var id = $(this).data('toggle');
        $('#'+id).toggleClass('hide');
    });

    $('.product-detail-close,.product-detail-layer').live('click', function () {
        $('.product-detail').remove();
        $('.product-detail-layer').remove();
        return false;
    });

    $('.product-list .product-details').click(function (e) {
        $.manufactory.Products.Product.details($(this),e);
        return false;
    });
    $(".product-list .offer_price_input").change(function() {
        var val = $(this).val().replace(',','.');
        var values = val.trim().split("\t");
        for (var k in values) {
            if(values[k].trim()=='') {
                delete(values[k]);
            }
        }
        if(count(values)>1) {
           // av(values);
            var items =  $(this).closest('tr').find('.offer_price_input');
            var item =  $(this);

            i_el = item.get(0);
            var counter = 0;
            var start = false;
            items.each(function(e){
                var current_item = $(this);
                if(!start){
                    var el = current_item.get(0);
                    if(el==i_el) {
                        start = true;
                    }
                }
                if(start && values.hasOwnProperty(counter)) {
                    current_item.val(values[counter]);
                    current_item.change();
                    counter++;
                }
            });
            return false;
        } else {
            if(val != '' && !isNaN(parseFloat(val))) {

                var container = $(this).closest(".offer_price");
                var data = {};
                data["offer_id"] = container.data("offer_id");
                data["price_id"] = container.data("price_id");
                data["price"] = val;
                $.post("?module=product&action=SkuPriceSave", data, function(response) {
                    container.find(".offer_price_value").html(response.data.price);
                    container.find(".offer_price_input").prop("type", "hidden");

                } ,"json");
                return false;
            }
        }

    });
    $(".product-list .offer_price_input").blur(function () {
        var val = $(this).val().replace(',','.');

        if(val != '' && !isNaN(parseFloat(val))) {
            var container = $(this).closest(".offer_price");
            container.find(".offer_price_input").prop("type", "hidden");
            container.find(".offer_price_value").css('display', 'inline');
        }
        return false;
    });
    $(".product-list .offer_price_value").dblclick(function() {
        var container = $(this).closest(".offer_price");

        container.find(".offer_price_input").prop("type","text");
        container.find(".offer_price_value").css('display','none');
        container.find(".offer_price_input").focus();
        return false;
    });
    $(document).on('change','.product-detail select[name="select_sku"]', function () {
        var id = $(this).val();
        $('.product-data-sku').addClass('hide');
        $('.product-data-sku-'+id).removeClass('hide');

    })

});