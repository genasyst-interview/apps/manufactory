function count( mixed_var, mode ) {	// Count elements in an array, or properties in an object
	//
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +	  input by: _argos

	var key, cnt = 0;

	if( mode == 'COUNT_RECURSIVE' ) mode = 1;
	if( mode != 1 ) mode = 0;

	for (key in mixed_var){
		cnt++;
		if( mode==1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object) ){
			cnt += count(mixed_var[key], 1);
		}
	}

	return cnt;
}

function var_dump () {
	var output = '', pad_char = ' ', pad_val = 4, lgth = 0, i = 0, d = this.window.document;
	var getFuncName = function (fn) {
		var name = (/\W*function\s+([\w\$]+)\s*\(/).exec(fn);
		if (!name) {
			return '(Anonymous)';
		}
		return name[1];
	};
	var repeat_char = function (len, pad_char) {
		var str = '';
		for (var i=0; i < len; i++) {
			str += pad_char;
		}
		return str;
	};
	var getScalarVal = function (val) {
		var ret = '';
		if (val === null) {
			ret = 'NULL';
		} else if (typeof val === 'boolean') {
			ret = 'bool(' + val + ')';
		} else if (typeof val === 'string') {
			ret = 'string(' + val.length + ') "' + val + '"';
		} else if (typeof val === 'number') {
			if (parseFloat(val) == parseInt(val, 10)) {
				ret = 'int(' + val + ')';
			} else {
				ret = 'float(' + val + ')';
			}
		} else if (val === undefined) {
			ret = 'UNDEFINED'; // Not PHP behavior, but neither is undefined as value
		}  else if (typeof val === 'function') {
			ret = 'FUNCTION'; // Not PHP behavior, but neither is function as value
			ret = val.toString().split("\n");
			txt = '';
			for(var j in ret) {
				txt += (j !=0 ? thick_pad : '') + ret[j] + "\n";
			}
			ret = txt;
		} else if (val instanceof Date) {
			val = val.toString();
			ret = 'string('+val.length+') "' + val + '"'
		}
		else if(val.nodeName) {
			ret = 'HTMLElement("' + val.nodeName.toLowerCase() + '")';
		}
		return ret;
	};
	var formatArray = function (obj, cur_depth, pad_val, pad_char) {
		var someProp = '';
		if (cur_depth > 0) {
			cur_depth++;
		}
		base_pad = repeat_char(pad_val * (cur_depth - 1), pad_char);
		thick_pad = repeat_char(pad_val * (cur_depth + 1), pad_char);
		var str = '';
		var val = '';
		if (typeof obj === 'object' && obj !== null) {
			if (obj.constructor && getFuncName(obj.constructor) === 'PHPJS_Resource') {
				return obj.var_dump();
			}
			lgth = 0;
			for (someProp in obj) {
				lgth++;
			}
			str += "array(" + lgth + ") {\n";
			for (var key in obj) {
				if (typeof obj[key] === 'object' && obj[key] !== null && !(obj[key] instanceof Date) && !obj[key].nodeName) {
					str += thick_pad + "["+key+"] =>\n" + thick_pad+formatArray(obj[key], cur_depth+1, pad_val, pad_char);
				} else {
					val = getScalarVal(obj[key]);
					str += thick_pad + "["+key+"] =>\n" + thick_pad + val + "\n";
				}
			}
			str += base_pad + "}\n";
		} else {
			str = getScalarVal(obj);
		}
		return str;
	};
	output = formatArray(arguments[0], 0, pad_val, pad_char);
	for ( i=1; i < arguments.length; i++ ) {
		output += '\n' + formatArray(arguments[i], 0, pad_val, pad_char);
	}
	return output;
}
function av(data) {
	alert(var_dump(data));
}
$.manufactory = {
    dialog: null,
    showModalWindow: function(url,content,success,button_val,width,height) {
        var self = this;
        if(!button_val) {
            button_val = 'OK';
        }
        if(!width) {
            width = 400;
        }
        if(!height) {
            height = 200;
        }
        $(content).waDialog({
            'width': width,
            'height': height,
            'buttons': '<input type="submit" class="button green" value="'+button_val+'" /> или <a class="cancel" href="#">отмена</a>',
            'esc':true,
            onSubmit: function (dialog) {
                self.dialog = dialog;
                $.post(url, $(this).serialize(),success, 'json');
                //dialog.trigger('close');
                return false;
            },
            'onClose': function(e){
                $(this).remove();
            }
        });
    }
};
$(document).ready(function () {
	$('.toggle').click(function () {
		var id = $(this).data('toggle');
        $('#'+id).toggleClass('hide');
	});
});