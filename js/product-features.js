if(typeof $.manufactory !=='object') {
    $.manufactory = {};
}
$.manufactory.Features = {
    Feature: {
        types:{},
        type:'',
        groups_values:{},
        deafult_unit: '',
        getTypeUnits:function(type) {
            return this.types[type]['units'];
        },
        changeType: function (obj) {
            var type = $(obj).val();
            $(obj).closest('table').find('[name=unit]').html(this.unitsSelect());
        },
        getGroupsSelect: function(){
            var html ='';
            for(var k in this.groups_values) {
                html +='<option value="'+k+'">'+this.groups_values[k]["name"]+'</option>';
            }
            return html;
        },
        unitsSelect: function() {
            var html ='';
           var type =  $('[name=type]').val();
            var units = this.getTypeUnits(type);
            for(var k in units) {
                html +='<option value="'+k+'" '+((k==this.deafult_unit)?" selected":"")+'>'+units[k]["name"]+'</option>';
            }
            return html;
        },
        addValue: function(obj){
            var rand = function() {
                return Math.random().toString(36).substr(2); // remove `0.`
            };
            var token = function() {
                return rand() + rand(); // to make it longer
            };
            var md5 = token().substr(3);
            md5 = 'new'+md5;
            var html = '<tr class="feature-value">' +
                '<td><input name="values['+md5+'][name]" value=""></td>' +
            ' <td><input name="values['+md5+'][value]" value=""></td>' +
                ' <td><select name="values['+md5+'][unit]">'+this.unitsSelect()+'</select></td>' +
                ' <td><select name="values['+md5+'][group_id]">'+this.getGroupsSelect()+'</select></td>' +
                ' <td><input name="values['+md5+'][sort]" value="0"></td>' +
            '<td><a href="#" class="feature-value-delete"><i class="icon16 delete"></i></a>' +
            ' </td>' +
            ' </tr>';
            $(obj).closest('tr').before(html);
        },
        deleteValue: function(obj){
            var value = $(obj).closest('tr.feature-value');
            var id = value.data('id');
            if(!isNaN(parseInt(id))) {

            } else {
                value.detach();
            }
        },
        save:function(obj) {
            var form = $(obj);
            $.post(form.attr('action'), form.serialize(), function(response) {
              if(response.status=='ok') {
                  var id = response.data.id;
                  window.location.href ='?module=settings&action=productFeatureEdit&id='+id;
              }

            } ,"json");

        }
    }


};

$(document).ready( function () {
    $('.feature-value-add').click(function () {
        $.manufactory.Features.Feature.addValue($(this));
        return false;
    });
    $('[name=unit]').change(function () {
        $.manufactory.Features.Feature.deafult_unit = $(this).val();
    });
    $('[name=type]').change(function () {
        $.manufactory.Features.Feature.changeType($(this));
    });
    $('.feature-save').submit(function(){
        $.manufactory.Features.Feature.save($(this));
        return false;
    });
    $(document).off('click','.feature-value-delete').on('click','.feature-value-delete',function () {

        $.manufactory.Features.Feature.deleteValue($(this));
        return false;
    });
});