if(typeof $.manufactory !=='object') {
    $.manufactory = {};
}
$.manufactory.Prices = {
    Price:{
        updatePrices:function (obj) {
           var id = $(obj).data('id');
            $.post("?module=marginProduct&action=updatePrice", {'id':id}, function(response) {
                if(response.status=='ok') {
                    $(obj).detach();
                } else {
                    alert(response.errors.join(','));
                }
            } ,"json");
        }
    }
};

$(document).ready(function () {
    $('.price-update-prices').click(function () {
        $.manufactory.Prices.Price.updatePrices($(this));
    });

});