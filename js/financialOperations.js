if(typeof $.manufactory !=='object') {
    $.manufactory = {};
}
$.manufactory.financialOperations = {
   categories: {},
    action: '',// cash , cashless
    addOperationHTML:function(block, operation){
        var html = '<div class="operation operation-'+operation.id+'" data-id="'+operation.id+'" ></div>';
        block.append(html);
        this.setOperation(operation);
    },
    setOperation: function(operation) {
        var html =  '<span class="operation-info"><i class="icon16 info"></i></span>' +
            '<span class="operation-date">'+operation.create_date+'</span>' +
            '<span class="operation-name">'+operation.name+'</span>' +
            '<span class="operation-amount">'+operation.amount+'</span>';
        $('.operation-'+operation.id).html(html);
    },
    setAction: function(action) {
        this.action = action;
    },
    setCategories:function(type, categories){
        this.categories[type] = categories;
    },
    setBalances: function(data) {
        $('#all').find('.amount').html(data.all+' руб.');
        $('#income').find('.amount').html(data.income+' руб.');
        $('#expense').find('.amount').html(data.expense+' руб.');
        this.setCategoriesBalances(data.categories);
    },
    setCategoriesBalances:function(data) {
        for(var k in data) {
            $('#category-'+k).children('.category-name').find('.amount:first-child').html(data[k]+' руб.');
        }
    },
    setContactsData:function(data) {
        for(var k in data) {
            var contact = data[k];
            var contact_block = $('#contact-'+k);
            contact_block.find('.amount').html(contact.amount+' руб.');
            var contact_detail_block = contact_block.find('#contact-detail-'+k);
            contact_detail_block.html('');
            for(var ok in contact.operations) {
                this.addOperationHTML(contact_detail_block,contact.operations[ok]);
            }
        }
    },
    addCategory:function (obj) {
        var block = $(obj).closest('.block');
        var post_data = {};
            post_data['financial_operations_type'] = block.data('type');
            post_data['financial_operations_payment_type'] = this.action;
        var parent_category_id =  $(obj).data('parent_category_id');
        var category_type_html ='';
        if(parent_category_id) {
             category_type_html = '<input type="hidden" name="type" value="category">';
        } else {
            parent_category_id = 0;
             category_type_html = '<div class="field">' +
              '<div class="name">Тип категории</div>'+
              '<div class="value">' +
              '<select name="type">' +
              '<option value="category">Категория</option> ' +
              '<option value="group">Группа категорий</option> ' +
              '</select> ' +
              '</div>' +
              '</div>' ;
        }

        var html = '<div class="fields">' +
                '<h1>Добавление категории</h1>'+
            '<div class="field-group">' +
                '<div class="field">' +
                    '<div class="name">Введите имя категории</div>' +
                    '<div class="value">' +
                    '<input type="text" name="name" value="">'+
                    '<input type="hidden" name="parent_category_id" value="'+parent_category_id+'">' +
                    '<input type="hidden" name="active" value="1">' +
                    '<input type="hidden" name="financial_operations_type" value="'+post_data['financial_operations_type']+'">' +
                    '<input type="hidden" name="financial_operations_payment_type" value="'+post_data['financial_operations_payment_type']+'">' +
                    '</div>' +
                '</div>' +
                category_type_html +
            '</div>' +
            '<div class="field-group">' +
        '<div class="field">'+
            '<div class="name"></div>' +
            '<div class="value"><span class="errors"></span></div>' +
            '</div>' +
            '</div>' +
        '</div>';
        var success = function(response) {
            if(response.status = 'ok') {
                var data = response.data;
                var category = data.category;
                if(parseInt(category.parent_category_id)>0) {
                    $.manufactory.financialOperations.categories[category.financial_operations_type][category.parent_category_id]['categories'][category.id] = category;
                } else {
                    $.manufactory.financialOperations.categories[category.financial_operations_type][category.id] = category;
                }

                var add_category = ' ';
                var dop_class = ' ';
                var data_parent_category_id = ' ';
                if(category.parent_category_id == 0) {
                    var block_selector = '#'+category.financial_operations_type+'-detail ul';
                    dop_class = ' sub-block ';
                    data_parent_category_id = ' ';
                    if(category.type=='group') {
                        add_category = '<a href="#" class="add-category" data-type="category" data-parent_category_id="'+category.id+'">'+
                            '<i class="icon16 add"></i>' +
                            '</a>';
                    }
                } else {
                    var block_selector = '#category-detail-'+category.parent_category_id;
                    data_parent_category_id = ' data-parent_category_id="'+category.parent_category_id+'" ';
                }
                var html = '<div class="category'+dop_class+'" id="category-'+category.id+'" '+data_parent_category_id+' data-id="'+category.id+'">' +
                            '<div class="category-name">';
                                if(category.parent_category_id == 0) {
                                    html += '<h2>' +
                                        add_category+'    <span class="toggle" data-toggle="category-detail-'+category.id+'">'+category.name+'</span> '+
                                        '<a href="#" class="add-operation">' +
                                        '<span class="amount">'+category.amount+' руб.</span>'+
                                        '</a></h2>';
                                } else {
                                    html += '<h3>'+
                                      '<span class="toggle" data-toggle="category-detail-'+category.id+'">'+category.name+'</span> '+
                                        '<a href="#" class="add-operation">' +
                                        '<span class="amount">'+category.amount+' руб.</span>'+
                                        '</a></h3>';
                                }
                               html += '</div>' +
                            '<div  class="category-detail" id="category-detail-'+category.id+'"></div>' +
                        '</div>';
                if(category.parent_category_id == 0) {
                    $(block_selector).append('<li>'+html+'</li>');
                } else {
                    $(block_selector).append(html);
                }
                $.manufactory.dialog.trigger('close');

            } else {
                $.manufactory.dialog.find('.errors').html(response.errors.join(','));
            }

        };
        $.manufactory.showModalWindow('?module=financialOperations&action=categorySave',html,success,'Добавить');
        //av(post_data);
        /*$.post('?module=financialOperations&action=categorySave',post_data, function (response) {
            if(response.status=='ok') {
                av(response.data);
            } else {
                
            }
        });*/
    },
    getOperationSelectCategories: function(type,id) {
        var categories = this.categories[type];
        var html = '<select name="category_id" >';
        var _default = false;
        if(id==0) {
            _default = true;
        }
        var selected = false;
        for(var k in categories) {
            var category = categories[k];
            if(category.active==1) {
                if(category.type=='group') {
                    if(id==category.id) {
                        _default = true;
                    }
                    var subcategories = category.categories;
                    html += '<optgroup label="'+category.name+'">';
                    for(var sk in subcategories) {
                        var subcategory = subcategories[sk];
                        if(subcategory.active==1) {
                            var _selected = '';
                            if(!selected && _default) {
                                 _selected = ' selected ';
                                selected = true;
                            } else if(!selected && id==subcategory.id ) {
                                 _selected = ' selected ';
                                selected = true;
                            }
                            html += '<option value="'+subcategory.id+'"  '+_selected+'>'+subcategory.name+'</option>';
                        }
                    }
                    html += '</optgroup>';
                } else {
                    var _selected = '';
                    if(!selected &&_default) {
                         _selected = ' selected ';
                        selected = true;
                    } else if(!selected && id==category.id ) {
                         _selected = ' selected ';
                        selected = true;
                    }
                    html += '<option value="'+category.id+'"  '+_selected+'>'+category.name+'</option>';
                }
            }

        }
        html += '</select>';
        return html;
    },
    addOperation: function (obj) {
        var block = $(obj).closest('.block');
        var type = block.data('type');
        var category_block = $(obj).closest('.category');
        var category_id = category_block.data('id');
        if(!category_id) {
            category_id = 0;
        }
        var post_data = {};
        post_data['category_id'] = category_id;
        var html = '<div class="fields">' +
            '<h1>Добавление финансовой операции</h1>'+
            '<div class="field-group">' +
            '<div class="field">' +
            '<div class="name">Название</div>' +
            '<div class="value">' +
            '<input type="text" name="name" value="">' +
            '</div>' +
            '</div>' +
            '<div class="field">' +
            '<div class="name">Сумма</div>' +
            '<div class="value">' +
            '<input type="text" name="amount" value="">' +
            '</div>' +
            '</div>' +
            '<div class="field">' +
                '<div class="name">Описание</div>' +
                '<div class="value">' +
                '<textarea name="description"></textarea>' +
                '</div>' +
            '</div>' +
            '<div class="field">' +
            '<div class="name">Категория</div>' +
            '<div class="value">' +
            this.getOperationSelectCategories(type,category_id) +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="field-group"><span class="errors"></span></div>' +
            '</div>';
        var success = function(response) {
            if(response.status=='ok') {
                var data = response.data;
                var operation = data.operation;

                $.manufactory.financialOperations.setBalances(data.balances);
                $.manufactory.financialOperations.setContactsData(data.contacts);
                
                var block = $('#category-detail-'+operation.category_id);
                $.manufactory.financialOperations.addOperationHTML(block,operation);
                block.removeClass('hide');
                $.manufactory.dialog.trigger('close');
            } else {
                $.manufactory.dialog.find('.errors').html(response.errors.join(','));
            }

        };
        $.manufactory.showModalWindow('?module=financialOperation&action=Save',html,success,'Добавить',600);
    }
};
$(document).ready(function () {
   $(document).on('click','.add-category',function () {
        $.manufactory.financialOperations.addCategory($(this));
        return false;
    });
    $(document).on('click','.add-operation',function () {
        $.manufactory.financialOperations.addOperation($(this));
        return false;
    });
   /* $('.amount').click(function () {
        $.manufactory.financialOperations.addOperation($(this));
        return false;
    });*/

});