<?php

class manufactoryCompositematerialsProductCostsModel 
    
    //extends manufactoryProductMaterialsModel
     extends manufactoryEntityCostsModel
{

    protected $table = 'manufactory_compositematerials_product_materials';
    protected $context = 'entity_id';
    public function setByEntity($id, $data = array()) {

        $delete_data = $this->getByEntity($id);
        $change_data = false;
        foreach ($data as $sid => $quantity) {
            if(array_key_exists($sid, $delete_data)) {
                if($delete_data[$sid]['quantity'] != $quantity) {
                    $this->updateByField(array(
                        $this->entity_id_column => $id,
                        $this->storage_id_column => $sid
                    ), array('quantity' => $quantity));
                    $change_data = true;
                }
                unset($delete_data[$sid]);
            } else {
                $change_data =  true;
                $_data = array(
                    $this->entity_id_column => $id,
                    $this->storage_id_column => $sid,
                    'quantity' => $quantity
                );
                $this->insert($_data);
            }
        }
        if(!empty($delete_data)) {
            $change_data =  true;
            foreach ($delete_data as $delete_row) {
                $this->deleteByField(
                    $delete_row
                );
            }
        }

        return $change_data;
    }
    public function getByProduct($id) {
        return $this->getByEntity($id);
    }
    public function setByProduct($id, $data) {
        return $this->setByEntity($id, $data);
    }
    public function deleteByProduct($id) {
        return $this->deleteByEntity($id);
    }
    public function getProductsByMaterialId($cost_id) {
        return $this->getByField(array(
            'cost_id' => $cost_id
        ),'entity_id');
    }


}