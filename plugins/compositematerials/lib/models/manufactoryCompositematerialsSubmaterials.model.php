<?php

class manufactoryCompositematerialsSubmaterialsModel extends manufactoryEntityCostsModel {

    protected $table = 'manufactory_compositematerials_submaterials';
    protected $entity_id_column = 'composite_material_id';
    protected $storage_id_column = 'material_id';





    public function deleteBySubMaterial($id) {
        return  $this->deleteByField(array(
            $this->storage_id_column => $id,
        ));
    }
    public function getBySubMaterial($id) {
        return  $this->getByField(array(
            $this->storage_id_column => $id,
        ),$this->entity_id_column);
    }
}