<?php

class manufactoryCompositematerialsProductMaterialsModel 
    
    //extends manufactoryProductMaterialsModel
     extends manufactoryCostEntityModel
{

    protected $table = 'manufactory_compositematerials_product_materials';
    protected $context = 'entity_id';
   
    public function getByProduct($id) {
        return $this->getByEntity($id);
    }
    public function setByProduct($id, $costs) {
        return $this->setByEntity($id,$costs);
    }
    public function deleteByProduct($id) {
        return $this->deleteByEntity($id);
    }
    public function getProductsByMaterialId($cost_id) {
        return $this->getByField(array(
            'cost_id' => $cost_id
        ),'entity_id');
    }


}