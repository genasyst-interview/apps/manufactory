<?php
return array(
    'manufactory_compositematerials_material' => array(
        'id' => array('int', 11, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'name' => array('varchar', 150, 'null' => 0),
        'price' => array('double', 'null' => 0),
        'formula' => array('text', 'null' => 0),
        'status' => array('enum', "'active','delete'", 'null' => 0, 'default' => 'active'),
        'category_id'  => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'type' => array('enum', "'coefficient','formula','delimiter'", 'null' => 0, 'default' => 'formula'),
        'sku_features_coefficients' => array('text', 'null' => 0),
        'create_datetime' => array('datetime', 'null' => 0),
        'update_datetime' => array('datetime', 'null' => 0),
        'sort' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        ':keys' => array(
            'PRIMARY' => 'id',
        ),
    ),
    'manufactory_compositematerials_product_materials' => array(
        'id' => array('int', 11, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'product_id' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'material_id' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'quantity' => array('double', 'null' => 0),
        'sort' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        ':keys' => array(
            'PRIMARY' => 'id',
        ),
    ),
    'manufactory_compositematerials_submaterials' => array(
        'id' => array('int', 11, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'material_id' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'quantity' => array('double', 'null' => 0),
        'sort' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        ':keys' => array(
            'PRIMARY' => 'id',
        ),
    ),
    'manufactory_compositematerials_category_materials' => array(
        'id' => array('int', 11, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'name' => array('varchar', 150, 'null' => 0),
        'parent_id' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'sort' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        ':keys' => array(
            'PRIMARY' => 'id',
        ),
    ),
);
