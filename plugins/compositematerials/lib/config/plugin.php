<?php

return array(
    'name'     => 'Составные материалы',
    'img'     => 'img/asn.png',
    'version'  => '1.1.0',
    'vendor'   => '45345345',
    'handlers' =>  array(
        /* 'product_materials' => 'productMaterials',
                 'product_sku_materials' => 'productSkuMaterials',*/
        'backend_costs' => 'backendCostsSidebar',
        'backend_product_edit' => 'backendProductEdit',
        //'backend_product_save' => 'backendProductSave',
        'backend_product_costs' => 'backendProductCosts',
        
        'costs' => 'registerCostTypes',
        
        'cost_delete' => 'costDelete',
        'cost_recalculate'=> 'costRecalculate',
),
);
