<?php

class manufactoryCompositematerialsPluginProductAction extends waViewAction  {

   /* public function display($clear_assign = true)
    {
        waSystem::pushActivePlugin('compositematerials', 'manufactory');
        $data =  parent::display($clear_assign);
        waSystem::popActivePlugin();
        return $data;
    }*/
   public function __construct($params)
   {
       parent::__construct($params);
       $this->view = new waSmarty3View(wa());
   }

    public function execute()
    {
       $this->view->assign('product',  $this->params['product']);
        $this->view->assign('categories', $this->getCategories());

        if(isset($this->params['product']) && is_object($this->params['product'])) {
            $this->view->assign('product_materials',  $this->getProductMaterials());
        }else {
            $this->view->assign('product_materials', array());

        }
    }
    public function getProductMaterials()  {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CompositematerialsProductCosts');
        $materiels = $model->getByProduct($this->params['product']->getId());
        $materials = array();
        $type = wa('manufactory')->getConfig()->getFactory('Costs')->getType('compositematerials');
        foreach($materiels as $id=> $m)  {
            $materials[$id] = $type->buildEntityCost($this->params['product'], $m);
        }
        return $materials;
    }
    public function getCategories() {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CompositematerialsCategoryCosts');
        $categories = $model->getAll('id');
        foreach ($categories as $id => $data) {
            $categories[$id] = new manufactoryСompositematerialsPluginCategoryCosts($data);
        }
        return $categories;
    }

}