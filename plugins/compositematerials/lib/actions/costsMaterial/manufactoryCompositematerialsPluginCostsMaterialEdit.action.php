<?php

class manufactoryCompositematerialsPluginCostsMaterialEditAction extends waViewAction
{

    public function execute()
    {
        $this->setLayout(new manufactoryCostsLayout());
        
        $id = waRequest::get('id',0, waRequest::TYPE_INT);
        $category = waRequest::get('category', 0, waRequest::TYPE_INT);
        $material = false;

        $Pricing  = new manufactoryPricing();
        $this->view->assign('Pricing', $Pricing);
        if(!empty($id)) {
            $material = new manufactoryCompositematerialsPluginCost($id);
        }
        $this->view->assign('categories', $this->getCategories());
        $this->view->assign('category',   $category);
        $this->view->assign('material',   $material);

    }
    public function getCategories() {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CompositematerialsCategoryCosts');
        $categories = $model->getAll('id');
        foreach ($categories as $id => $data) {
            $categories[$id] = new manufactoryСompositematerialsPluginCategoryCosts($data);
        }
        return $categories;
    }

}
