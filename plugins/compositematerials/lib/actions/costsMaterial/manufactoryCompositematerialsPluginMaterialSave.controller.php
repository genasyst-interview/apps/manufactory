<?php

class manufactoryCompositematerialsPluginMaterialSaveController extends waJsonController  {


    public function execute()
    {
        $id = waRequest::post('id',0, waRequest::TYPE_INT);
        if(!empty($id)) {
            $material = new manufactoryCompositematerialsPluginCost($id);
        } else {
            $material = new manufactoryCompositematerialsPluginCost();
        }
        $data = waRequest::post();
        $data['name'] = trim($data['name']);
        $this->response['id'] = $material->save($data);
    }
}