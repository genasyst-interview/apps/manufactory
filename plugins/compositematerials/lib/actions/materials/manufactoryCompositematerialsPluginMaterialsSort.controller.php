<?php

class manufactoryCompositematerialsPluginMaterialsSortController extends waJsonController  {


    public function execute()
    {
        $id  = waRequest::post('id');
        $after_id  = waRequest::post('after_id');
        if(!empty($id)) {
            $model = manufactoryModelsPool::getInstance()->get('CompositematerialsCost');
            $model->move($id, $after_id);
        }
    }
}