<?php

class manufactoryCompositematerialsPluginCostsAction extends waViewAction
{

    public function execute()
    {
        if(waRequest::method()=='post') {
            $this->sort();
        }
        if(waRequest::get('mobile')==1) {
            $this->setLayout(new manufactoryCostsLayout());
        } else {
            if(waRequest::isMobile()) {
                $this->setLayout(new manufactoryCostsLayout());
            } else {
                $this->setLayout(new manufactoryCostsLayout());
            }
        }
        $this->view->assign('categories', $this->getCategories());


    }
    public function sort() {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('Material');
        $sort = waRequest::post('sort');
        foreach ($sort as $k=>$v) {
            $model->updateById($k,array('sort'=>$v));
        }
    }
    public function getCategories() {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CompositematerialsCategoryCosts');
        $categories = $model->getAll('id');
        foreach ($categories as $id => $data) {
            $categories[$id] = new manufactoryСompositematerialsPluginCategoryCosts($data);
        }
        return $categories;

    }
}
