<?php

class manufactoryCompositematerialsPluginCategoryMaterialsDeleteController extends waJsonController  {


    public function execute()
    {
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);
        if(!empty($id)) {
            $category = new manufactoryСompositematerialsPluginCategoryCosts($id);
            $category->delete();
            
        }
    }
}