<?php

class manufactoryCompositematerialsPluginCategoryMaterialsAction extends waViewAction
{

    public function execute()
    {
        if(waRequest::method()=='post') {
            $this->sort();
        }
        if(waRequest::get('mobile')==1) {
            $this->setLayout(new manufactoryMobileLayout());
        } else {
            if(waRequest::isMobile()) {
                $this->setLayout(new manufactoryMobileLayout());
            } else {
                $this->setLayout(new manufactoryDefaultLayout());
            }
        }
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CompositematerialsCategoryCosts');
        $categories = $model->getAll('id');
        foreach ($categories as $id => $data) {
            $categories[$id] = new manufactoryСompositematerialsPluginCategoryCosts($data);
        }
        $this->view->assign('categories', $categories);
      
    }
    public function sort() {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CompositematerialsCategoryCosts');

        $sort = waRequest::post('sort');
        foreach ($sort as $k => $v) {
            $model->updateById($k, array('sort'=> $v));
        }
    }
}
