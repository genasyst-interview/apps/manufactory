<?php

class manufactoryCompositematerialsPluginCategoryMaterialsEditAction extends waViewAction
{

    public function execute()
    {
        $this->setLayout(new manufactoryCostsLayout());
        
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);
        $category = false;


        if(!empty($id)) {
            $models_pool = manufactoryModelsPool::getInstance();
            $model = $models_pool->get('CompositematerialsCategoryCosts');

            $data  = $model->getById($id);
            if($data) {
             $category = new manufactoryСompositematerialsPluginCategoryCosts($data);
            }

        }
        $this->view->assign('category', $category);
    }

}
