<?php

class manufactoryCompositematerialsPluginMaterialEditAction extends waViewAction
{

    public function execute()
    {
        if(waRequest::get('mobile')==1) {
            $this->setLayout(new manufactoryMobileLayout());
        } else {
            if(waRequest::isMobile()) {
                $this->setLayout(new manufactoryMobileLayout());
            } else {
                $this->setLayout(new manufactoryDefaultLayout());
            }
        }
        $id = waRequest::get('id',0, waRequest::TYPE_INT);
        $category = waRequest::get('category', 0, waRequest::TYPE_INT);
        $material = false;

        $Pricing  = new manufactoryPricing();
        $this->view->assign('Pricing', $Pricing);
        if(!empty($id)) {
            $material = new manufactoryCompositematerialsPluginMaterial($id);
        }
        $this->view->assign('categories', $this->getCategories());
        $this->view->assign('category',   $category);
        $this->view->assign('material',   $material);

    }
    public function getCategories() {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CompositematerialsCategoryMaterials');
        $categories = $model->getAll('id');
        foreach ($categories as $id => $data) {
            $categories[$id] = new manufactoryСompositematerialsPluginCategoryMaterials($data);
        }
        return $categories;
    }

}
