<?php

class manufactoryCompositematerialsPluginMaterialSaveController extends waJsonController  {


    public function execute()
    {
        $id = waRequest::post('id',0, waRequest::TYPE_INT);
        if(!empty($id)) {
            $material = new manufactoryCompositematerialsPluginMaterial($id);
        } else {
            $material = new manufactoryCompositematerialsPluginMaterial();
        }
        $data = waRequest::post();
        $data['name'] = trim($data['name']);
        $this->response['id'] = $material->save($data);
    }
}