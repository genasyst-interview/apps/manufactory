<?php


class manufactoryCompositematerialsPlugin extends manufactoryPluginCost
{
   public function __construct($info)
   {
      parent::__construct($info);
   }

   public function productMaterials(&$materials) {

   }
   public function productSkuMaterials(&$materials) {

   }
   public function backendCostsSidebar()  {
      $plugin = waRequest::get('plugin');
      $module = waRequest::get('module');
      if(strtolower($plugin)=='compositematerials' && strtolower($module)=='materials') {
         return  array('sidebar' => '<li class="selected"><a href="?plugin=compositematerials&module=costs"><i class="icon16 stack"></i>Составные материалы</a></li>');
      } else {
         return  array('sidebar' => '<li><a href="?plugin=compositematerials&module=costs"><i class="icon16 stack"></i>Составные материалы</a></li>');
      }
   }
   public function backendProductEdit($product) {
         $action = new manufactoryCompositematerialsPluginProductAction(array('product'=> $product));
         return  array('costs' => $action->display(),
             'costs_menu'=>'<li><a href="#" data-tab="costs_compositematerials">Составные материалы</a></li>' ,
             'costs_type' => 'compositematerials'
         );
   }

   public function registerCostTypes(manufactoryFactoryCosts $costs)  {
     
      $costs->set('compositematerials', new manufactoryCompositematerialsPluginCostsType());
   }
   public function costDelete(manufactoryEntityCostDelete $data)  {
      $this->log($data->getEntityType());
      if($data->getEntityType() == 'materials') {
         $data->setDependency(new manufactoryCompositematerialDependenciesSubmaterial($data->getEntity()));
      }
   }
   public function costRecalculate(manufactoryEntityCostRecalculate $data)  {
      if($data->getEntityType() == 'materials') {
         $data->setDependency(new manufactoryCompositematerialDependenciesSubmaterial($data->getEntity()));
      }
   }
   public function log($data) {
      waLog::dump($data,'compositematerials.log');
   }

}
