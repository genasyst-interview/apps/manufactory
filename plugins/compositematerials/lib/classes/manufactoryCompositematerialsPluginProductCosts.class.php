<?php

class manufactoryCompositematerialsPluginProductCosts extends manufactoryEntityCosts {
   public function init()
   {
     $this->data =  $this->getFactory('Costs')
         ->getType($this->getEntityType())
         ->getByEntity($this->getEntity());
       
   }
    public function getEntityType()
    {
        return 'compositematerials';
    }

}