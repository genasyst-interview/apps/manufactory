<?php

class manufactoryCompositematerialDependenciesMaterial extends manufactoryEntityCostDependencies {

    public function getProducts()
    {
        return  $this->getModel('CompositematerialsProductMaterials')
            ->select('product_id as id')->where(array('material_id' => $this->id ))->fetchAll('id');
    }
    
    public function deleteDependencies()
    {
       $this->deleteProductMaterials();
       $this->deleteCostSubmaterials();
    }
    public function deleteProductMaterials() {
        $this->getModel('CompositematerialsProductMaterials')->deleteByCost($this->getId());
    }
    public function deleteCostSubmaterials() {
        $this->getModel('CompositematerialsSubmaterials')->deleteByCost($this->getId());
    }





}
