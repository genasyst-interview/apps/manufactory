<?php

class manufactoryCompositematerialDependenciesCost extends manufactoryEntityCostDependencies {

    public function getProducts()
    {
        return  $this->getModel('CompositematerialsProductCosts')
            ->select('entity_id as id')->where('cost_id ='.$this->id)->fetchAll('id');
    }
    public function getDependencies()
    {
        //
    }
    public function recalculateDependencies()
    {
        // TODO: Implement recalculateDependencies() method.
    }

    public function deleteDependencies()
    {
       $this->deleteProductMaterials();
       $this->deleteCostSubmaterials();
    }
    public function deleteProductMaterials() {
        $this->getModel('CompositematerialsProductCosts')->deleteByCost($this->getId());
    }
    public function deleteCostSubmaterials() {
        $this->getModel('CompositematerialsSubmaterials')->deleteByEntity($this->getId());
    }





}
