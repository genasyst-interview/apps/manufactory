<?php

class manufactoryCompositematerialDependenciesSubmaterial extends manufactoryEntityCostDependencies {

    public function getDependencies() {
        $return = array(
            'products' => array(),
            'costs' => array(
                'compositematerial' => $this->getCompositematerials()
            ),
        );
    }
    public function getProducts()
    {
        $sql = "SELECT entity_id AS id FROM  manufactory_compositematerials_product_materials AS pm  
LEFT JOIN manufactory_compositematerials_submaterials sm ON sm.composite_material_id = pm.cost_id
WHERE sm.material_id = i:material_id";
        return $this->getModel('CompositematerialsProductCosts')
            ->query($sql, array('material_id' => $this->id))->fetchAll('id');
    }

    public function deleteDependencies()
    {
       $this->deleteFromCompositematerials();
    }
    public function deleteFromCompositematerials()  {
        $this->getModel('CompositematerialsSubmaterials')->deleteBySubMaterial($this->getId());
    }

    public function getCompositematerials() {
        $return = array();
        $compositematerials =   $this->getModel('CompositematerialsSubmaterials')->getBySubMaterial($this->id);
        if(!empty($compositematerials)) {
            foreach ($compositematerials as $id => $v) {
                $return[$id] = $id;
            }
        }
        return $return;
    }
    public function recalculateDependencies()
    {
        /*foreach ($this->getCompositematerials() as $id => $v)  {
            $this->getFactory('Costs')->get($id,'compositematerial');
        }*/
    }
}
