<?php

class manufactoryCompositematerialsPluginCost extends manufactoryCost  implements ArrayAccess {
    protected $data = array();
    protected $submaterials = array();
    const ENTITY_TYPE = 'compositematerials';
    public function init(){

    }
    public function __construct($data = null)
    {
        if(is_array($data)) {
            $this->data = $data;
        } elseif(!empty($data)){
            $model = manufactoryModelsPool::getInstance()->get('CompositematerialsCost');
            $data =  $model->getById($data);
            if(!empty($data)) {
                $this->data = $data;
            }
        }
        if(!empty($this->data)) {
            $this->setData();
        }
    }
    public function setData($data = null) {
        $submaterials = $this->getModel('CompositematerialsSubmaterials')->getByEntity($this->getId());
        $ids = array();
        foreach ($submaterials as $submaterial) {
            $ids[] = $submaterial['material_id'];
        }
        $materials = $this->getModel('Material')->getByField('id', $ids, 'id');
        foreach ($materials as $id => $material) {
            $this->submaterials[$id] = new manufactoryCompositematerialsPluginSubmaterial(
                new manufactoryCostMaterial($material),
                $submaterials[$id]['quantity']
            );
        }
    }

    public function getType() {
        return $this->data['type'];
    }
   
    public function getCost() {
        $cost = 0.0;
       foreach ($this->submaterials as $submaterial) {
           $cost += $submaterial->getCost()->getCost();
       }
        return $cost;
    }
    public function getCosts() {
        return $this->submaterials;
    }
    public function getSkuCosts($sku) {
        $costs = array();
        foreach($this->getCosts() as $id => $cost) {
            $costs[$id] = new manufactoryProductSkuCost($cost, $sku);
        }
        return $costs;
    }
    public function getFormula() {
        return '';
    }
    public function getPrice($sku_data = '')  {
        $price  = 0.0;
        foreach ($this->getCosts() as $submaterial) {
            $price += $submaterial->getPrice($sku_data);
        }
        return $price;
    }

    public function getCalculateString($sku_data = '') {
        $price  = '';
        foreach ($this->getCosts() as $submaterial) {
            $price .= round($submaterial->getPrice($sku_data),2).'+';
        }
        $price = rtrim($price,'+');
        return $price;
    }
    public function getSkusFeatures() {
        return array();
    }
    public function regenerateSkusFeatures()  {

    }
    public function getCoefficient($sku_data = '')  {
       return '';
    }
    public function isActive() {
        if($this->getStatus()=='active') {
            return true;
        }
        return false;
    }
    public function getStatus() {
        return $this->data['status'];
    }
    public function getSort(){
        return $this->data['sort'];
    }
    public function setStatus($status) {
        $data = array('status'=> $status);
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('Material');
        return $model->updateById($this->getId(), $data);
    }
    public function save($data = array())  {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CompositematerialsCost');
        $materials = array();
        if(array_key_exists('materials_id', $data)) {
            $materials = $data['materials_id'];
        }
        unset($data['materials_id']);
        
        if($this->getId()) {
            $material = $model->getById($this->getId());
            if(!empty($material)) {
                $model->updateById($this->getId(), $data);
            }
        } else {
            $data['update_datetime'] = $data['create_datetime'] = date("Y-m-d H:i:s");
            $this->data['id'] =  $model->insert($data);
        }
        $change = $this->getModel('CompositematerialsSubmaterials')->setByEntity($this->getId(),  $materials);
        if($change) {
            $depen = new manufactoryCompositematerialsCostRecalculate($this);
            $depen->run();
        }
        
        return $this->getId();
    }
    public function delete() {
        $delete = new manufactoryCompositematerialsCostDelete($this);
        $delete->run();
        return true;
    }
}