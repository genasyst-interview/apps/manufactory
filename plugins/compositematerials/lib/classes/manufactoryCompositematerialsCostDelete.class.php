<?php

class manufactoryCompositematerialsCostDelete extends manufactoryEntityCostDelete {

    public function getEntityModel() {
        return $this->getModel('CompositematerialsCost');
    }
    public function setDependencies()
    {
        $this->setDependency(new manufactoryCompositematerialDependenciesCost($this->getEntity()));
    }
}
