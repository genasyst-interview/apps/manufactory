<?php

class manufactoryСompositematerialsPluginCategoryMaterials  {
    protected $data = array();


    public function __construct($data = null)
    {
        if(is_array($data)) {
            $this->data = $data;
        } elseif(!empty($data)){

            $model = manufactoryModelsPool::getInstance()->get('CompositematerialsCategoryMaterials');
            $data =  $model->getById($data);
            if(!empty($data)) {
                $this->data = $data;
            }
        }
       /* if(!empty($this->data)) {
            $this->setData();
        }*/
    }
    public function getName(){
        return $this->data['name'];
    }
    public function getId(){
        return $this->data['id'];
    }
   public function getMaterials() {
       $model = manufactoryModelsPool::getInstance()->get('CompositematerialsMaterial');
       $data = $model->getByField('category_id',$this->getId(),'id');
       foreach ($data as $k => $v) {
           $data[$k] = new manufactoryCompositematerialsPluginMaterial($v);
       }
       return $data;
   }
    public function getSort(){
        return $this->data['sort'];
    }
}