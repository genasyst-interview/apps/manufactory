<?php

/*
 * id	int(11) unsigned Автоматическое приращение	 
name	varchar(255)	 
price	float	 
formula	text	 
sku_features_coefficients	text	 
category_id	int(11) unsigned	 
sort	int(11) unsigned
*/
class manufactoryCompositematerialsMaterialDelete extends manufactoryEntityCostDelete {

    public function getEntityModel() {
        return $this->getModel('CompositematerialsMaterial');
    }
    public function setDependencies()
    {
        $this->setDependency(new manufactoryCompositematerialDependenciesMaterial($this->getEntity()));
    }

}
