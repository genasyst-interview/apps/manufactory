<?php

class manufactoryCompositematerialsPluginProductCost extends manufactoryCostEntity {
  protected $cost = null;
  protected $quantity = 0;

    public function __construct(manufactoryCost $cost, $quantity)
    {
        if(is_object($cost)) {
            $this->cost = $cost;
        } elseif(is_array($cost) || is_numeric($cost)) {
            $this->cost = new manufactoryCompositematerialsPluginCost($cost);
        }
        $this->quantity = $quantity;
    }
    public function getPrice($sku_data = '')  {
        $price = $this->cost->getPrice($sku_data);
      return  $price*$this->quantity;
    }
    public function getQuantity() {
        return $this->quantity;
    }
    public function __call($name, $arguments)
    {
        if (method_exists($this->cost, $name)) {
            return call_user_func_array(array($this->cost, $name), $arguments);
        }
    }
    public function __get($name)
    {
        return $this->cost->$name;
    }
}