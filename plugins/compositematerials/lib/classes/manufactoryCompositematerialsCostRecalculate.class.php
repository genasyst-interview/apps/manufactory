<?php

/*
 * id	int(11) unsigned Автоматическое приращение	 
name	varchar(255)	 
price	float	 
formula	text	 
sku_features_coefficients	text	 
category_id	int(11) unsigned	 
sort	int(11) unsigned
*/
class manufactoryCompositematerialsCostRecalculate extends manufactoryEntityCostRecalculate {

    public function getEntityModel() {
        return $this->getModel('CompositematerialsCost');
    }
    public function setDependencies()
    {
        $this->setDependency(new manufactoryCompositematerialDependenciesCost($this->getEntity()));
    }
}
