<?php

class manufactoryCompositematerialsPluginMaterial extends manufactoryCost  implements ArrayAccess {
    protected $data = array();
    protected $submaterials = array();
    const ENTITY_TYPE = 'compositematerial';
    public function __construct($data = null)
    {
        if(is_array($data)) {
            $this->data = $data;
        } elseif(!empty($data)){

            $model = manufactoryModelsPool::getInstance()->get('CompositematerialsMaterial');
            $data =  $model->getById($data);
            if(!empty($data)) {
                $this->data = $data;
            }
        }
        if(!empty($this->data)) {
            $this->setData();
        }
    }
    public function setData($data = null) {
        $model = manufactoryModelsPool::getInstance()->get('CompositematerialsSubmaterials');
        $submaterials = $model->getByCost($this->getId());
        $Materialmodel = manufactoryModelsPool::getInstance()->get('Material');
        $ids = array();
        foreach ($submaterials as $submaterial) {
            $ids[] = $submaterial['material_id'];
        }
        $materials = $Materialmodel->getByField('id', $ids,'id');
        foreach ($materials as $id => $material) {
            $this->submaterials[$id] = new manufactoryCompositematerialsPluginSubmaterial(
                new manufactoryMaterial($material),
                $submaterials[$id]['quantity']
            );
        }
    }
    public function getId(){
        if(isset($this->data['id'])) {
            return $this->data['id'];
        }
        return false;
    }

    public function getType() {
        return 1;
    }
    /* баг метод getType уже занят под расчеты это будет под тип */
    public function getSubType() {
        return $this->data['type'];
    }
    public function getName() {
        return $this->data['name'];
    }
    public function getCost() {
        $cost = 0.0;
       foreach ($this->submaterials as $submaterial) {
           $cost += $submaterial->getCost();
       }
        return $cost;
    }
    public function getCosts() {
        return $this->submaterials;
    }
    public function getSkuCosts($sku) {
        $costs = array();
        foreach($this->getCosts() as $id => $cost) {
            $costs[$id] = new manufactoryProductSkuCost($cost, $sku);
        }
        return $costs;
    }
    public function getFormula() {
        return '';
    }
    public function getPrice($sku_data = '')  {
        $price  = 0.0;
        foreach ($this->getCosts() as $submaterial) {
            $price += $submaterial->getPrice($sku_data);
        }
        return $price;
    }

    public function getCalculateString($sku_data = '') {
        $price  = '';
        foreach ($this->getCosts() as $submaterial) {
            $price .= round($submaterial->getPrice($sku_data),2).'+';
        }
        $price = rtrim($price,'+');
        return $price;
    }
    public function getSkusFeatures() {
        return array();
    }
    public function regenerateSkusFeatures()  {

    }
    public function getCoefficient($sku_data = '')  {
       return '';
    }
    public function isActive() {
        if($this->getStatus()=='active') {
            return true;
        }
        return false;
    }
    public function getStatus() {
        return $this->data['status'];
    }
    public function getSort(){
        return $this->data['sort'];
    }
    public function setStatus($status) {
        $data = array('status'=> $status);
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('Material');
        return $model->updateById($this->getId(), $data);
    }
    public function save($data = array())  {

        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CompositematerialsMaterial');
        $materials = array();
        if(array_key_exists('materials_id', $data)) {
            $materials = $data['materials_id'];
        }
        unset($data['materials_id']);

       
        if($this->getId()) {
            $material = $model->getById($this->getId());
            if(!empty($material)) {

                $model->updateById($this->getId(), $data);
            }

        } else {
            $data['update_datetime'] = $data['create_datetime'] = date("Y-m-d H:i:s");
            $this->data['id'] =  $model->insert($data);
        }
      
        $submaterials_model = $models_pool->get('CompositematerialsSubmaterials');
        $submaterials_model->setByCost($this->getId(), $materials);

        return $this->getId();
    }
    public function delete() {
        $delete = new manufactoryCompositematerialsMaterialDelete($this);
        $delete->run();
        $models_pool = manufactoryModelsPool::getInstance();
        $product_materials = $models_pool->get('CompositematerialsProductMaterials');
        $s_model = $models_pool->get('CompositematerialsProductMaterials');
        $products = $product_materials->getByCost($this->getId());
        // Удаляем привязки к продуктам
        $product_materials->deleteByCost($this->getId());
        $s_model->deleteByCost($this->getId());
        
        foreach ($products as $pv) {
            $product = manufactoryProducts::getById($pv['product_id']);
            $product->recalculatePrices();
        }
        $model = $models_pool->get('CompositematerialsMaterial');
        $model->deleteById($this->getId());
        return true;
    }









    public function offsetExists($offset)
    {
        if(isset($this->data[$offset])) {
            return true;
        }
        return false;
    }
    public function offsetGet($offset)
    {
        if(isset($this->data[$offset])) {
            return  $this->data[$offset];
        }
        return null;
    }
    public function offsetSet($offset, $value)
    {

        $this->data[$offset] = $value;

    }
    public function offsetUnset($offset)
    {

        $this->data[$offset] = null;

    }
}