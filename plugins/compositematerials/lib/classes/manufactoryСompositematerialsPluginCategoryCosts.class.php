<?php

class manufactoryСompositematerialsPluginCategoryCosts extends manufactoryClass {
    protected $data = array();


    public function __construct($data = null)
    {
        if(is_array($data)) {
            $this->data = $data;
        } elseif(!empty($data)){

            $model = $this->getModel('CompositematerialsCategoryCosts');
            $data =  $model->getById($data);
            if(!empty($data)) {
                $this->data = $data;
            }
        }
       /* if(!empty($this->data)) {
            $this->setData();
        }*/
    }
    public function getName(){
        return $this->data['name'];
    }
    public function getId(){
        return $this->data['id'];
    }
   public function getData() {
       $model = $this->getModel('CompositematerialsCost');
       $data = $model->getByField('category_id', $this->getId(),'id');
       foreach ($data as $k => $v) {
           $data[$k] = new manufactoryCompositematerialsPluginCost($v);
       }
       return $data;
   }
    public function getSort(){
        return $this->data['sort'];
    }
    public function delete()  {
        $costs = $this->getData();
        $costs_delete =  array();
        $products = array();
        foreach ($costs as $v)  {
            $cost_delete =  new manufactoryCompositematerialsCostDelete($v);
            $costs_delete[$v->getId()] = $cost_delete;
            $products = $products+ $cost_delete->getProducts(); // Собираем продукты для пересчета
            $cost_delete->deleteDependencies();// Удаляем зависимости
            $cost_delete->recalculateDependencies(); // Пересчитываем зависимости
            $cost_delete->deleteEntity();
        }
        foreach ($products as $id =>  $v)  {
            $product = manufactoryProducts::getById($id);
            $product->recalculatePrices();
        }
        // Удаляем саму категорию
        $CategoryMaterialsModel = $this->getModel('CompositematerialsCategoryCosts');

        $CategoryMaterialsModel->deleteById($this->getId());
        
    }
}