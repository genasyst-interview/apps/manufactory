<?php

class manufactoryCompositematerialsPluginProductMaterial {
  protected $material = null;
  protected $quantity = 0;

    public function __construct($material, $quantity)
    {
        if(is_object($material)) {
            $this->material = $material;
        } elseif(is_array($material) || is_numeric($material)) {
            $this->material = new manufactoryCompositematerialsPluginMaterial($material);
        }
        $this->quantity = $quantity;
    }
    public function getPrice($sku_data = '')  {
        $price = $this->material->getPrice($sku_data);
      return   $price*$this->quantity;
    }
    public function getQuantity() {
        return $this->quantity;
    }
    public function __call($name, $arguments)
    {
        if (method_exists($this->material, $name)) {
            return call_user_func_array(array($this->material, $name), $arguments);
        }
    }
    public function __get($name)
    {
        return $this->material->$name;
    }
}