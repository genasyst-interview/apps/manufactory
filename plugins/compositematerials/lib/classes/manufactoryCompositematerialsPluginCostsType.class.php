<?php

class manufactoryCompositematerialsPluginCostsType extends manufactoryCostsType {
    const TYPE = 'compositematerials';
    const COST_MODEL = 'CompositematerialsCost';
    const COST_CLASS_NAME = 'manufactoryCompositematerialsPluginCost';
    const ENTITY_COSTS_CLASS_NAME = 'manufactoryCompositematerialsPluginProductCosts';
    protected $entity_costs_class_name =  array(
        'product' => 'manufactoryCompositematerialsPluginProductCost',
        'sku' => 'manufactoryCompositematerialsPluginSkuCost'
    );
    protected $entity_cost_models_name = array(
        'product' => 'CompositematerialsProductCosts',
        'sku' => 'CompositematerialsSkuCosts'
    );

    public function getTypeId()
    {
        return 'compositematerials';
    }
    public function getName() {
        return 'Состывные материалы';
    }
}