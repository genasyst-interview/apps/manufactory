<?php


class manufactoryLayerPlugin extends manufactoryPlugin
{
   public function __construct($info)
   {
      parent::__construct($info);
   }

   public function productBaseTypes(&$types) {
      $class_name = get_class($this).'ProductType';
      if(class_exists($class_name)) {
         $types[get_class($this)] = new $class_name();
      }
   }
}
