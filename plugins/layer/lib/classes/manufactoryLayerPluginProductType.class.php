<?php

class manufactoryLayerPluginProductType extends manufactoryProductTypeBaseMultiple {

    public function getProduct($data) {
        return new manufactoryLayerPluginProduct($data);
    }
    public function getName() {
        return 'Многоартикульный слоевой продукт';
    }
    public function getDescription() {
        return 'Данный тип продукта позволяет добавить артикулы с одним составом, но разными характеристиками длины и ширины, 
        высота расчитывается исходя из ';
    }
}