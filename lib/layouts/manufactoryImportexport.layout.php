<?php

class manufactoryImportexportLayout extends manufactoryDefaultLayout
{
    //backend_costs.sidebar
    public function execute()
    {
        parent::execute();
        $left_sidebar = new manufactoryImportexportSidebarAction(array());
        $this->view->assign('left_sidebar', $left_sidebar->display());
    }
}
// EOF