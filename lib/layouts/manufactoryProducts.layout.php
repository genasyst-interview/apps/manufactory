<?php

class manufactoryProductsLayout extends manufactoryDefaultLayout
{
    public function execute()
    {
        parent::execute();
        $params = null;
        $backend_products = wa('manufactory')->event('backend_products', $params, array('sidebar', 'right_sidebar'));

        $left_sidebar = new manufactoryProductsSidebarAction(array('backend_products' => $backend_products));
        $this->view->assign('left_sidebar', $left_sidebar->display(false));

        $id = waRequest::get('category', 0, waRequest::TYPE_INT);
        $category = new manufactoryCategoryProducts($id);
        if ($category->getRights() > 1) {
            $right_sidebar = new manufactoryProductsRightSidebarAction(array('backend_products' => $backend_products));
            $this->view->assign('right_sidebar', $right_sidebar->display(false));
        }

        $this->view->assign('backend_products',
            $backend_products
        );
    }
}
// EOF