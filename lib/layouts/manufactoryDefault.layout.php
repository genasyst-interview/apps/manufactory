<?php

class manufactoryDefaultLayout extends waLayout
{
    protected $template = 'Default';

    public function execute()
    {
        /* $plugins = array();
         $backend_sidebar = array(
             'menu'=> array()
         );
         wa('manufactory')->event('backend_sidebar', $plugins);
         var_dump($plugins);
         foreach ($plugins as $data) {
             foreach (array_keys($backend_sidebar) as $v) {
                 if(array_key_exists($v, $data)) {
                     $backend_sidebar[$v][] = $data[$v];
                 }
             }
         }*/
        $params = null;
        $backend_sidebar = wa('manufactory')->event('backend_sidebar', $params, array('menu'));
        $this->view->assign('backend_sidebar',
            $backend_sidebar
        );
    }
}

// EOF