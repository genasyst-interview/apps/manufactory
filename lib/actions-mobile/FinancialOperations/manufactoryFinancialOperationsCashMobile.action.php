<?php

class manufactoryFinancialOperationsCashMobileAction extends manufactoryMobileViewAction
{
    protected $cashExpenseFTModel = null;
    protected $cashIncomeFTModel = null;
    protected $categoryModel = null;

    protected $start_timestamp = 0;
    protected $end_timestamp = 0;

    public function execute()
    {
        $this->setLayout(new manufactoryDefaultLayout());

        $start_date = waRequest::get('start_date');
        $end_date = waRequest::get('end_date');
        if (strtotime($start_date)) {
            $this->start_timestamp = strtotime($start_date);
        }
        if (strtotime($end_date)) {
            $this->end_timestamp = strtotime($end_date);
        }


        $income_data = $this->getTypeData('income');
        $this->view->assign('income_categories', $income_data);
    }

    protected function getTypeData($type = '')
    {
        $categoryModel = new manufactoryFinancialOperationsCategoryModel();
        $categoryModel->setType($type);
        $categoryModel->setPaymentType('cash');
        $cashFOModel = new  manufactoryFinancialOperationCashModel($type);
        if (!empty($this->start_timestamp)) {
            $cashFOModel->setPeriod($this->start_timestamp, $this->end_timestamp);
        }
        $categories = $categoryModel->getCategories();
        foreach ($categories as &$v) {
            if ($v['type'] == 'group') {
                $group_amount = 0;
                $subcategories = $categoryModel->getCategories($v['id']);
                foreach ($subcategories as &$subv) {
                    $category_amount = 0.0;
                    $category_operations = $cashFOModel->getByCategory($subv['id']);
                    foreach ($category_operations as $v) {
                        $category_amount += floatval($v['amount']);
                    }
                    $subv['operations'] = $category_operations;
                    $subv['amount'] = $category_amount;
                    $group_amount += $category_amount;
                }
                unset($subv);
                $v['categories'] = $subcategories;
                $v['amount'] = $group_amount;

            }
        }
        unset($v);

        return $categories;
    }
}
