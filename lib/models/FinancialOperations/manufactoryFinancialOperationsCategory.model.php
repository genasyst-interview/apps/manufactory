<?php

class manufactoryFinancialOperationsCategoryModel extends manufactoryModel
{
    protected $table = 'manufactory_financial_operations_category';
    protected $financial_operations_type = '';
    protected $payment_type = '';

    public function __construct($financial_operations_type = '', $payment_type = '', $connect_type = null, $writable = false)
    {
        if (!empty($financial_operations_type)) {
            $this->setType($financial_operations_type);
        }
        if (!empty($payment_type)) {
            $this->setPaymentType($payment_type);
        }
        parent::__construct($connect_type, $writable);
    }

    public function setType($financial_operations_type)
    {
        $this->financial_operations_type = $financial_operations_type;
    }

    public function setPaymentType($payment_type)
    {
        $this->payment_type = $payment_type;
    }

    public function getCategories($parent_category_id = 0, $active = null)
    {
        $where = array(
            'parent_category_id'                => $parent_category_id,
            'financial_operations_type'         => $this->financial_operations_type,
            'financial_operations_payment_type' => $this->payment_type,
        );
        if (!empty($this->financial_operations_type)) {
        }
        if (!is_null($active)) {
            $where['active'] = intval(boolval($active));
        }
        $categories = $this->getByField($where, 'id');

        return $categories;
    }

    public function save($data, &$errors)
    {
        $data['name'] = trim($data['name']);
        if (empty($data['id'])) {
            if (!empty($data['parent_category_id'])) {
                $parentCategory = $this->getById($data['parent_category_id']);
                if (!empty($parentCategory)) {
                    $data['type'] = 'category';
                    $data['financial_operations_type'] = $parentCategory['financial_operations_type'];
                    $data['financial_operations_payment_type'] = $parentCategory['financial_operations_payment_type'];
                } else {
                    $data['parent_category_id'] = 0;
                }
            } else {
                $data['parent_category_id'] = 0;
            }
            if (!empty($data['type']) && !empty($data['name']) &&
                !empty($data['financial_operations_type']) &&
                !empty($data['financial_operations_payment_type'])
            ) {
                $category = $this->getByField(array(
                    'name'                              => $data['name'],
                    'type'                              => $data['type'],
                    'financial_operations_type'         => $data['financial_operations_type'],
                    'financial_operations_payment_type' => $data['financial_operations_payment_type'],
                    'parent_category_id'                => $data['parent_category_id'],
                ));
                if (!empty($category) && $category['active'] == 0) {
                    $errors['restore'] = 'Такая категория есть, но не активна! Восстановить?';

                    return false;
                } elseif (!empty($category)) {
                    $errors[] = 'Такая категория есть!';

                    return false;
                } else {
                    $id = $this->insert($data);

                    return $id;
                }
            } else {
                $errors[] = 'Переданы не все данные!';

                return false;
            }
        } else {
            unset($data['type']);
            unset($data['financial_operations_type']);
            unset($data['financial_operations_payment_type']);
            unset($data['parent_category_id']);
            $category = $this->getById($data['id']);
            if (!empty($category) && !empty($data['name'])) {
                $this->updateById($data['id'], $data);

                return $data['id'];
            } else {
                $errors[] = 'Передан неверный id категории!';

                return false;
            }
        }
    }

}
