<?php

class manufactoryFinancialOperationsCategoryBalanceModel extends manufactoryPeriodModel

{
    protected $table = 'manufactory_financial_operations_category_balance';
    protected $financial_operations_type = '';
    protected $id = 'category_id';
    protected $payment_type = '';

    protected function getPeriodString()
    {
        return "  year = '{$this->getStartDatetime("Y")}'  AND month = '{$this->getEndDatetime("m")}'  ";
    }

    public function __construct($financial_operations_type = '', $payment_type = '', $connect_type = null, $writable = false)
    {
        if (!empty($financial_operations_type)) {
            $this->setType($financial_operations_type);
        }
        if (!empty($payment_type)) {
            $this->setPaymentType($payment_type);
        }
        parent::__construct($connect_type, $writable);
    }

    public function setType($financial_operations_type)
    {
        $this->financial_operations_type = $financial_operations_type;
    }

    public function setPaymentType($payment_type)
    {
        $this->payment_type = $payment_type;
    }

    public function save($data)
    {
        if (!empty($data['category_id'])) {
            $balance = $this->getById($data['category_id']);
            if ($balance) {
                $this->updateById($balance['category_id'], array('amount' => $data['amount']));
            } else {
                $categoryModel = new manufactoryFinancialOperationsCategoryModel();
                $category = $categoryModel->getById($data['category_id']);
                if ($category) {
                    $data['type'] = $category['type'];
                    $data['financial_operations_type'] = $category['financial_operations_type'];
                    $data['financial_operations_payment_type'] = $category['financial_operations_payment_type'];
                    $data['year'] = $this->getStartDatetime("Y");
                    $data['month'] = $this->getStartDatetime("m");
                    $this->insert($data);
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public function getCategoryBalance($id)
    {
        $balance = $this->getById($id);
        if ($balance) {
            return $balance['amount'];
        } else {
            return 0.0;
        }
    }

    public function getTypeAmount($type = '', $payment_type = '')
    {
        if (empty($type)) {
            $type = $this->financial_operations_type;
        }
        if (empty($payment_type)) {
            $payment_type = $this->payment_type;
        }
        $rr = $this->select('SUM(amount) as sum')
            ->where("year='{$this->getStartDatetime('Y')}'")
            ->where("month='{$this->getStartDatetime('m')}'")
            ->where("financial_operations_type='$type'")
            ->where("financial_operations_payment_type='$payment_type'")
            ->where("type!='group'")->fetchField('sum');

        if ($rr) {
            return floatval($rr);
        } else {
            return 0.0;
        }

    }

    public function getById($value, $period = true)
    {
        return parent::getById($value, $period);
    }


}
