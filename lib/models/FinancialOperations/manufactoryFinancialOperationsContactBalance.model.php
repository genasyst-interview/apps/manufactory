<?php

class manufactoryFinancialOperationsContactBalanceModel extends manufactoryModel

{
    protected $table = 'manufactory_financial_operations_contact_balance';
    protected $payment_type = '';

    public function __construct($payment_type = '', $connect_type = null, $writable = false)
    {
        if (!empty($payment_type)) {
            $this->setPaymentType($payment_type);
        }
        parent::__construct($connect_type, $writable);
    }

    public function setPaymentType($payment_type)
    {
        $this->payment_type = $payment_type;
    }

    public function getByPaymentType($payment_type = '')
    {
        if (empty($payment_type)) {
            $payment_type = $this->payment_type;
        }

        return $this->getByField(array('payment_type' => $payment_type), 'contact_id');
    }

    public function getContactBalance($id, $payment_type = '')
    {
        $balance = $this->getByContactId($id, $payment_type);
        if ($balance) {
            return floatval($balance['amount']);
        } else {
            return 0.0;
        }
    }

    public function getByContactId($id, $payment_type = '')
    {
        if (empty($payment_type)) {
            $payment_type = $this->payment_type;
        }

        return $this->getByField(array('contact_id' => $id, 'payment_type' => $payment_type));
    }

    public function save($contact_id, $amount, $payment_type = '')
    {
        if (empty($payment_type)) {
            $payment_type = $this->payment_type;
        }
        if (!empty($contact_id)) {
            $balance = $this->getByField(
                array('contact_id' => $contact_id, 'payment_type' => $payment_type)
            );
            if ($balance) {
                $this->updateById($balance['id'], array('amount' => $amount));
            } else {
                $data = array(
                    'contact_id'   => $contact_id,
                    'payment_type' => $payment_type,
                    'amount'       => floatval($amount),
                    'default'      => 0,
                );
                $contacts = $this->getByPaymentType($payment_type);
                if (empty($contacts)) {
                    $data['default'] = 1;
                }
                $this->insert($data);
            }
        } else {
            return false;
        }
    }


}
