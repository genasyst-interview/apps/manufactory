<?php

class manufactoryFinancialOperationsBalanceModel extends manufactoryPeriodModel

{
    protected $table = 'manufactory_financial_operations_balance';
    protected $payment_type = '';

    protected function getPeriodString()
    {
        return "  year = '{$this->getStartDatetime("Y")}'  AND month = '{$this->getEndDatetime("m")}'  ";
    }

    public function __construct($payment_type = '', $connect_type = null, $writable = false)
    {
        if (!empty($payment_type)) {
            $this->setPaymentType($payment_type);
        }
        parent::__construct($connect_type, $writable);
    }

    public function setPaymentType($payment_type)
    {
        $this->payment_type = $payment_type;
    }

    public function getByType($type)
    {
        return $this->getByField(array('type' => $type, 'payment_type' => $this->payment_type));
    }

    public function getTypeBalance($type = '')
    {
        $balance = $this->getByType($type);
        if ($balance) {
            return floatval($balance['amount']);
        } else {
            return 0.0;
        }
    }

    public function save($type, $amount)
    {
        if (!empty($type)) {
            $balance = $this->getByType($type);
            if ($balance) {
                $this->updateById($balance['id'], array('amount' => $amount));
            } else {
                $data = array(
                    'year'         => $this->getStartDatetime("Y"),
                    'month'        => $this->getStartDatetime("m"),
                    'amount'       => floatval($amount),
                    'payment_type' => $this->payment_type,
                    'type'         => $type,
                );
                $this->insert($data);
            }
        } else {
            return false;
        }
    }


}
