<?php

class manufactoryFeatureValueModel extends manufactorySortableModel
{
    protected $context = 'feature_id';
    protected $table = 'manufactory_product_feature_value';//manufactory_product_types_features

    public function getByFeature($id, $group_id = null)
    {
        if (empty($group_id)) {
            return $this->getByField('feature_id', $id, 'id');
        } else {
            return $this->getByField(array(
                'feature_id' => $id,
                'group_id'   => $group_id,
            ), 'id');
        }
    }

    public function getByFeatureGroup($feature_id, $group_id)
    {
        return $this->getByField($feature_id, $group_id);
    }
}