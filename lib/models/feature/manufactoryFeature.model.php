<?php

class manufactoryFeatureModel extends manufactoryModelObject
{

    protected $table = 'manufactory_feature';
    protected $object_class_name = 'manufactoryFeature';

    public function getOfferFeatures()
    {
        return $this->select('*')->where('supplier_name NOT LIKE "%custom%"')->getAll();
    }

}
