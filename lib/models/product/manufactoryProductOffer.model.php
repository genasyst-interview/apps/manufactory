<?php

class manufactoryProductOfferModel extends manufactorySortableModel
{
    protected $context = 'product_id';
    protected $table = 'manufactory_product_sku';

    public function getByProduct($id, $type = null)
    {
        if (!empty($type)) {
            return $this->getByField(array(
                'product_id' => $id,
                'type'       => $type,
            ), 'id');
        } else {
            return $this->getByField('product_id', $id, 'id');
        }
    }

}
