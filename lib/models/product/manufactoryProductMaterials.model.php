<?php

class manufactoryProductMaterialsModel extends manufactoryEntityCostsModel
{
    protected $context = 'entity_id';
    protected $table = 'manufactory_product_materials';
    protected $entity_id_column = 'entity_id';
    protected $storage_id_column = 'cost_id';

    public function stagingModel()
    {
        /*return new manufactoryProductMaterialsModel( array (
            'host' => 'localhost',
            'user' => 'staging_sensor1',
            'password' => 'staging_sensor1',
            'database' => 'staging_sensor1',
        ));*/
    }

    public function getByProduct($id, $all = 'id')
    {
        return $this->getByField(array(
            'product_id' => $id,
        ), $all);
    }

    public function deleteByProduct($id)
    {
        return $this->deleteByField(array(
            'entity_id' => $id,
        ));
    }

    public function getProductsByMaterialId($material_id)
    {
        return $this->getByField(array(
            'material_id' => $material_id,
        ), 'product_id');
    }
}
