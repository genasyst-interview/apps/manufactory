<?php

class manufactoryProductSkuModel extends manufactoryEntityProductStorageModel
{

    protected $table = 'manufactory_product_sku';
    protected $context = 'product_id';

    protected $entity_id_column = 'product_id';
    protected $storage_id_column = 'id';


    public function setByEntity($id, $data = array())
    {
        $delete_data = $this->getByEntity($id);
        $change_data = false;
        foreach ($data as $sid => $sku) {
            if (array_key_exists($sid, $delete_data)) {
                if ($delete_data[$id]['name'] != $sku['name']) {
                    $this->updateByField(array(
                        $this->entity_id_column  => $id,
                        $this->storage_id_column => $sid,
                    ), array('name' => $sku['name']));
                    $change_data = true;
                }
                unset($delete_data[$sid]);
            } else {
                $change_data = true;
                $_data = array(
                    $this->entity_id_column => $id,
                    'name'                  => $sku['name'],
                    'sku_data'              => $sku['sku_data'],
                );
                $this->insert($_data);
            }
        }
        foreach ($delete_data as $delete_row) {
            $this->deleteByField(
                $delete_row
            );
        }

        return $change_data;


    }

}
