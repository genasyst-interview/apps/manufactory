<?php

class manufactoryProductFeaturesModel extends manufactoryEntityProductStorageModel
{
    protected $entity_id_column = 'product_id';
    protected $storage_id_column = 'feature_id';
    protected $table = 'manufactory_product_features';

    public function getByProduct($id, $feature_id = 0)
    {
        if (!empty($feature_id)) {
            return $this->getByField(array('product_id' => $id, 'feature_id' => $feature_id), 'value_id');
        } else {
            return $this->select('*')->where('product_id = ' . intval($id) . '')->order('feature_id')->fetchAll('value_id');
            //return $this->getByField(array('product_id'=> $id), 'value_id');
        }
    }

    public function getByEntity($id, $storage_id_column = 'value_id')
    {
        return $this->select('*')->where($this->entity_id_column . ' = ' . intval($id) . '')->order('feature_id')->fetchAll($storage_id_column);
    }

    public function setByEntity($id, $data = array())
    {
        $delete_data = $this->getByEntity($id);
        $old_data = array();
        foreach ($delete_data as $v) {
            $old_data[$v['feature_id']][$v['value_id']] = $v['value_id'];
        }
        $change_data = false;
        foreach ($data as $f_id => $values) {
            foreach ($values as $vid) {
                if (array_key_exists($f_id, $old_data) && array_key_exists($vid, $old_data[$f_id])) {
                    /// Совпадает все оставляем
                    unset($old_data[$f_id][$vid]);
                } else {
                    $_data = array(
                        $this->entity_id_column  => $id,
                        $this->storage_id_column => $f_id,
                        'value_id'               => $vid,
                    );
                    $this->insert($_data);
                    $change_data = true;
                }
            }
        }

        if (!empty($old_data)) {
            $change_data = true;
            foreach ($old_data as $fid => $feature) {
                foreach ($feature as $value) {
                    $this->deleteByField(array(
                        $this->entity_id_column  => $id,
                        $this->storage_id_column => $fid,
                        'value_id'               => $value,
                    ));
                }

            }
        }

        return $change_data;
    }
}
