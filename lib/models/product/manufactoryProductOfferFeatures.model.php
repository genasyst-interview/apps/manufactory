<?php

class manufactoryProductOfferFeaturesModel extends manufactoryModel
{

    protected $table = 'manufactory_product_offer_features';

    public function getByOffer($id)
    {
        return $this->getByField('offer_id', $id, 'feature_id');
    }

    public function deleteByOffer($id)
    {
        return $this->deleteByField('offer_id', $id);
    }
}