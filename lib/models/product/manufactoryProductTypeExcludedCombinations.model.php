<?php

class manufactoryProductTypeExcludedCombinationsModel extends manufactoryModel
{

    protected $table = 'manufactory_product_type_excluded_combinations';

    public function getByType($type_id)
    {
        return $this->getByField(array(
            'product_type_id' => $type_id,
        ), 'index');
    }


}
