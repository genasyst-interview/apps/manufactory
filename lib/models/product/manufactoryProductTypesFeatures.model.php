<?php

class manufactoryProductTypesFeaturesModel extends manufactoryModel
{

    protected $table = 'manufactory_product_types_features';

    public function getByType($id)
    {
        return $this->getByField('type_id', $id, 'id');
    }
}
