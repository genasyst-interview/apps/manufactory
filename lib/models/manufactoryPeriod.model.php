<?php

class manufactoryPeriodModel extends manufactoryModel
{
    protected $start_datetime = null;
    protected $end_datetime = null;
    protected $period_column = 'create_datetime';
    protected $period_column_type = 'datetime';

    public function __construct($connect_type = null, $writable = false)
    {
        $this->setPeriod(time());
        parent::__construct($connect_type, $writable);
    }

    public function setPeriod($start_timestamp = 0, $end_timestamp = 0)
    {
        if (!empty($end_timestamp) && $end_timestamp < $start_timestamp) {
            $end_timestamp = 0;
        }
        $this->start_datetime = new DateTime();
        if (!empty($start_timestamp)) {
            $this->start_datetime->setTimestamp($start_timestamp);
        } else {
            $this->start_datetime->modify('first day of this month');
            $this->start_datetime->setTime(0, 0, 0);
        }

        $this->end_datetime = DateTime::createFromFormat('U', $this->start_datetime->getTimestamp());

        if (!empty($end_timestamp)) {
            $this->end_datetime->setTimestamp($end_timestamp);
        } else {
            $this->end_datetime->modify('last day of this month');
            $this->end_datetime->setTime(23, 59, 59);
        }
    }

    public function getStartDatetime($format = '')
    {
        if (empty($format)) {
            $format = $this->period_column_type;
        }

        return $this->getDatetimeFormat($this->start_datetime, $format);
    }

    public function getEndDatetime($format = '')
    {
        if (empty($format)) {
            $format = $this->period_column_type;
        }

        return $this->getDatetimeFormat($this->end_datetime, $format);
    }

    protected function getDatetimeFormat($datetime_obj, $format)
    {
        if ($format == 'date') {
            return $datetime_obj->format('Y-m-d');
        } elseif ($format == 'datetime') {
            return $datetime_obj->format('Y-m-d H:i:s');
        } elseif ($format == 'timestamp') {
            return $datetime_obj->getTimestamp();
        } else {
            return $datetime_obj->format($format);
        }
    }

    protected function getMysqlDatetimeFormat($format = '')
    {
        if (empty($format)) {
            $format = $this->period_column_type;
        }
        if ($format == 'date') {
            return '%Y-%m-%d';
        } elseif ($format == 'datetime') {
            return '%Y-%m-%d %H:%i:%s';
        } else {
            return false;
        }
    }

    protected function getPeriodString()
    {
        return "  {$this->period_column} BETWEEN   STR_TO_DATE('{$this->getStartDatetime()}', '{$this->getMysqlDatetimeFormat()}')   AND STR_TO_DATE('{$this->getEndDatetime()}', '{$this->getMysqlDatetimeFormat()}')  ";
    }

    public function getByField($field, $value = null, $all = false, $period = null, $limit = null)
    {
        if (is_array($field)) {
            $limit = $period;
            $period = $all;
            $all = $value;
            $value = false;
            if (is_null($period)) {
                $period = true;
            }
            if (is_null($limit)) {
                $limit = false;
            }
        }
        $sql = "SELECT * FROM " . $this->table;
        $where = $this->getWhereByField($field, $value);
        if ($where != '') {
            $sql .= " WHERE " . $where;
        }
        if ($period) {
            $sql .= " AND  " . $this->getPeriodString();
        }
        if ($limit) {
            $sql .= " LIMIT " . (int)$limit;
        } elseif (!$all) {
            $sql .= " LIMIT 1";
        }
        $result = $this->query($sql);
        if ($all) {
            return $result->fetchAll(is_string($all) ? $all : null);
        } else {
            return $result->fetchAssoc();
        }
    }

    public function getById($value, $period = false)
    {
        $all = !is_array($this->id) && is_array($value);
        if (!is_array($this->id)) {
            return $this->getByField($this->id, $value, $all ? $this->id : false, $period);
        } else {
            return $this->getByField($this->remapIds($value), $all ? $this->id : false, $period);
        }
    }

}
