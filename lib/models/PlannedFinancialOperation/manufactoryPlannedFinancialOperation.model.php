<?php

class manufactoryPlannedFinancialOperationModel extends manufactoryModel
{
    protected $table = 'manufactory_planned_financial_operation';
    protected $type = '';
    protected $payment_type = '';
    protected $start_datetime = null;
    protected $end_datetime = null;

    protected $log = null;

    public function __construct($type = '', $payment_type = '', $connect_type = null, $writable = false)
    {
        $this->log = new manufactoryFinancialOperationLogModel();
        if (!empty($type)) {
            $this->type = $type;
        }
        if (!empty($payment_type)) {
            $this->payment_type = $payment_type;
        }
        $this->setPeriod(date('Y'), date('m'));
        parent::__construct($connect_type, $writable);
    }

    public function setPeriod($start_year = 0, $start_month = 0, $end_year = 0, $end_month = 0)
    {
        $this->start_datetime = new DateTime();
        if (!empty($start_year) && !empty($start_month)) {
            $this->start_datetime->setDate($start_year, $start_month, 1);
        }
        $this->start_datetime->setTime(0, 0, 1);
        $this->end_datetime = DateTime::createFromFormat('U', $this->start_datetime->getTimestamp());
        if (!empty($end_year) && !empty($end_month)) {
            $this->end_datetime->setDate($end_year, $end_month, 1);
        } else {
            $this->end_datetime->modify('+1 month');
        }
        /* var_dump($this->start_datetime->format('Y-m-d H:i:s'));
         var_dump($this->end_datetime->format('Y-m-d H:i:s'));*/
    }

    public function save($data)
    {
        if (!empty($this->type)) {
            $data['type'] = $this->type;
        }
        if (!empty($this->type)) {
            $data['payment_type'] = $this->payment_type;
        }
        if (isset($data['id']) && $this->getById($data['id'])) {
            $this->updateById($data['id'], $data);

            return $data['id'];
        } else {
            return $this->insert($data);
        }
    }

    public function getByCategory($id)
    {

    }

    public function getByType($type = '')
    {

    }


}
