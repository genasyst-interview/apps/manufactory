<?php

class manufactoryFinancialOperationLogModel extends manufactoryModel
{
    protected $table = 'manufactory_financial_operation_log';

    public function add($financial_operation_id, $action)
    {
        $user_id = wa()->getUser()->getId();
        $data = array(
            'financial_operation_id' => $financial_operation_id,
            'action'                 => $action,
            'contact_id'             => $user_id,
            'create_datetime'        => date("Y-m-d H:i:s"),
        );
        $this->insert($data);
    }

}
