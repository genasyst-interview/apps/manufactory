<?php

class manufactoryFinancialOperationModel extends manufactoryPeriodModel
{
    protected $table = 'manufactory_financial_operation';
    protected $financial_operations_type = '';
    protected $payment_type = '';

    protected $log = null;

    public function getAmount($type = '', $payment_type = '')
    {
        if (empty($type)) {
            $type = $this->financial_operations_type;
        }
        if (empty($payment_type)) {
            $payment_type = $this->payment_type;
        }
        $amount = $this->select('SUM(amount) as sum')
            ->where("type='$type'")
            ->where("payment_type = '$payment_type'")
            ->where($this->getPeriodString())
            ->fetchField('sum');
        if ($amount) {
            return floatval($amount);
        } else {
            return 0.0;
        }
    }

    public function __construct($financial_operations_type = '', $payment_type = '', $connect_type = null, $writable = false)
    {
        //$this->log = new manufactoryFinancialOperationLogModel();
        if (!empty($financial_operations_type)) {
            $this->financial_operations_type = $financial_operations_type;
        }
        if (!empty($payment_type)) {
            $this->payment_type = $payment_type;
        }
        parent::__construct($connect_type, $writable);
    }

    /**
     * @param string $payment_type
     */
    public function setPaymentType($payment_type)
    {
        $this->payment_type = $payment_type;
    }

    public function getOperations($field = 'id')
    {
        return $this->getByField(array(
            'type'         => $this->financial_operations_type,
            'payment_type' => $this->payment_type,
        ), $field, true);
    }

    public function getLastContactOperations($contact_id)
    {
        $operations = $this->getByField(array(
            'payment_type' => $this->payment_type,
            'contact_id'   => $contact_id,
        ), 'id', true, '10');
        foreach ($operations as $k => $v) {
            $operations[$k]['create_date'] = date('d.m.Y', strtotime($v['create_datetime']));
            $operations[$k]['operation_date'] = date('d.m.Y', strtotime($v['operation_datetime']));
        }

        return $operations;
    }

    public function getByCategory($category_id)
    {
        if (!empty($category_id)) {

            return $this->getByField(array(
                'type'         => $this->financial_operations_type,
                'payment_type' => $this->payment_type,
                'category_id'  => $category_id,
            ), 'id');
        } else {
            return array();
        }
    }

    public function save($data, &$errors)
    {
        $data['amount'] = abs(round(floatval($data['amount']), 2));
        if (!empty($data['id'])) {
            $operation = $this->getById($data['id']);
            $FOBalanceModel = new manufactoryFinancialOperationsContactBalanceModel($operation['payment_type']);
            if (!empty($operation)) {
                unset($data['payment_type']);
                unset($data['type']);
                unset($data['category_id']);
                unset($data['contact_id']);
                unset($data['create_datetime']);
                if ($operation['type'] == 'expense') {
                    $data['amount'] = -$data['amount'];
                } else {

                }
                $difference = floatval($data['amount']) - floatval($operation['amount']);
                $contact_balance = $FOBalanceModel->getContactBalance($operation['contact_id']);
                $contact_balance = $contact_balance + $difference;
                $FOBalanceModel->save($operation['contact_id'], $contact_balance);

                $data['edit_datetime'] = date("Y-m-d H:i:s");
                $this->updateById($data['id'], $data);

                return $data['id'];
            } else {
                $errors[] = 'Неверный ID операции!';
            }
        } elseif (!empty($data['category_id']) && !empty($data['name']) && isset($data['amount'])) {
            $FOCategoryModel = new manufactoryFinancialOperationsCategoryModel();
            $category = $FOCategoryModel->getById($data['category_id']);
            $data['type'] = $category['financial_operations_type'];
            $data['payment_type'] = $category['financial_operations_payment_type'];
            $data['operation_datetime'] = $data['create_datetime'] = date("Y-m-d H:i:s");
            $data['contact_id'] = wa()->getUser()->getId();
            if ($data['type'] == 'expense') {
                $data['amount'] = -$data['amount'];
            }
            $id = $this->insert($data);
            $operation = $this->getById($id);

            $FOBalanceModel = new manufactoryFinancialOperationsContactBalanceModel($operation['payment_type']);
            $difference = floatval($operation['amount']);
            $contact_balance = $FOBalanceModel->getContactBalance($operation['contact_id']);
            $contact_balance = $contact_balance + $difference;
            $FOBalanceModel->save($operation['contact_id'], $contact_balance);

            return $id;
        } else {
            $errors[] = 'Нехватает данных';
        }

        return false;
    }


}
