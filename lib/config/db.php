<?php
return array(
    'manufactory_financial_operation'                   => array(
        'id'                 => array('int', 11, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'name'               => array('text', 'null' => 0),
        'description'        => array('text', 'null' => 0),
        'amount'             => array('double', 'null' => 0),
        'create_datetime'    => array('datetime', 'null' => 0),
        'operation_datetime' => array('datetime', 'null' => 0),
        'edit_datetime'      => array('datetime'),
        'type'               => array('enum', "'expense','income'", 'null' => 0),
        'payment_type'       => array('enum', "'cash','cashless'", 'null' => 0),
        'category_id'        => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'contact_id'         => array('int', 10, 'unsigned' => 1, 'null' => 0),
        ':keys'              => array(
            'PRIMARY' => 'id',
        ),
    ),
    'manufactory_financial_operations_balance'          => array(
        'id'           => array('int', 10, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'year'         => array('smallint', 5, 'unsigned' => 1, 'null' => 0),
        'month'        => array('smallint', 5, 'unsigned' => 1, 'null' => 0),
        'amount'       => array('double', 'null' => 0),
        'type'         => array('enum', "'income','expense','all'", 'null' => 0),
        'payment_type' => array('enum', "'cash','cashless'", 'null' => 0),
        ':keys'        => array(
            'PRIMARY' => 'id',
        ),
    ),
    'manufactory_financial_operations_category'         => array(
        'id'                                => array('int', 10, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'name'                              => array('varchar', 255, 'null' => 0),
        'parent_category_id'                => array('int', 10, 'unsigned' => 1, 'null' => 0),
        'type'                              => array('enum', "'group','category'", 'null' => 0),
        'financial_operations_type'         => array('enum', "'expense','income'", 'null' => 0),
        'financial_operations_payment_type' => array('enum', "'cash','cashless'", 'null' => 0),
        'active'                            => array('tinyint', 1, 'unsigned' => 1, 'null' => 0),
        ':keys'                             => array(
            'PRIMARY' => 'id',
        ),
    ),
    'manufactory_financial_operations_category_balance' => array(
        'id'                                => array('int', 10, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'year'                              => array('smallint', 5, 'unsigned' => 1, 'null' => 0),
        'month'                             => array('smallint', 5, 'unsigned' => 1, 'null' => 0),
        'category_id'                       => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'financial_operations_type'         => array('enum', "'income','expense'", 'null' => 0),
        'financial_operations_payment_type' => array('enum', "'cash','cashless'", 'null' => 0),
        'amount'                            => array('double', 'null' => 0),
        'type'                              => array('enum', "'category','group'", 'null' => 0),
        ':keys'                             => array(
            'PRIMARY' => 'id',
        ),
    ),
    'manufactory_financial_operations_contact_balance'  => array(
        'id'           => array('int', 10, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'contact_id'   => array('int', 10, 'unsigned' => 1, 'null' => 0),
        'amount'       => array('double', 'null' => 0),
        'payment_type' => array('enum', "'cash','cashless'", 'null' => 0),
        'default'      => array('tinyint', 1, 'null' => 0),
        ':keys'        => array(
            'PRIMARY' => 'id',
        ),
    ),
    'manufactory_financial_operation_log'               => array(
        'id'                     => array('int', 10, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'financial_operation_id' => array('int', 10, 'unsigned' => 1, 'null' => 0),
        'action'                 => array('varchar', 255, 'null' => 0),
        'contact_id'             => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'create_datetime'        => array('datetime', 'null' => 0),
        ':keys'                  => array(
            'PRIMARY' => 'id',
        ),
    ),
    'manufactory_financial_operation_transfer_contact'  => array(
        'id'                   => array('int', 11, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'sender_contact_id'    => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'recipient_contact_id' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'amount'               => array('double unsigned', 'null' => 0),
        'contact_id'           => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'payment_type'         => array('enum', "'cash','cashless'", 'null' => 0),
        'create_datetime'      => array('datetime', 'null' => 0),
        ':keys'                => array(
            'PRIMARY' => 'id',
        ),
    ),
    'manufactory_planned_financial_operation'           => array(
        'id'              => array('int', 11, 'null' => 0, 'autoincrement' => 1),
        'name'            => array('text', 'null' => 0),
        'description'     => array('text', 'null' => 0),
        'create_datetime' => array('datetime', 'null' => 0),
        'edit_datetime'   => array('datetime'),
        'type'            => array('enum', "'expense','income'", 'null' => 0),
        'payment_type'    => array('enum', "'cash','cashless'", 'null' => 0),
        'amount'          => array('double', 'null' => 0, 'default' => '0'),
        'category_id'     => array('int', 11, 'null' => 0, 'default' => '0'),
        ':keys'           => array(
            'PRIMARY' => 'id',
        ),
    ),
    'manufactory_category_materials'                    => array(
        'id'        => array('int', 11, 'null' => 0, 'autoincrement' => 1),
        'name'      => array('varchar', 255, 'null' => 0),
        'group_id'  => array('int', 11, 'null' => 0),
        'parent_id' => array('int', 11, 'null' => 0),
        'sort'      => array('int', 11, 'null' => 0),
        ':keys'     => array(
            'PRIMARY' => 'id',
        ),
    ),//////
    'manufactory_category_products'                     => array(
        'id'        => array('int', 11, 'null' => 0, 'autoincrement' => 1),
        'name'      => array('varchar', 255, 'null' => 0),
        'type'      => array('tinyint', 1, 'null' => 0),
        'parent_id' => array('int', 11, 'null' => 0),
        'sort'      => array('int', 11, 'null' => 0),
        ':keys'     => array(
            'PRIMARY' => 'id',
        ),
    ),/////////
    'manufactory_material'                              => array(
        'id'          => array('int', 11, 'null' => 0, 'autoincrement' => 1),
        'name'        => array('varchar', 255, 'null' => 0),
        'price'       => array('float', 'null' => 0),
        'sizes_price' => array('text', 'null' => 0),
        'category_id' => array('int', 11, 'null' => 0),
        'sort'        => array('int', 11, 'null' => 0),
        ':keys'       => array(
            'PRIMARY' => 'id',
        ),
    ),////////
    'manufactory_margin_product'                        => array(
        'type'       => array('varchar', 255, 'null' => 0, 'default' => 'generate.ratio'),
        'sort'       => array('int', 11, 'null' => 0, 'default' => '0'),
        'round_to'   => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'round_type' => array('tinyint', 4, 'null' => 0, 'default' => '1'),
        'round'      => array('tinyint', 4, 'null' => 0),
        'id'         => array('int', 11, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'name'       => array('varchar', 255, 'null' => 0),
        'koef'       => array('text', 'null' => 0),
        ':keys'      => array(
            'PRIMARY' => 'id',
        ),
    ),//////
    'manufactory_product'                               => array(
        'id'           => array('int', 11, 'null' => 0, 'autoincrement' => 1),
        'name'         => array('varchar', 255, 'null' => 0),
        'group_id'     => array('int', 11, 'null' => 0),
        'category_id'  => array('int', 11, 'null' => 0),
        'sizes_id'     => array('text', 'null' => 0),
        'materials_id' => array('text', 'null' => 0),
        ':keys'        => array(
            'PRIMARY' => 'id',
        ),
    ),////////
    'manufactory_group_products'                        => array(
        'id'    => array('int', 11, 'null' => 0, 'autoincrement' => 1),
        'name'  => array('varchar', 255, 'null' => 0),
        'value' => array('varchar', 255, 'null' => 0),
        ':keys' => array(
            'PRIMARY' => 'id',
        ),
    ),//////
    'manufactory_product_offer'                         => array(
        'name'       => array('varchar', 255, 'null' => 0),
        'id'         => array('int', 11, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'product_id' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'size_id'    => array('int', 11, 'unsigned' => 1, 'null' => 0),
        ':keys'      => array(
            'PRIMARY' => 'id',
        ),
    ),///////
    'manufactory_product_offer_price'                   => array(
        'id'       => array('int', 11, 'unsigned' => 1, 'null' => 0, 'autoincrement' => 1),
        'offer_id' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'price_id' => array('int', 11, 'unsigned' => 1, 'null' => 0),
        'price'    => array('double', 'null' => 0),
        ':keys'    => array(
            'PRIMARY' => 'id',
        ),
    ),//////
    'manufactory_size'                                  => array(
        'id'        => array('int', 11, 'null' => 0, 'autoincrement' => 1),
        'name'      => array('varchar', 255, 'null' => 0),
        'groups_id' => array('int', 11, 'null' => 0),
        ':keys'     => array(
            'PRIMARY' => 'id',
        ),
    ),//////
    'manufactory_group_sizes'                           => array(
        'id'    => array('int', 11, 'null' => 0, 'autoincrement' => 1),
        'name'  => array('varchar', 255, 'null' => 0),
        'sort'  => array('int', 11, 'null' => 0),
        ':keys' => array(
            'PRIMARY' => 'id',
        ),
    )///
);
