<?php
return array(
    'name'     => 'Фабрика',
    'prefix'   => 'manufactory',
    'icon'     => array(
        48 => 'img/enterprise48.png',
        96 => 'img/enterprise96.png',
    ),
    'rights'   => true,
    'mobile'   => true,
    'plugins'  => true,
    'version'  => '1.0.0',
    'critical' => '1.0.0',
    'vendor'   => '990614',
);