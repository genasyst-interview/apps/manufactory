<?php

class manufactoryRightConfig extends waRightConfig
{
    const RIGHT_NONE = 0;
    const RIGHT_READ = 1;
    const RIGHT_EDIT = 2;
    const RIGHT_FULL = 3;

    const PRODUCT_COST_RIGHT_NONE = 0;
    const PRODUCT_COST_RIGHT_ALLOW = 1;

    public function init()
    {
        $this->addItem('settings', 'Настройки');

        // Доступ к ценам
        $this->addItem('importexport_header', 'Импорт/Экспорт', 'header', array(
            'position' => 'left',
        ));

        $this->addItem('importexport_1c', 'Выгрузки 1С');
        $this->addItem('importexport_prices', 'Прайсы');
        $this->addItem('importexport_composition', 'Состав');


        $this->addItem('costs_header', 'Затраты', 'header', array(
            'position' => 'left',
        ));
        $types = wa('manufactory')->getConfig()->getFactory('costs')->getTypes();
        $cost_types = array();
        foreach ($types as $v) {
            $cost_types[$v->getTypeId()] = $v->getName();
        }


        // Доступ к затратам
        /*$this->addItem('costs_header', 'Затраты', 'header', array(
            'position'	 => 'left',
        ));*/

        $this->addItem('costs', 'Затраты', 'select', array(
            'position' => 'left',
            'options'  => array(
                self::RIGHT_NONE => 'Нет доступа',
                self::RIGHT_READ => 'Только просмотр',
                //self::RIGHT_EDIT =>  'Создание и их редактирование',
                self::RIGHT_FULL => 'Полный доступ',
            ),
        ));
        $this->addItem('productcost', 'Добавление затрат продукту', 'selectlist', array(
            'items'    => $cost_types,
            'position' => 'left',
            'options'  => array(
                self::PRODUCT_COST_RIGHT_NONE  => 'Запрещено',
                self::PRODUCT_COST_RIGHT_ALLOW => 'Разрешено',
            ),
        ));

        $this->addItem('costs_header', 'Продукты', 'header', array(
            'position' => 'left',
        ));
        // Доступ к категориям
        $model_category_products = new manufactoryCategoryProductsModel();
        $data = $model_category_products->select('id,name')
            ->where('parent_id=0')
            ->fetchAll('id', true);
        $this->addItem('product_category', 'Категории продуктов', 'selectlist', array(
            'items'    => $data,
            'position' => 'left',
            'options'  => array(
                self::RIGHT_NONE => 'Нет доступа',
                self::RIGHT_READ => 'Только просмотр',
                self::RIGHT_EDIT => 'Создание и их редактирование',
            ),
            'hint1'    => 'all_select',
        ));

        // Доступ к ценам
        $this->addItem('margin_header', 'Наценки', 'header', array(
            'position' => 'left',
        ));
        $model_margin_products = new manufactoryMarginProductModel();
        $data = $model_margin_products->select('id,name')->fetchAll('id', true);

        // Цена по которой считается для показа себестоимости при просмотре состава
        $this->addItem('contact_price', 'Ценобразование состава продукта', 'select', array(
            'position' => 'left',
            'options'  => $data,
        ));
        $this->addItem('margin_product', 'Наценки', 'list', array(
            'items'    => $data,
            'position' => 'left',
            'hint1'    => 'all_checkbox',
        ));
        /**
         * @event rights.config
         *
         * @param waRightConfig $this Rights setup object
         *
         * @return void
         */
        wa()->event('rights.config', $this);
    }
}
