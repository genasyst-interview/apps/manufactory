<?php

class manufactoryConfig extends waAppConfig
{
    protected $models_pool = null;
    protected $_factories = array();

    public function __construct($environment, $root_path, $application, $locale)
    {
        parent::__construct($environment, $root_path, $application, $locale);
        $this->models_pool = new manufactoryModelsPool();
    }

    public function getFactory($name)
    {
        if (array_key_exists($name, $this->_factories)) {
            return $this->_factories[$name];
        }
        $class = 'manufactoryFactory' . ucfirst($name);
        if (class_exists($class)) {
            $this->_factories[$name] = new $class();

            return $this->_factories[$name];
        } else {
            return parent::getFactory($name);
        }
    }

    public function getModel($name)
    {
        return $this->models_pool->get($name);
    }
}

