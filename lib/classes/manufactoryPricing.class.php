<?php

class manufactoryPricing
{
    protected $prices = array();
    protected $categories = array();

    /*
     * У вас там стоит слайдер картинок продукта, значит придется подменить картинки продукта на картинки артикулов, также необходимо будет  делать скрытый выбор характеристик по артикулу, что не так просто реализовать., поэтому такая доработка будет стоить около 2000 рублей. Но идея у вас классная*/
    public function __construct()
    {
        $this->setPrices();
    }

    public function setPrices($data = array())
    {
        $this->prices = array();
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                if (wa()->getUser()->getRights(wa()->getApp(), 'margin_product.' . $v)) {
                    $this->prices[$v] = manufactoryPricesPool::get($v);
                }
            }
        } else {
            $models_pool = manufactoryModelsPool::getInstance();
            $model = $models_pool->get('MarginProduct');
            $prices = $model->query("SELECT id FROM " . $model->getTableName() . " ORDER BY sort ")->fetchAll('id');
            foreach ($prices as $price) {
                if (wa()->getUser()->getRights(wa()->getApp(), 'margin_product.' . $price['id'])) {
                    $this->prices[$price['id']] = manufactoryPricesPool::get($price['id']);
                }
            }
        }
    }

    public function getProductTypes()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('ProductType');

        return $model->getAll('id');
    }

    public function getPrices()
    {
        return $this->prices;
    }

    public function setCategories($data = array())
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CategoryProducts');
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $this->categories[$v] = $model->getById($v);
            }
        } else {
            $this->categories = $model->getAll('id');
        }
    }

    public function getProductCategory($id)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CategoryProducts');

        return $model->getById($id);

    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function getSubCategories($id)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CategoryProducts');

        return $model->select('*')->where('parent_id=' . intval($id))->order('sort')->fetchAll('id');
    }

    public function getPricingGroups()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('GroupProducts');

        return $model->getAll('id');
    }

    public function getCategoryMaterials($id)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('Material');
        $data = $model->select('id')->where('category_id = ' . intval($id))->order('sort')->fetchAll('id');
        $return = array();
        foreach ($data as $v) {
            $return[$v['id']] = manufactoryMaterialsPool::get($v['id']);
        }

        return $return;
    }

    public function getMaterialsCategories()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CategoryMaterials');
        $model->select('*')->order('sort')->fetchAll('id');

        return $model->select('*')->order('sort')->fetchAll('id');
    }

    public function setMaterialsColumn()
    {
        return array();
    }

    public static function getSkuIndexName($sku_index)
    {
        return manufactoryFeaturesSkusGenerate::getSkuIndexName($sku_index);
    }

    public static function getProduct($id)
    {
        return manufactoryProducts::getById($id);
    }

    public static function getOffer($id)
    {
        return new manufactoryProductSku(null, $id);
    }

    public function getCategoryProducts($id)
    {
        return manufactoryProducts::getByCategoryId($id);
    }

    public static function getFeaturesSkus()
    {
        $gen = new manufactoryFeaturesSkusGenerate();

        return $gen->all();
    }

    public static function categoriesPrintThree($id)
    {

        $subcategories = manufactoryPricing::getSubCategories($id);
        if (is_array($subcategories) && !empty($subcategories)) {
            $return = '<ul>';
            foreach ($subcategories as $k => $v) {
                $podcat = self::categoriesPrintThree($k);
                if (!empty($podcat)) {
                    $return .= '<li>' . $v['name'] . '' . $podcat . '</li>';
                } else {
                    $return .= '<li>
				<label for="[' . $k . ']"><input id="[' . $k . ']" name="categories[]" type="checkbox" value="' . $k . '" >' . $v['name'] . '</label><br></li>';
                }

            }
            $return .= '</ul>';
        } else {
            $return = "";
        }

        return $return;
    }
}