<?php

class manufactoryFactoryProducts
{

    public function get($data)
    {
        $product_data = false;
        if (is_array($data)) {
            $product_data = $data;
        } elseif (is_numeric($data) && intval($data) > 0) {
            $model = manufactoryModelsPool::getInstance()->get('Product');
            $product_data = $model->getById($data);
        }
        if (!empty($product_data)) {
            return manufactoryProductTypes::getInstance()->getType($product_data['type_id'])->getProduct($product_data);
        }

        return false;
    }

    public function getType($type_id)
    {
        return manufactoryProductTypes::getInstance()->getType($type_id);
    }
}
 