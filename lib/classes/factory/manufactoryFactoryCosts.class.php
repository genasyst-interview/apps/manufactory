<?php

class manufactoryFactoryCosts extends manufactorySingletone implements manufactoryCostTypesInterface
{
    protected static $_instance = null;
    protected static $types = null;
    protected static $base_types = array(
        'materials' => 'manufactoryCostsTypeMaterials',
    );

    public function __construct()
    {
    }


    public function get($type, $data)
    {
        $type = $this->getType($type);
        if ($type) {
            return $type->getCost($data);
        }

        return false;
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        if (self::$types == null) {
            self::$types = array();
            foreach (self::$base_types as $type_id => $class) {
                $this->set($type_id, $class);
            }
            wa('manufactory')->event('costs', $this);
        }

        return self::$types;
    }

    public function set($name, $class)
    {
        if (is_object($class)) {
            self::$types[$name] = $class;
        } else if (is_string($class)) {
            self::$types[$name] = new $class();
        }
    }

    /*
     * @return manufactoryCostsType
    */
    public function getType($cost_type = '')
    {
        $types = self::getTypes();
        if (array_key_exists($cost_type, $types)) {
            return $types[$cost_type];
        } else {
            return false;
        }
    }
}