<?php

class manufactoryFinancialOperations
{
    protected $start_datetime = null;
    protected $end_datetime = null;
    protected $set_period = false; // изменен ли период или дефолтный

    public function __construct()
    {
        $this->setPeriod(time());
        $this->set_period = false;
    }

    public function setPeriod($start_timestamp = 0, $end_timestamp = 0)
    {
        if (!empty($end_timestamp) && $end_timestamp < $start_timestamp) {
            $end_timestamp = 0;
        }
        $this->start_datetime = new DateTime();
        if (!empty($start_timestamp)) {
            $this->start_datetime->setDate(date("Y", $start_timestamp), date("m", $start_timestamp), 1);
        }
        $this->start_datetime->setTime(0, 0, 0);
        $this->end_datetime = DateTime::createFromFormat('U', $this->start_datetime->getTimestamp());
        if (!empty($end_timestamp)) {
            $this->end_datetime->setDate(date("Y", $end_timestamp), date("m", $end_timestamp), 1);
        } else {
            $this->end_datetime->modify('last day of this month');
            $this->end_datetime->setTime(23, 59, 59);
        }
        $this->set_period = true;
    }

    public function getStartDatetime($format)
    {
        return $this->getDatetimeFormat($this->start_datetime, $format);
    }

    public function getEndDatetime($format)
    {
        return $this->getDatetimeFormat($this->end_datetime, $format);
    }

    protected function getDatetimeFormat($datetime_obj, $format)
    {
        if ($format == 'date') {
            return $datetime_obj->format('Y-m-d');
        } elseif ($format == 'datetime') {
            return $datetime_obj->format('Y-m-d H:i:s');
        } elseif ($format == 'timestamp') {
            return $datetime_obj->getTimestamp();
        } else {
            return $datetime_obj->format($format);
        }
    }

    public function isSetPeriod()
    {
        return $this->set_period;
    }

    public function isDateInPeriod($date)
    {
        $date = new DateTime($date);
        if ($this->getStartDatetime('timestamp') < $date->getTimestamp() && $this->getEndDatetime('timestamp') > $date->getTimestamp()) {
            return true;
        }

        return false;
    }

    public function getFinancialOperationModel($type = '', $payment_type = '')
    {
        $FOModel = new manufactoryFinancialOperationModel($type, $payment_type);
        $FOModel->setPeriod($this->getStartDatetime('timestamp'), $this->getEndDatetime('timestamp'));

        return $FOModel;
    }

    public function getFinancialOperationsCategoryModel($type = '', $payment_type = '')
    {
        return new manufactoryFinancialOperationsCategoryModel($type, $payment_type);
    }

    public function getFinancialOperationsCategoryBalanceModel($type = '', $payment_type = '')
    {
        $FOBalanceModel = new manufactoryFinancialOperationsCategoryBalanceModel($type, $payment_type);
        $FOBalanceModel->setPeriod($this->getStartDatetime('timestamp'), $this->getEndDatetime('timestamp'));

        return $FOBalanceModel;
    }

    public function getFinancialOperationsContactBalanceModel($payment_type = '')
    {
        $FOBalanceModel = new manufactoryFinancialOperationsContactBalanceModel($payment_type);

        return $FOBalanceModel;
    }

    public function getFinancialOperationsBalanceModel($payment_type = '')
    {
        $FOBalanceModel = new manufactoryFinancialOperationsBalanceModel($payment_type);
        $FOBalanceModel->setPeriod($this->getStartDatetime('timestamp'), $this->getEndDatetime('timestamp'));

        return $FOBalanceModel;
    }

    public function recalculateCategory($id)
    {
        $FOCategoryModel = new manufactoryFinancialOperationsCategoryModel();
        $FOBalanceModel = $this->getFinancialOperationsCategoryBalanceModel();
        $category = $FOCategoryModel->getById($id);
        $FOCategoryModel->setType($category['financial_operations_type']);
        $FOCategoryModel->setPaymentType($category['financial_operations_payment_type']);
        $category_amount = 0.0;
        if ($category && $category['type'] == 'group') {
            $categories = $FOCategoryModel->getCategories($id);
            foreach ($categories as $v) {
                $balance = $FOBalanceModel->getById($v['id']);
                $category_amount += floatval($balance['amount']);
            }
        } else {
            $FOModel = $this->getFinancialOperationModel($category['financial_operations_type'], $category['financial_operations_payment_type']);
            $operations = $FOModel->getByCategory($category['id']);
            foreach ($operations as $v) {
                $category_amount += floatval($v['amount']);
            }
        }
        $category_amount = abs($category_amount);
        if ($category['financial_operations_type'] == 'expense') {
            $category_amount = -$category_amount;
        }
        $data = array(
            'category_id' => $category['id'],
            'amount'      => $category_amount,
        );
        $FOBalanceModel->save($data);
        if ($category['parent_category_id'] > 0) {
            $this->recalculateCategory($category['parent_category_id']);
        } else {
            $this->recalculateBalances($category['financial_operations_payment_type']);
        }
    }

    public function recalculateBalances($payment_type = '')
    {
        $income_amount = $this->recalculateBalance('income', $payment_type);
        $expense_amount = $this->recalculateBalance('expense', $payment_type);
        $all_amount = $income_amount + $expense_amount;
        $FOBModel = $this->getFinancialOperationsBalanceModel($payment_type);
        $FOBModel->save('all', $all_amount);
    }

    public function recalculateBalance($type, $payment_type)
    {
        $FOBModel = $this->getFinancialOperationsBalanceModel($payment_type);
        $amount = $this->getTypeAmount($type, $payment_type);
        $FOBModel->save($type, $amount);

        return $FOBModel->getTypeBalance($type);
    }

    public function saveOperation($data, &$errors)
    {
        if ($this->isSetPeriod()) {
            $errors[] = 'Вы не можете редактировавать операции старых периодов!';

            return false;
        }
        if (isset($data['operation_datetime']) && !$this->isDateInPeriod($data['operation_datetime'])) {
            $errors[] = 'Вы не можете указывать другой период!';

            return false;
        }
        $FOModel = $this->getFinancialOperationModel();
        if ($data['id']) {
            $operation = $FOModel->getById($data['id']);
            if ($operation && $this->isDateInPeriod($operation['operation_datetime'])) {
                $id = $FOModel->save($data, $errors);
            } else {
                $errors[] = 'Вы не можете редактировавать операции старых периодов!';

                return false;
            }
        } else {
            $id = $FOModel->save($data, $errors);
        }
        if ($id) {
            $operation = $FOModel->getById($id);
            $this->recalculateCategory($operation['category_id']);

            return $id;
        } else {
            return false;
        }

    }

    public function getPeriodData($type = '', $payment_type = '')
    {
        $categoryModel = $this->getFinancialOperationsCategoryModel($type, $payment_type);
        $FOModel = $this->getFinancialOperationModel($type, $payment_type);
        $operations = $FOModel->getOperations();
        $categories = $categoryModel->getCategories();
        foreach ($categories as &$v) {
            if ($v['type'] == 'group') {
                $subcategories = $categoryModel->getCategories($v['id']);
                foreach ($subcategories as &$subv) {
                    $category_operations = $FOModel->getByCategory($subv['id']);
                    foreach ($category_operations as $opv) {
                        unset($operations[$opv['id']]);
                    }
                    $subv['operations'] = $category_operations;
                    $subv['amount'] = $this->getFinancialOperationsCategoryBalanceModel()->getCategoryBalance($subv['id']);
                }
                unset($subv);
                $v['categories'] = $subcategories;
                $v['amount'] = $this->getFinancialOperationsCategoryBalanceModel()->getCategoryBalance($v['id']);
            } else {
                $category_operations = $FOModel->getByCategory($v['id']);
                foreach ($category_operations as $opv) {
                    unset($operations[$opv['id']]);
                }
                $v['operations'] = $category_operations;
                $v['amount'] = $this->getFinancialOperationsCategoryBalanceModel()->getCategoryBalance($v['id']);
            }
        }
        $data = array();
        $data['categories'] = $categories;
        unset($v);

        return $data;
    }

    public function getCategoryAmount($id)
    {
        $FOBalanceModel = $this->getFinancialOperationsCategoryBalanceModel();

        return $FOBalanceModel->getCategoryBalance($id);
    }

    public function getTypeAmount($type, $payment_type)
    {
        $FOBalancesModel = $this->getFinancialOperationModel($type, $payment_type);

        return $FOBalancesModel->getAmount();
    }

    public function getBalances($payment_type)
    {
        $FOBModel = $this->getFinancialOperationsBalanceModel($payment_type);
        $balances = array();
        $balances['income'] = $FOBModel->getTypeBalance('income');
        $balances['expense'] = $FOBModel->getTypeBalance('expense');
        $balances['all'] = $FOBModel->getTypeBalance('all');
        $balances['contacts'] = $this->getContactsBalances($payment_type);

        return $balances;

    }

    public function getContactsBalances($payment_type)
    {
        $Model = $this->getFinancialOperationsContactBalanceModel($payment_type);

        return $Model->getByPaymentType($payment_type);
    }
}
