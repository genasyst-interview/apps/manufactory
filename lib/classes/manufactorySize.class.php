<?php

class manufactorySize implements ArrayAccess
{
    protected $data = array();

    public function __construct($data)
    {
        if (is_array($data)) {
            $this->data = $data;
        } else {
            $models_pool = manufactoryModelsPool::getInstance();
            $model = $models_pool->get('Size');
            $data = $model->getById($data);
            if (!empty($data)) {
                $this->data = $data;
            }
        }
        $this->setData();

    }

    protected function setData()
    {

    }

    public function getId()
    {
        return $this->data['id'];
    }

    public function getName()
    {
        return $this->data['name'];
    }

    public function getGroupId()
    {
        return $this->data['groups_id'];
    }

    public function offsetExists($offset)
    {
        if (isset($this->data[$offset])) {
            return true;
        }

        return false;
    }

    public function offsetGet($offset)
    {
        if (isset($this->data[$offset])) {
            return $this->data[$offset];
        }

        return null;
    }

    public function offsetSet($offset, $value)
    {

        $this->data[$offset] = $value;

    }

    public function offsetUnset($offset)
    {

        $this->data[$offset] = null;

    }

}