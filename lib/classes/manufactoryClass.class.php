<?php

abstract class manufactoryClass
{
    /**
     * @param null $name
     *
     * @return manufactoryModel
     */
    protected function getModel($name = null)
    {
        if ($name == null) {
            return false;
        }

        return wa('manufactory')->getConfig()->getModel($name);
    }

    protected function getFactory($name)
    {

        return wa('manufactory')->getConfig()->getFactory($name);
    }
}