<?php

class manufactorySingletone
{

    protected function __construct()
    {

    }

    /*protected static $_instance = null;*/
    public static function getInstance()
    {
        if (static::$_instance == null) {
            static::$_instance = new static();
        }

        return static::$_instance;
    }
}