<?php

class manufactoryCategoryProducts extends manufactoryEntity implements ArrayAccess
{
    public function __construct($data)
    {
        if (is_array($data) && isset($data['id'])) {
            $this->setData($data);
        } elseif (is_numeric($data) && intval($data) > 0) {
            $category_data = $this->getModel('CategoryProducts')->getById(intval($data));
            if (!empty($category_data)) {
                $this->setData($category_data);;
            }
        } elseif (!is_null($data)) {
            // Корневая категория
            $data = array(
                'name'      => 'Весь каталог',
                'id'        => 0,
                'parent_id' => 0,
                'deth'      => 0,
                'sort'      => 0,
            );
            $this->setData($data);
        } else {
            $data = array(
                'name'      => 'Новая категория',
                'id'        => '',
                'parent_id' => '',
                'sort'      => '',
            );
            $this->setData($data);
        }
    }

    public function getRights()
    {
        if ($this->getId() > 0) {
            $path = $this->getModel('CategoryProducts')->getPath($this->getId(), 0);
            if (is_array($path) && !empty($path)) {
                $root_category = array_shift($path);
                $root_id = $root_category['id'];
            } else {
                $root_id = $this->getId();
            }

            return wa()->getUser()->getRights('manufactory', 'product_category.' . $root_id);
        } else {
            return manufactoryRightConfig::RIGHT_READ;
        }
    }

    public function getSubCategories()
    {
        $data = $this->getModel('CategoryProducts')->getSubcategories($this->getId());
        $return = array();
        if (!empty($data)) {
            foreach ($data as $v) {
                $return[$v['id']] = new manufactoryCategoryProducts($v);
            }
        }

        return $return;
    }

    public function getProducts($start = 0, $limit = 50)
    {
        $model = $this->getModel('CategoryProducts');
        $data = $model->getProducts($this->getId(), $start, $limit);

        $return = array();
        if (!empty($data)) {
            foreach ($data as $id => $v) {
                $return[$id] = $this->getFactory('Products')->get($v);
            }
        }

        return $return;
    }

    public function save($data)
    {
        if (!$this->getId()) {
            $this->getModel('CategoryProducts')->updateById($this->getId(), $data);
        } else {
            return $this->getModel('CategoryProducts')->insert($data);
        }
    }

    public function delete()
    {

    }

    public function offsetExists($offset)
    {
        if (array_key_exists($offset, $this->data)) {
            return true;
        }

        return false;
    }

    public function offsetGet($offset)
    {
        if (array_key_exists($offset, $this->data)) {
            return $this->data[$offset];
        }

        return null;
    }

    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

}