<?php


class manufactoryMaterialsPool extends manufactoryObjectPool
{
    protected static $model_name = 'Material';
    protected static $obj_class_name = 'manufactoryCostMaterial';

    protected static $storage = array();
    protected static $model = null;

    public function getCategoryMaterials($id)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('Material');
        $data = $model->select('id')->where('category_id = ' . intval($id))->order('sort')->fetchAll('id');
        $return = array();
        foreach ($data as $v) {
            $return[$v['id']] = self::get($v['id']);
        }

        return $return;
    }

}