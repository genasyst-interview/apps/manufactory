<?php


class manufactoryFeaturesSkusGenerate
{
    protected $features = array();
    protected $skus = array();
    protected $excluded_combinations = array();

    public function __construct()
    {
        /* $product_type = new manufactoryProductType();
         $this->excluded_combinations = $product_type->getExcludedCombinations();*/
    }

    /*
     * $features = array(
           '10' => array(
               110,120,130
           ),
           '20' => array(
               210,220,230
           ),
           '30' => array(
               310,320,330
           ),
           '40' => array(
               410,420,430
           ),
);*/
    public static function getSkuIndexFeatures($sku_data)
    {
        //1:2;2:7;
        if (is_array($sku_data)) {
            return $sku_data;
        } else {
            $arr_feature = explode(';', $sku_data);
            $features = array();
            foreach ($arr_feature as $v) {
                if (!empty($v)) {
                    $v_arr = explode(':', $v);
                    $features[$v_arr[0]] = $v_arr[1];
                }
            }

            return $features;
        }
    }

    public static function getSkuIndexName($sku_data)
    {
        //1:2;2:7;

        $features = self::getSkuIndexFeatures($sku_data);
        $name = '';
        foreach ($features as $id => $value_id) {
            $feature_class = manufactoryFeaturesPool::getInstance()->get($id);
            if (is_object($feature_class)) {
                $name .= $feature_class->getName();
                $value = $feature_class->getValueData($value_id);
                $name .= ' ' . $value['name'] . ', ';
            }
        }
        $name = rtrim(trim($name), ',');

        return $name;

    }

    public function all()
    {
        $data = array();
        $features = manufactoryFeaturesPool::getInstance()->getAll();
        $feature_groups = manufactoryFeaturesPool::getInstance()->getFeatureGroups();
        $skus_features = array();
        foreach ($features as $feature_id => $feature) {
            foreach ($feature_groups as $gv) {
                $feature_values = $feature->getValues($gv['id']);
                foreach ($feature_values as $value_id => $value_data) {
                    $data[$gv['id']][$feature_id][$value_id] = $value_id;
                }
            }
        }
        foreach ($data as $cluster) {
            $skus_features = $skus_features + $this->generate($cluster);
        }

        return $skus_features;
    }

    public function generate($features)
    {
        if (!empty($features)) {
            $this->generateSkuVariants($features);
        }

        return $this->skus;

    }

    public static function getSkuByFeatures($features)
    {
        $str_sku = '';
        foreach ($features as $features_values_feature_id => $features_values_feature_value_id) {
            $str_sku .= $features_values_feature_id . ':' . $features_values_feature_value_id . ';';
        }

        return $str_sku;
    }

    protected function generateSkuVariants($features, $features_values = array(), $level = 0)
    {
        foreach ($features as $k => $values) {
            $features_temp = $features;
            unset($features_temp[$k]);
            foreach ($values as $value) {
                $features_values[$k] = $value;
                if (count($features_temp) > 0) {
                    $this->generateSkuVariants($features_temp, $features_values, $level++);
                } else {
                    $str_sku = self::getSkuByFeatures($features_values);
                    $this->skus[$str_sku] = $features_values;
                    //array_reverse($this->skus);
                }
            }
        }
    }
}