<?php


class manufactoryFeaturesPool extends manufactoryClass
{

    protected $model_name = 'Feature';
    protected $obj_class_name = 'manufactoryFeature';
    protected $types = array(
        'width'         => array(
            'name'      => 'Ширина (Для просчета)',
            'units'     => array(
                'mm' => array(
                    'name'        => 'миллиметр',
                    'coefficient' => 0.001,
                ),
                'cm' => array(
                    'name'        => 'сантиметр',
                    'coefficient' => 0.01,
                ),
                'm'  => array(
                    'name'        => 'метр',
                    'coefficient' => 1,
                ),
            ),
            'base_unit' => 'm',
        ),
        'length'        => array(
            'name'      => 'Длина (Для просчета)',
            'units'     => array(
                'mm' => array(
                    'name'        => 'миллиметр',
                    'coefficient' => 0.001,
                ),
                'cm' => array(
                    'name'        => 'сантиметр',
                    'coefficient' => 0.01,
                ),
                'm'  => array(
                    'name'        => 'метр',
                    'coefficient' => 1,
                ),
            ),
            'base_unit' => 'm',
        ),
        'height'        => array(
            'name'      => 'Высота (Для просчета)',
            'units'     => array(
                'mm' => array(
                    'name'        => 'миллиметр',
                    'coefficient' => 0.001,
                ),
                'cm' => array(
                    'name'        => 'сантиметр',
                    'coefficient' => 0.01,
                ),
                'm'  => array(
                    'name'        => 'метр',
                    'coefficient' => 1,
                ),
            ),
            'base_unit' => 'm',
        ),
        'weight'        => array(
            'name'      => 'Вес (Для расчета)',
            'units'     => array(
                'g'  => array(
                    'name'        => 'грамм',
                    'coefficient' => 0.001,
                ),
                'kg' => array(
                    'name'        => 'килограмм',
                    'coefficient' => 1,
                ),
            ),
            'base_unit' => 'g',
        ),
        'custom.length' => array(
            'name'      => 'Мера длины',
            'units'     => array(
                'mm' => array(
                    'name'        => 'миллиметр',
                    'coefficient' => 0.001,
                ),
                'cm' => array(
                    'name'        => 'сантиметр',
                    'coefficient' => 0.01,
                ),
                'm'  => array(
                    'name'        => 'метр',
                    'coefficient' => 1,
                ),
            ),
            'base_unit' => 'm',
        ),
        'custom.weight' => array(
            'name'      => 'Мера веса',
            'units'     => array(
                'g'  => array(
                    'name'        => 'грамм',
                    'coefficient' => 0.001,
                ),
                'kg' => array(
                    'name'        => 'килограмм',
                    'coefficient' => 1,
                ),
            ),
            'base_unit' => 'g',
        ),

    );
    protected $types_units = array(
        'length' => array(
            'mm' => array(
                'name'        => 'миллиметр',
                'coefficient' => 0.001,
            ),
            'cm' => array(
                'name'        => 'сантиметр',
                'coefficient' => 0.01,
            ),
            'm'  => array(
                'name'        => 'метр',
                'coefficient' => 1,
            ),
        ),
        'weight' => array(
            'g'  => array(
                'name'        => 'грамм',
                'coefficient' => 0.001,
            ),
            'kg' => array(
                'name'        => 'килограмм',
                'coefficient' => 1,
            ),
        ),
    );
    protected $storage = array();
    protected $model = null;
    protected static $_instance = null;

    public function __construct()
    {
        $data = $this->getModel($this->model_name)->getAll('id');
        $obj_class_name = $this->obj_class_name;
        foreach ($data as $k => $v) {
            if (!is_object($v)) {
                $this->storage[$k] = new $obj_class_name($v);
            } else {
                $this->storage[$k] = $v;
            }
        }
    }

    /**
     * @return self
     */
    public static function getInstance()
    {
        if (static::$_instance == null) {
            static::$_instance = new static();
        }

        return static::$_instance;
    }

    public function get($id = null)
    {
        if (!empty($id)) {
            if (isset($this->storage[$id])) {
                return $this->storage[$id];
            } else {
                $models_pool = manufactoryModelsPool::getInstance();
                $model = $models_pool->get($this->model_name);
                $data = $model->getById($id);
                if (is_object($data)) {
                    $this->storage[$id] = $data;

                    return $this->storage[$id];
                }
                if (!empty($data) && !empty($this->obj_class_name) && class_exists($this->obj_class_name)) {
                    $className = $this->obj_class_name;
                    $this->storage[$id] = new $className($data);

                    return $this->storage[$id];
                }

                return null;
            }
        } else {
            return $this->storage;
        }
    }

    public function getFeatureGroups($feature_id = null)
    {
        if (!empty($feature_id)) {
            // Пока нет разделения по типу!
            $models_pool = manufactoryModelsPool::getInstance();
            $FeatureGroupValues_model = $models_pool->get('FeatureGroupValues');

            return $FeatureGroupValues_model->getAll('id');
        } else {
            $models_pool = manufactoryModelsPool::getInstance();
            $FeatureGroupValues_model = $models_pool->get('FeatureGroupValues');

            return $FeatureGroupValues_model->getAll('id');
        }

    }

    public function getTypeName($type)
    {
        if (isset($this->types[$type])) {
            return $this->types[$type]['name'];
        }

        return 'Неизвестный тип';
    }

    public function getTypes()
    {
        if (isset($this->types)) {
            return $this->types;
        }
    }

    public function getBaseUnit($type)
    {
        if (isset($this->types[$type])) {
            return $this->types[$type]['base_unit'];
        }

        return 'Неизвестный тип';
    }

    public function getTypeUnits($type)
    {
        if (isset($this->types[$type])) {
            return $this->types[$type]['units'];
        }

        return 'Неизвестный тип';
    }

    public function getAll()
    {
        return $this->storage;
    }

}