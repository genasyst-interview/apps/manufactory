<?php

class manufactoryRecalculatePrices
{


    public function __construct()
    {

    }

    public function recalculatePrice($id, &$errors)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $prices = array();
        $price = manufactoryPricesPool::get($id);
        if (!$price) {
            $errors[] = 'Неправильный id цены!';

            return false;
        }
        $prices[$id] = $price;
        $datetime = date("Y-m-d H:i:s");
        $product_model = $models_pool->get('Product');
        $products = $product_model->getAll();

        foreach ($products as $product) {
            $product_class = wa('manufactory')->getConfig()->getFactory('Products')->get($product);
            $product_class->recalculatePrices($prices);
            $product_class->updateProductDatetime($datetime);
        }

        return true;
    }

    public function recalculateMaterialsCoefficients()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $materials_model = $models_pool->get('Material');
        $materials = $materials_model->getAll('id');
        foreach ($materials as $id => $v) {
            $material = new manufactoryMaterial($v);
            $material->regenerateSkusFeatures();
        }

    }

    public function updateMaterials()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $Product = $models_pool->get('Product');
        $ProductMaterials = $models_pool->get('ProductMaterials');
        $products = $Product->getAll();
        foreach ($products as $product_data) {
            if (@unserialize($product_data['materials_id'])) {
                $materials = unserialize($product_data['materials_id']);
                $ProductMaterials->deleteByProduct($product_data['id']);
                foreach ($materials as $mat_id => $quantity) {
                    $m_data = array(
                        'product_id'  => $product_data['id'],
                        'material_id' => $mat_id,
                        'quantity'    => $quantity,
                    );
                    $ProductMaterials->insert($m_data);
                }
            }
        }
    }

    public function recalculateMaterial($material_id)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $prices = manufactoryPricesPool::geByType('generate');
        $datetime = date("Y-m-d H:i:s");
        $product_materials_model = $models_pool->get('ProductMaterials');
        $products = $product_materials_model->getProductsByMaterialId($material_id);
        foreach ($products as $product) {

            $product_class = wa('manufactory')->getConfig()->getFactory('Products')->get($product['product_id']);
            $product_class->recalculatePrices($prices);
            $product_class->updateProductDatetime($datetime);

        }

    }

}