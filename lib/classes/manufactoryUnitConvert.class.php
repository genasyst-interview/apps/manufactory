<?php


class manufactoryUnitConvert
{
    protected static $base_unit = 'm';
    protected static $units_calculate = array(
        'mm' => 0.001,
        'cm' => 0.01,
        'm'  => 1,
        'km' => 1000,
    );

    public static function convertValue($value)
    {
        if ($value['unit'] != 'm') {
            $coefficient = self::$units_calculate[$value['unit']];
            $value['value'] = floatval($value['value']) * floatval($coefficient);
        }

        return $value;

    }
}