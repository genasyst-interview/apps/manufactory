<?php

abstract class manufactoryModelObject extends manufactorySortableModel
{
    protected $object_class_name = '';

    public function getClassName()
    {
        return $this->object_class_name;
    }

    public function setClassName($class_name = '')
    {
        $this->object_class_name = $class_name;
    }

    public function getById($value)
    {
        $field = $this->remapId($value);
        $all = (!is_array($this->id) && is_array($value)) ? (is_array($this->id) ? true : $this->id) : false;

        return $this->getByField($field, $all);
    }

    public function getByField($field, $value = null, $all = false, $limit = false)
    {
        $class_name = $this->getClassName();
        $data = parent::getByField($field, $value, $all, $limit);
        if (is_array($field)) {
            $all = $value;
        }
        if ($all) {
            if (class_exists($class_name)) {
                foreach ($data as $k => $v) {
                    $data[$k] = new $class_name($v);
                }
            }

            return $data;
        } else {
            if (class_exists($class_name)) {
                $data = new $class_name($data);
            }
        }

        return $data;

    }

    public function getAll($key = null, $normalize = false)
    {
        $class_name = $this->getClassName();
        $data = parent::getAll($key, $normalize);
        if (class_exists($class_name)) {
            foreach ($data as $k => $v) {
                $data[$k] = new $class_name($v);
            }
        }

        return $data;
    }

}
