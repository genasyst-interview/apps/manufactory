<?php

class manufactoryMarginsProduct extends manufactoryClass implements IteratorAggregate, ArrayAccess
{
    protected $data = array();
    protected $categories = array();

    public function __construct($data = array())
    {
        $this->setPrices($data = array());
    }

    public function getPrices()
    {
        return $this->data;
    }

    public function setPrices($data = array())
    {
        $this->data = array();
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                if (wa()->getUser()->getRights(wa()->getApp(), 'margin_product.' . $v)) {
                    $this->data[$v] = manufactoryPricesPool::get($v);
                }

            }
        } else {
            $models_pool = manufactoryModelsPool::getInstance();
            $model = $models_pool->get('MarginProduct');
            $prices = $model->query("SELECT id FROM " . $model->getTableName() . " ORDER BY sort ")->fetchAll('id');
            foreach ($prices as $price) {
                if (wa()->getUser()->getRights(wa()->getApp(), 'margin_product.' . $price['id'])) {
                    $this->data[$price['id']] = manufactoryPricesPool::get($price['id']);
                }
            }
        }
    }

    public function offsetExists($offset)
    {
        if (array_key_exists($offset, $this->data)) {
            return true;
        }

        return false;
    }

    public function offsetGet($offset)
    {
        if (array_key_exists($offset, $this->data)) {
            return $this->data[$offset];
        }

        return null;
    }

    public function offsetSet($offset, $value)
    {
        // $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        //
    }

    public function getIterator()
    {
        return new ArrayObject($this->data);
    }
}