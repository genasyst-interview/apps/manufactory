<?php

class manufactorySizesPool extends manufactoryObjectPool
{
    protected static $model_name = 'Size';
    protected static $obj_class_name = 'manufactorySize';

    protected static $storage = array();
    protected static $model = null;

}
