<?php

class manufactoryMatch
{
    public static $error = ''; // Ошибки!
    //
    public static $pi = 3.14;// число подставляется в F Replase(); {pi}=$pi

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    // SINGLETONE екземпляр класса через Match::getInstanse();
    public static function getInstanse()
    {
        if (self::$instanse == null) {
            self::$instanse = new self();
        }

        return self::$instanse;
    }

    // Функция приема и расчета выражения
    public static function calculate($str)
    {
        self::$error = '';
        $result = 0;
        if ($validate = self::Validate($str)) {
            $str = self::Replase($str);
            try {
                if (@eval('$result = (' . $str . ');') === false) {
                    waLog::log('$result = (' . $str . ');  - no eval ', 'manufactory_match.log');
                }
            } catch (waException $e) {
                waLog::log('$result = ' . $str . ';  - no eval ', 'manufactory_match.log');
            }

            return $result;
        } else {
            return null;
        }

    }

    // Проверка на безопасность....
    private static function Validate($str)
    {
        // В массиве разрешенные символы, переменные и функции
        $result = strtr($str, array(
                '{pi}'                             => '', // разрешенные переменные
                'myfunc('                          => '', // разрешенные пользовательские функции
                'manufactoryPricesPool::get('      => '',
                'manufactoryPricesPool::getPrice(' => '',
                '->calculate('                     => '',
                '1'                                => '',
                '2'                                => '',
                '3'                                => '',
                '4'                                => '',
                '5'                                => '',
                '6'                                => '',
                '7'                                => '',
                '8'                                => '',
                '9'                                => '',
                '0'                                => '',
                '.'                                => '',
                ','                                => '',
                '+'                                => '',
                '-'                                => '',
                '*'                                => '',
                '/'                                => '',
                '%'                                => '',
                'asin('                            => '',
                'sin('                             => '',
                'acos('                            => '',
                'cos('                             => '',
                'abs('                             => '',
                'ceil('                            => '',
                'exp('                             => '',
                'floor('                           => '',
                'tan('                             => '',
                'round('                           => '',
                'sqrt('                            => '',
                ')'                                => '',
                '('                                => '',
                ' '                                => '',
            )
        );
        //если все символы заменены на '' и $result получился пустой строкой
        if (empty($result)) {
            // Проверка валидности скобок
            if (substr_count($str, '(') != substr_count($str, ')')) {
                self::$error .= 'Ошибка. Число открытых и закрытых скобок не совпадает!';

                return false;
            } else {
                return true;
            }
            // Остались незамененные символы, выводим в $error
        } else {
            self::$error .= 'Ошибка. Запрещенные символы <b>' . $result . '<b>!!!';

            return false;
        }
    }

    // Замена переменных и некоторых символов
    private static function Replase($str)
    {
        $return = strtr($str,
            array(
                '{pi}'    => 'self::$pi',// заменяем в строке на переменную $pi в классе
                'stepen(' => '$this->stepen(',
                ','       => '.',
                '++'      => '',
                '--'      => '',
                //++ и -- мы запрещаем по причине, что они изменяют исходную переменную,
                // что может быть недопустимо.
            )
        );

        return $return;
    }

    public function stepen($chislo = 1, $stepen = 1)
    { // пользовательская функция
        $chislo = floatval($chislo);
        for ($i = 1; $i < $stepen; $i++) {
            if (!isset($result)) {
                $result = $chislo * $chislo;
            } else {
                $result = $result * $chislo;
            }
        }

        return $result;
    }
}

?>