<?php

class manufactoryViewHelper extends manufactoryClass
{
    protected function getModel($name = null)
    {
        return false;
    }

    //// PRODUCT CATEGORIES
    public function getProductSubcategories($category_id = 0)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('categoryProducts');

        return $model->query("SELECT * FROM " . $model->getTableName() . " WHERE parent_id = '" . intval($category_id) . "' ORDER BY name ")->fetchAll('id');
    }

    public function allowCategoriesThree($category_id, $min_rights = 0)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('categoryProducts');
        $categories = $model->descendants($category_id)->fetchAll('id');
        foreach ($categories as $k => $v) {
            if ($v['parent_id'] > 0) {
                $categories[$v['parent_id']]['childs'][$v['id']] = $v;
                unset($categories[$k]);
            }
        }
        foreach ($categories as $k => $v) {
            if ($model->getRights($k) < $min_rights) {
                unset($categories[$k]);
            }
        }

        return $categories;

    }

    public function allowCategories($category_id, $min_rights = 0)
    {
        $three = $this->allowCategoriesThree($category_id, $min_rights);
        $categories = array();
        foreach ($three as $k => $v) {
            $categories = array_merge($categories, $this->categories_list($v));
        }
        $return = array();
        foreach ($categories as $v) {
            if (!empty($v['id'])) {
                $return[$v['id']] = $v;
            }
        }

        return $return;
    }

    protected function categories_list($category_data)
    {
        $categories = array();

        if (array_key_exists('childs', $category_data)) {
            $childs = $category_data['childs'];
            unset($category_data['childs']);
            $categories[$category_data['id']] = $category_data;
            foreach ($childs as $child) {
                $categories = array_merge($categories, $this->categories_list($child));
            }
        } else {
            $category_data['name'] = str_pad('', $category_data['depth'] * 2, '.') . $category_data['name'];
            $categories[$category_data['id']] = $category_data;
        }

        return $categories;
    }

    public function categories($category_id)
    {
        return $this->getProductSubcategories($category_id);
    }

    public function category($id)
    {
        $model = $this->getModel('categoryProducts');

        return $model->getById($id);
    }

    public function getCostType($type = '')
    {
        return $this->getFactory('Costs')->getType($type);
    }

    public function categoriesThree($id = 0)
    {
        $categories = $this->getProductSubcategories($id);
        if (is_array($categories) && !empty($categories)) {
            foreach ($categories as $k => &$v) {
                if ($v['parent_id'] == 0) {
                    if (wa()->getUser()->getRights('manufactory', 'product_category.' . $v['id']) > 0) {
                        $childrens = $this->categoriesThree($k);
                        if (!empty($childrens)) {
                            $v['childrens'] = $childrens;
                        }
                    } else {
                        unset($categories[$k]);
                    }
                } else {
                    $childrens = $this->categoriesThree($k);
                    if (!empty($childrens)) {
                        $v['childrens'] = $childrens;
                    }
                }


            }

            return $categories;
        } else {
            return array();
        }
    }

    public function getSize($id)
    {
        return manufactorySizesPool::get($id);
    }
}