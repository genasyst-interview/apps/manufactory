<?php

class manufactoryProducts
{

    public static function getByCategoryId($id)
    {
        $return = array();
        if (!empty($id)) {
            $models_pool = manufactoryModelsPool::getInstance();
            $model = $models_pool->get('Product');
            $products = $model->query("SELECT * FROM " . $model->getTableName() . " WHERE category_id= '" . intval($id) . "' ORDER BY id ")->fetchAll('id');
            if (!empty($products)) {
                foreach ($products as $product) {
                    $return[$product['id']] = wa('manufactory')->getConfig()->getFactory('Products')->get($product);
                }
            }
        }

        return $return;
    }

    public static function getById($id)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('Product');
        if (!empty($id)) {
            $product = $model->getById($id);
            if (!empty($product)) {
                return wa('manufactory')->getConfig()->getFactory('Products')->get($product);
            } else {
                return null;
            }
        }
    }
}