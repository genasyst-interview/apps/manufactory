<?php

/**
 * Interface manufactoryCostInterface
 */
interface manufactoryCostInterface extends ArrayAccess
{
    const ENTITY_TYPE = '';

    public function __construct($data);

    public function setData($data);

    public function init();

    public function isActive();

    public function getName();

    public function getPrice();

    public function getFormula();

    public function isEquation();

    public function getType();

    public function getEntityType();


}