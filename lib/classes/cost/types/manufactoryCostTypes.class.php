<?php

/**
 * Class CostTypes
 */
class manufactoryCostTypes implements manufactoryCostTypesInterface
{


    protected static $_instance = null;

    protected function __construct()
    {
    }

    public function __clone()
    {
    }

    public static function getInstance()
    {
        if (self::$_instance == null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }


    protected static $types = null;
    protected static $base_types = array(
        'materials' => 'manufactoryCostsTypeMaterials',
    );

    /**
     * @return array|null
     */
    public function getTypes()
    {
        if (self::$types == null) {
            self::$types = array();
            foreach (self::$base_types as $type_id => $class) {
                $this->set($type_id, $class);
            }
            wa('manufactory')->event('costs', $this);
        }

        return self::$types;
    }

    public function set($name, $class)
    {
        if (is_object($class)) {
            self::$types[$name] = $class;
        } else if (is_string($class)) {
            self::$types[$name] = new $class();
        }
    }

    public function getType($cost_type = '')
    {
        $types = self::getTypes();
        if (array_key_exists($cost_type, $types)) {
            return $types[$cost_type];
        } else {
            return wao($cost_type);
        }
    }
}
