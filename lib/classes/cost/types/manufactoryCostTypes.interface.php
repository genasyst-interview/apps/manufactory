<?php

/**
 * Interface manufactoryCostTypesInterface
 */
interface manufactoryCostTypesInterface
{
    /**
     * @return mixed
     */
    public function getTypes();
}