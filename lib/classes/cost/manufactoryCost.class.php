<?php

/**
 * Class manufactoryCost
 */
abstract class manufactoryCost extends manufactoryEntity implements manufactoryCostInterface
{

    /**
     * manufactoryCost constructor.
     *
     * @param $data array|integer
     */
    public function __construct($data = null)
    {
        $this->setData($data);
        $this->init();
    }

    abstract public function init();

    /**
     * @param $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    public function isActive()
    {
        if ($this->getData('status') == 'active') {
            return true;
        }

        return false;
    }

    public function getFormula()
    {
        return $this->data['formula'];
    }

    public function getType()
    {
        return $this->getData('type');
    }

    public function isEquation()
    {
        if (preg_match('/[xys]/i', $this->getFormula())) {
            return true;
        }

        return false;
    }

    public function getCost()
    {
        return floatval($this->data['price']);
    }

    public function getPrice()
    {
        return floatval($this->data['price']);
    }

    public function getEntityType()
    {
        return static::ENTITY_TYPE;
    }


    public function offsetExists($offset)
    {
        if (array_key_exists($offset, $this->data)) {
            return true;
        }
    }

    public function offsetGet($offset)
    {
        return $this->getData($offset);
    }

    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

}