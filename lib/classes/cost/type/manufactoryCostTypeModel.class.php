<?php

/**
 * Модель хранения зарегистрированных типов расходов
 * Class manufactoryCostTypeModel
 */
class manufactoryCostTypeModel extends manufactoryModel
{
    /* id, type */
    /**
     * @var string
     */
    protected $table = 'manufactory_cost_type';
}
