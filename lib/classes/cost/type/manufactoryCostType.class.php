<?php

/**
 * Class manufactoryCostType
 */
abstract class manufactoryCostType extends manufactoryClass implements manufactoryCostTypeInterface
{

    protected $cost_model = null;

    protected $entity_cost_models = array();
    protected $entity_cost_models_name = array();

    protected $data = null;


    public function __construct()
    {
    }

    public function getData()
    {
        if ($this->data == null) {
            $costs = $this->getModel()->getAll('id');
            foreach ($costs as $id => $data) {
                $this->data[$id] = $this->buildCost($data);
            }
        }

        return $this->data;
    }

    public function setByEntity(manufactoryEntityProduct $entity, $data = array())
    {
        $model = $this->getEntityModel($entity);
        $model->setByEntity($entity, $data);
    }

    public function getByEntity(manufactoryEntityProduct $entity)
    {
        $model = $this->getEntityModel($entity);
        $data = array();
        if (!empty(static::TYPE) && $model) {
            $costs = $model->getByEntity($entity->getId());
            foreach ($costs as $id => $entity_cost) {
                $data[$id] = $this->buildEntityCost($entity_cost);
            }
        }

        return $data;
    }

    public function getModel($name = null)
    {
        if (!empty($name)) {
            return parent::getModel($name);
        }
        if ($this->cost_model == null) {
            $model_name = static::COST_MODEL;
            $this->cost_model = parent::getModel($model_name);
        }

        return $this->cost_model;
    }

    function getEntityModel($entity)
    {
        $entity_type = $entity->getEntityType();
        if (!empty($entity_type) && !array_key_exists($entity_type, $this->entity_cost_models)) {
            if (array_key_exists($entity_type, $this->entity_cost_models_name)) {
                $model_name = $this->entity_cost_models_name[$entity_type];
            } else {
                //    CompositematerialsProductCosts
                $model_name = ucfirst($this->getTypeId()) . ucfirst($entity_type) . 'Costs';
            }
            $this->entity_cost_models[$entity_type] = wao($this->getModel($model_name));
        }
        if (array_key_exists($entity_type, $this->entity_cost_models)) {
            return $this->entity_cost_models[$entity_type];
        }

        return null;
    }

    public function buildCost($cost_data)
    {
        $class_name = static::COST_CLASS_NAME;

        return new $class_name($cost_data);
    }

    public function buildEntityCost($entity_cost)
    {
        $class_name = static::ENTITY_COST_CLASS_NAME;

        return wao(new $class_name($this->getCost($entity_cost['cost_id']), $entity_cost['quantity']));
    }

    public function setCost($data)
    {
        $cost = array();
        if (is_array($data) && array_key_exists('id', $data)) {
            $cost = $data;
        } elseif (is_numeric($data)) {
            $cost = $this->getModel()->getById($data);
        }
        if (!empty($cost)) {
            $this->data[$data['id']] = $this->buildCost($cost);
        }
    }

    public function getCost($id)
    {
        if (!array_key_exists($id, $this->data)) {
            $this->setCost($id);
        }
        if (array_key_exists($id, $this->data)) {
            return $this->data[$id];
        }
    }

    function getTypeId()
    {
        return static::TYPE;
    }

}