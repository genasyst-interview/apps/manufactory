<?php

interface manufactoryCostTypeInterface
{
    const TYPE = '';
    const COST_MODEL = '';
    const COST_CLASS_NAME = '';
    const ENTITY_COST_CLASS_NAME = '';

    /**
     * manufactoryCostType constructor.
     *
     * @param $data
     */
    function __construct();

    /**
     * @param $data
     *
     * @return mixed
    protected function setData($data);
     */

    /**
     * @return mixed
     */
    function getEntityModel($entity);

    /**
     * @return mixed
     */
    function getTypeId();

    public function getEntityCosts($entity);

    public function getCosts();
}