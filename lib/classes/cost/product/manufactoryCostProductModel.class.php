<?php

/**
 * Модель для хранения данных расходов для продукта
 * Class manufactoryProductCostModel
 */
abstract class manufactoryCostProductModel extends manufactoryCostEntityModel
{
    protected $entity_id_column = 'product_id';
}