<?php

/**
 * Модель для хранения данных расходов товарного предложения
 * Class manufactoryProductOfferCostModel
 */
abstract class manufactoryCostProductSkuModel extends manufactoryCostEntityModel
{
    protected $entity_id_column = 'sku_id';

}