<?php

interface  manufactoryCostEntityInterface
{
    /* Принимает какую-то затрату и ее количество */
    /**
     * gicIProductCost constructor.
     *
     * @param gicCost $cost
     * @param         $quantity
     */
    public function __construct(manufactoryCost $cost, $quantity);
    /* Записываем объект затраты */
    /**
     * @param $cost
     *
     * @return mixed
     */
    public function setCost(manufactoryCost $cost);
    /* Записываем количество затраты */
    /**
     * @param $quantity
     *
     * @return mixed
     */
    public function setQuantity($quantity);
    /* Отдаем количество затраты */
    /**
     * @return mixed
     */
    public function getQuantity();
    /* Базовая стоимость затраты */
    /**
     * @return mixed
     */
    public function getCost();
    /* Отдаем объект затраты */
    /**
     * @return mixed
     */
    /*ВОзвращаем объект прослойку для расчетов gicIProductCostCalculation */
    /**
     *
     * @return mixed
     */
    public function getPrice($data);
}