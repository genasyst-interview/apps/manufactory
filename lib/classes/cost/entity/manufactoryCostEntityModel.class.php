<?php

/**
 * Абстрактная модель для хранения данных расходов для продукта или товарного предложения
 * Class manufactoryCostEntityModel
 */
abstract class manufactoryCostEntityModel extends manufactoryEntityProductStorageModel implements manufactoryCostEntityModelInterface
{
    /**
     * Название колонки сущности (entity_id, product_id, offer_id)
     * @var string
     */
    protected $entity_id_column = 'entity_id';
    protected $storage_id_column = 'cost_id';

    public function setByEntity($id, $data = array())
    {
        $delete_costs = $this->getByEntity($id);
        foreach ($data as $sid => $quantity) {
            if (array_key_exists($sid, $delete_costs)) {
                unset($delete_costs[$sid]);
                $this->updateByField(array(
                    $this->entity_id_column  => $id,
                    $this->storage_id_column => $sid,
                ),
                    array('quantity' => $quantity)
                );
            } else {
                $_data = array(
                    $this->entity_id_column  => $id,
                    $this->storage_id_column => $sid,
                    'quantity'               => $quantity,
                );
                $this->insert($_data);
            }
        }
        foreach ($delete_costs as $delete_material) {
            $this->deleteByField(
                $delete_material
            );
        }
    }

    public function getByCost($id, $all = null)
    {
        if ($all == null) {
            $all = $this->entity_id_column;
        }

        return $this->getByField(array(
            $this->storage_id_column => $id,
        ), $all);
    }

    public function deleteByCost($id)
    {
        return $this->deleteByField($this->storage_id_column, $id);
    }
}