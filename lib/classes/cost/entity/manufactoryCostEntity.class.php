<?php

/**
 * Абстрактная модель для хранения данных расходов для продукта или товарного предложения
 * Class manufactoryCostEntity
 */
abstract class manufactoryCostEntity implements manufactoryCostEntityInterface
{
    /**
     * @var null
     */
    protected $cost = null;
    /**
     * @var int
     */
    protected $quantity = 0;

    /**
     * gicProductCost constructor.
     *
     * @param $cost
     * @param $quantity
     */
    public function __construct(manufactoryCost $cost, $quantity)
    {

        $this->setCost($cost);
        $this->setQuantity($quantity);
        /* var_dump($this->getCost()->getId());
         var_dump($this->getQuantity());*/
    }
    /* ОБъект затраты */
    /**
     * @param manufactoryCost $cost
     */
    public function setCost(manufactoryCost $cost)
    {
        $this->cost = $cost;
    }

    /**
     * @param $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return manufactoryCost
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @return null
     */

    public function getPrice($data)
    {
        return $this->getCost() * $this->getQuantity();
    }

    public function __call($name, $arguments)
    {
        if (method_exists($this->cost, $name)) {
            return call_user_func_array(array($this->cost, $name), $arguments);
        }
    }

    public function __get($name)
    {
        return $this->cost->$name;
    }
}