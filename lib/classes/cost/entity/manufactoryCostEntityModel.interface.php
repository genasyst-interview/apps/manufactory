<?php

/**
 * Интерфейс для моделей хранения затрат каких-то продуктовых сущностей
 * (товары, товарные предложения, полуфабрикаты, возможно сборные материалы)
 * Interface manufactoryCostEntityModelInterface
 */
interface manufactoryCostEntityModelInterface extends manufactoryEntityProductStorageModelInterface
{
    /**
     * Получение затрат для товарной сущности
     * Отдает массив расходов для сущности (продукт, предложение продукта)
     *
     * @param $id
     *
     * @return array (cost_id => data,.....)
     *
     */
    //public function getByEntity($id);

    /**
     * Запись затрат для товарной сущности
     * $cost  =
     *
     * @param integer $id    Идентификатор товарной сущности
     * @param array   $costs Массив затрат в формате cost_id => quantity,...
     *
     * @return boolean
     */
    //public function setByEntity($id, $costs = array());

    /**
     * Удаление затрат из товарной сущности
     *
     * @param $id Идентификатор товарной сущности
     *
     * @return mixed
     */
    //public function deleteByEntity($id);

    /**
     * Получение товарых сущностей по идентификатору затраты
     *
     * @param             $id
     * @param null|string $all параметр ключа получения, по умолчанию - колонка товарных сущностей
     *
     * @return array (entity_id => data,.....)
     *
     */
    public function getByCost($id, $all = null);

    /**
     * Удаление затраты из всех товарных сущностей
     * Используется при удалении затраты
     *
     * @param $id
     *
     * @return mixed
     */
    public function deleteByCost($id);
}