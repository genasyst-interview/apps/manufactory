<?php

class manufactoryCostMaterialDependencies extends manufactoryEntityCostDependencies
{

    public function getProducts()
    {
        return $this->getModel('ProductMaterials')
            ->select('entity_id as id')->where('cost_id = ' . $this->getId())->fetchAll('id');
    }

    public function getDependencies()
    {
        return array(
            'products' => $this->getProducts(),
        );
    }

    public function recalculateDependencies()
    {
        // У материала нет зависимотей 
    }

    public function deleteDependencies()
    {
        $this->deleteProductMaterials();

    }

    public function deleteProductMaterials()
    {
        $this->getModel('ProductMaterials')->deleteByField('cost_id', $this->getId());
    }
}
