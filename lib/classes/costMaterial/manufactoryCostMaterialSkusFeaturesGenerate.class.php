<?php

/*
id	int(11) unsigned Автоматическое приращение
name	varchar(255)
price	float
formula	text
sku_features_coefficients	text
category_id	int(11) unsigned
sort	int(11) unsigned
*/

class manufactoryCostMaterialSkusFeaturesGenerate
{
    protected $material_id = 0;
    protected $formula = '';
    protected $features = array();

    public function __construct($material_id, $formula)
    {
        $this->material_id = $material_id;
        $this->formula = $formula;
        $this->features = manufactoryFeaturesPool::getInstance()->getAll();
        ksort($this->features);
    }

    public function isEquation()
    {
        if (preg_match('/[xys]/i', $this->formula)) {
            return true;
        }

        return false;
    }

    public function generate()
    {
        $data = array();

        $feature_groups = manufactoryFeaturesPool::getInstance()->getFeatureGroups();
        $skus_features = array();
        foreach ($this->features as $feature_id => $feature) {
            foreach ($feature_groups as $gv) {
                $feature_values = $feature->getValues($gv['id']);
                foreach ($feature_values as $value_id => $value_data) {
                    $data[$gv['id']][$feature_id][$value_id] = $value_id;
                }
            }
        }
        foreach ($data as $cluster) {
            $gen = new manufactoryFeaturesSkusGenerate();
            $skus_features = $skus_features + $gen->generate($cluster);
        }
        $return_data = array();
        foreach ($skus_features as $sku_data => $sku) {
            if ($this->isEquation()) {
                $sku_features = array();
                foreach ($sku as $feature_id => $value_id) {
                    $value = $this->features[$feature_id]->getValueData($value_id);

                    $sku_features[$feature_id] = $value;
                }
                $calculate_string = $this->getSkuCalculateString($sku_features);
            } else {
                $calculate_string = '1';
            }
            $coefficient = $this->getCoefficient($calculate_string);

            $return_data[$sku_data] = array(
                'coefficient'      => $coefficient,
                'calculate_string' => $calculate_string,
                'features'         => $sku,
            );
        }
        unset($v);

        return $return_data;
    }

    public function getSkuCalculateString($sku_features)
    {
        $width = $length = $height = $weight = 0;
        foreach ($sku_features as $id => $value) {
            $value = manufactoryUnitConvert::convertValue($value);
            $value = $value['value'];
            switch ($this->features[$id]->getType()) {
                case 'width':
                    $width = $value;
                    break;
                case 'length':
                    $length = $value;
                    break;
                case 'height':
                    $height = $value;
                    break;
                case 'weight':
                    $weight = $value;
                    break;
            }
        }
        $formula = $this->formula;
        $formula = str_ireplace('x', floatval($width), $formula);
        $formula = str_ireplace('y', floatval($length), $formula);
        $formula = str_ireplace('z', floatval($height), $formula);
        $formula = str_ireplace('m', floatval($weight), $formula);
        if ($width > 0 && $length > 0) {
            $s = floatval($width) * floatval($length);
            $formula = str_ireplace('s', $s, $formula);
        }
        $formula = str_replace(',', '.', $formula);

        return '(' . $formula . ')';


    }

    public function getCoefficient($calculate_string)
    {
        return manufactoryMatch::calculate($calculate_string);
    }

    public function getMaterialId()
    {
        return $this->material_id;
    }

    public function getMaterial()
    {

    }


}
