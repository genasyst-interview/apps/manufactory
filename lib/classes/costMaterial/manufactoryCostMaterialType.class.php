<?php

class manufactoryCostMaterialType extends manufactoryCostsType
{
    const COST_CLASS_NAME = 'manufactoryCostMaterial';
    const COST_MODEL = 'manufactoryCostMaterialModel';

    public function getTypeId()
    {
        return 'material';
    }
}