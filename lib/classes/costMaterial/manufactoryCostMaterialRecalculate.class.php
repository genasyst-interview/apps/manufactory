<?php

/*
 * id	int(11) unsigned Автоматическое приращение	 
name	varchar(255)	 
price	float	 
formula	text	 
sku_features_coefficients	text	 
category_id	int(11) unsigned	 
sort	int(11) unsigned
*/

class manufactoryCostMaterialRecalculate extends manufactoryEntityCostRecalculate
{

    public function setDependencies()
    {
        $this->setDependency(new manufactoryCostMaterialDependencies($this->getEntity()));
    }
}
