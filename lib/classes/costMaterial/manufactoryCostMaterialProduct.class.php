<?php

class manufactoryCostMaterialProduct extends manufactoryCostEntity
{
    protected $id = 0;
    protected $quantity = 1;


    public function getId()
    {
        return $this->getCost()->getId();
    }

    public function getName()
    {
        return $this->getCost()->getName();
    }

    public function getType()
    {
        return $this->getCost()->getType();
    }

    public function getPrice($data)
    {
        $price = $this->getCost()->getPrice() * $this->getCoefficient($data);

        return $price * $this->getQuantity();
    }

    public function getCalculateString($sku_data = '')
    {
        if (is_array($sku_data) && !empty($sku_data)) {
            $width = $length = $height = $weight = 0;
            $formula = $this->getCost()->getFormula();
            if ($this->getCost()->isEquation()) {
                $features = manufactoryFeaturesPool::getInstance()->getAll();
                ksort($features);
                foreach ($sku_data as $id => $value) {
                    if (!is_array($value)) {
                        $value = $features[$id]->getValueData($value);
                    }
                    $value = manufactoryUnitConvert::convertValue($value);
                    $value = $value['value'];
                    switch ($features[$id]->getType()) {
                        case 'width':
                            $width = $value;
                            break;
                        case 'length':
                            $length = $value;
                            break;
                        case 'height':
                            $height = $value;
                            break;
                        case 'weight':
                            $weight = $value;
                            break;
                    }
                }
                $formula = str_ireplace('x', floatval($width), $formula);
                $formula = str_ireplace('y', floatval($length), $formula);
                $formula = str_ireplace('z', floatval($height), $formula);
                $formula = str_ireplace('m', floatval($weight), $formula);
                if ($width > 0 && $length > 0) {
                    $s = floatval($width) * floatval($length);
                    $formula = str_ireplace('s', $s, $formula);
                }
                $formula = str_replace(',', '.', $formula);
            }

            $calculate_string = '(' . $formula . ')';
            //var_dump($calculate_string);
            if (!empty($calculate_string)) {
                return $calculate_string;
            }

            return 0;
        } else {
            return 1;
        }
    }

    ////
    public function getCoefficient($sku_data = '')
    {
        $calculate_string = $this->getCalculateString($sku_data);
        $coefficient = manufactoryMatch::calculate($calculate_string);
        ///var_dump($sku_data);
        if (!empty($coefficient)) {
            return $coefficient;
        }

        return 0;
    }

    public function isEquation()
    {
        if (preg_match('/[xys]/i', $this->getCost()->getFormula())) {
            return true;
        }

        return false;
    }

}