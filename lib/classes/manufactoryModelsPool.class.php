<?php


class manufactoryModelsPool
{
    protected static $_instance = null;
    protected static $models = array();

    public function __construct()
    {

    }

    // Deprecated
    public static function getInstance()
    {
        if (self::$_instance == null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function get($name)
    {
        $name = trim($name);
        if (isset(self::$models[strtolower($name)])) {
            return self::$models[strtolower($name)];
        } else {
            $class_name = 'manufactory' . ucfirst($name) . 'Model';
            if (class_exists($class_name)) {
                self::$models[strtolower($name)] = new $class_name();

                return self::$models[strtolower($name)];
            }

            return null;
        }
    }
}