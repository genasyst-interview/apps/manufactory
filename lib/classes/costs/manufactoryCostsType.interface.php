<?php

interface manufactoryCostsTypeInterface
{
    const TYPE = '';
    const COST_MODEL = '';
    const COST_CLASS_NAME = '';
    const ENTITY_COSTS_CLASS_NAME = '';

    /**
     * manufactoryCostType constructor.
     *
     * @param $data
     */
    function __construct();

    public function getCategories();

    public function getByCategory();

    public function getEntityModel($entity);

    public function getData();

    public function setByEntity(manufactoryEntityProduct $entity, $data = array());

    public function deleteByEntity(manufactoryEntityProduct $entity, $data = array());

    public function getTypeId();

    public function getEntityData(manufactoryEntityProduct $entity);

    public function getByEntity(manufactoryEntityProduct $entity);

}