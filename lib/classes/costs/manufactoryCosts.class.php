<?php

class manufactoryCosts extends manufactorySingletone
{
    protected static $_instance = null;
    protected $costs = array();

    protected function __construct()
    {
        $costs = array();
        wa('manufactory')->event('costs', $costs);
        foreach ($costs as $cost_type_id => $cost_type) {
            if ($cost_type instanceof manufactoryCostType) {
                $this->costs[$cost_type_id] = $cost_type;
            }
        }
    }

    public function get()
    {
        return $this->costs;
    }
}