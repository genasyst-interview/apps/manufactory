<?php

/**
 * Class manufactoryCostType
 */
abstract class manufactoryCostsType extends manufactoryClass implements manufactoryCostsTypeInterface
{

    protected $cost_model = null;

    protected $entity_cost_models = array();
    protected $entity_cost_models_name = array();
    protected $entity_costs_class_name = array();

    protected $data = null;


    public function __construct()
    {
    }

    public function getData()
    {
        if ($this->data == null) {
            $costs = $this->getModel()->getAll('id');
            foreach ($costs as $id => $data) {
                $this->data[$id] = $this->buildCost($data);
            }
        }

        return $this->data;
    }

    public function setByEntity(manufactoryEntityProduct $entity, $data = array())
    {

        $model = $this->getEntityModel($entity);
        $change = $model->setByEntity($entity->getId(), $data);
        if ($change) {
            $entity->setChangeCosts(true);
        }
    }

    public function deleteByEntity(manufactoryEntityProduct $entity, $data = array())
    {

        $model = $this->getEntityModel($entity);
        $change = $model->deleteByEntity($entity->getId(), $data);
        if ($change) {
            $entity->setChangeCosts(true);
        }
    }

    public function getEntityData(manufactoryEntityProduct $entity)
    {
        $class = static::ENTITY_COSTS_CLASS_NAME;

        return wao(new $class($entity));
    }

    public function getByEntity(manufactoryEntityProduct $entity)
    {


        $model = $this->getEntityModel($entity);
        $data = array();
        $type = static::TYPE;
        if (!empty($type) && $model) {
            $costs = $model->getByEntity($entity->getId());
            foreach ($costs as $id => $entity_cost) {
                $data[$id] = $this->buildEntityCost($entity, $entity_cost);
            }
        }

        return $data;
    }

    protected function getModel($name = null)
    {
        if (!empty($name)) {
            return parent::getModel($name);
        }
        if ($this->cost_model == null) {
            $model_name = static::COST_MODEL;
            $this->cost_model = parent::getModel($model_name);
        }

        return $this->cost_model;
    }

    function getEntityModel($entity)
    {
        $entity_type = $entity->getEntityType();

        if (!empty($entity_type) && !array_key_exists($entity_type, $this->entity_cost_models)) {
            if (array_key_exists($entity_type, $this->entity_cost_models_name)) {
                $model_name = $this->entity_cost_models_name[$entity_type];
            } else {
                //    CompositematerialsProductCosts
                $model_name = ucfirst($this->getTypeId()) . ucfirst($entity_type) . 'Costs';
            }

            $this->entity_cost_models[$entity_type] = wao($this->getModel($model_name));
        }
        if (array_key_exists($entity_type, $this->entity_cost_models)) {
            return $this->entity_cost_models[$entity_type];
        }

        return null;
    }

    public function buildCost($cost_data)
    {
        $class_name = static::COST_CLASS_NAME;

        return new $class_name($cost_data);
    }

    public function buildEntityCost($entity, $entity_cost)
    {
        $class_name = $this->getEntityCostClass($entity);
        $product_cost = new $class_name($this->getCost($entity_cost['cost_id']), $entity_cost['quantity']);

        return wao(new $class_name($this->getCost($entity_cost['cost_id']), $entity_cost['quantity']));
    }

    public function getEntityCostClass($entity)
    {
        $entity_type = $entity->getEntityType();
        if (array_key_exists($entity_type, $this->entity_costs_class_name)) {
            return $this->entity_costs_class_name[$entity_type];
        }
    }

    public function setCost($data)
    {
        $cost = array();
        if (is_array($data) && array_key_exists('id', $data)) {
            $cost = $data;
        } elseif (is_numeric($data)) {
            $cost = $this->getModel()->getById($data);
        }
        if (!empty($cost)) {
            $this->data[$data['id']] = $this->buildCost($cost);
        }
    }

    public function getCost($data)
    {
        if (!is_array($this->data)) {
            $this->getData();
        }
        if (is_array($data) && isset($data['id'])) {
            $id = $data['id'];
        } elseif (is_numeric($data)) {
            $id = intval($data);
        }
        if (!array_key_exists($id, $this->data)) {
            $this->setCost($data);
        }
        if (array_key_exists($id, $this->data)) {
            return $this->data[$id];
        }

        return false;
    }

    public function getCategories()
    {
        $model = $this->getModel('CategoryMaterials');
        $model->select('*')->order('sort')->fetchAll('id');

        return $model->select('*')->order('sort')->fetchAll('id');
    }

    public function getByCategory($id = 0)
    {
        $model = $this->getModel('Material');
        $data = $model->select('id')->where('category_id = ' . intval($id))->order('sort')->fetchAll('id');
        $return = array();
        foreach ($data as $v) {
            $return[$v['id']] = $this->getCost($v['id']);
        }

        return $return;
    }


}