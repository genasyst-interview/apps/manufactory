<?php

class manufactoryCostsTypeMaterials extends manufactoryCostsType
{
    const TYPE = 'materials';
    const COST_CLASS_NAME = 'manufactoryCostMaterial';
    const COST_MODEL = 'CostMaterial';
    const ENTITY_COSTS_CLASS_NAME = 'manufactoryCostMaterialProductCosts';
    protected $entity_costs_class_name = array(
        'product' => 'manufactoryCostMaterialProduct',
        'sku'     => 'manufactoryCostMaterialSku',
    );
    protected $entity_cost_models_name = array(
        'product' => 'ProductMaterials',
        'sku'     => 'SkuMaterials',
    );

    public function getTypeId()
    {
        return 'materials';
    }

    public function getName()
    {
        return 'Материалы';
    }
}