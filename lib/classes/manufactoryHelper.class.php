<?php

class manufactoryHelper
{
    //// PRODUCT CATEGORIES
    public static function getProductSubcategories($category_id = 0)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('categoryProducts');

        return $model->query("SELECT * FROM " . $model->getTableName() . " WHERE parent_id = '" . intval($category_id) . "' ORDER BY name ")->fetchAll('id');
    }

    public static function categories($category_id)
    {
        return self::getProductSubcategories($category_id);
    }

    public static function category($id)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('categoryProducts');

        return $model->getById($id);
    }

    // Materials
    public static function getMaterials($category_id)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('Material');
        $data = $model->select('id')->where('category_id = ' . intval($category_id))->order('sort')->fetchAll('id');
        $return = array();
        foreach ($data as $v) {
            $return[$v['id']] = self::getMaterial($v['id']);
        }

        return $return;
    }

    public static function getMaterial($id)
    {
        return manufactoryMaterialsPool::get($id);
    }

    // Sizes
    public static function getSize($id)
    {
        return manufactorySizesPool::get($id);
    }
}