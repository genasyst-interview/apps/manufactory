<?php

class manufactoryPrice implements ArrayAccess
{
    protected $data = array();
    protected $amount_pattern = '{amount}';
    protected $calculate_str = '';
    protected $full_name_str;
    protected $is_formula = false;
    protected $formula = array();


    public function __construct($data = null)
    {
        if (is_array($data)) {
            $this->data = $data;
        } elseif (is_numeric($data) && !empty($data)) {
            $models_pool = manufactoryModelsPool::getInstance();
            $price_model = $models_pool->get('MarginProduct');
            $data = $price_model->getById($data);
            if (!empty($data)) {
                $this->data = $data;
            }
        }
        if (!empty($this->data)) {
            $this->setData();
        }
    }

    public function getOfferPrice($offer_obj)
    {
        return new manufactoryProductSkuPrice($offer_obj, $this->getId());
    }

    protected function setData()
    {

        if (isset($this->data['koef'])) {
            if (@unserialize($this->data['koef'])) {
                $this->is_formula = true;
                $this->formula = unserialize($this->data['koef']);
                $arr_name = array();
                $this->calculate_str = '';
                foreach ($this->formula as $k => $v) {
                    $arr_name[$k] = $v;
                    if (preg_match('/{(\d*)}/', $v, $ids)) {
                        if ($ids[1] != $this->data['id']) {
                            $this->calculate_str .= 'manufactoryPricesPool::get(' . intval($ids[1]) . ')->calculate(' . $this->amount_pattern . ')';
                            $arr_name[$k] = manufactoryPricesPool::get($ids[1])->getName();
                        }
                    } else {
                        $v = str_replace(',', '.', $v);
                        $this->calculate_str .= $v;
                    }
                }
                $this->full_name_str = implode('', $arr_name);
            } else {
                $this->data['koef'] = str_replace(',', '.', $this->data['koef']);
                // Если не массив то просто перемножаем
                if ($this->data['type'] == 'value') {
                    $this->calculate_str = '1*{amount}';
                    $this->full_name_str = 'значение';
                } else {
                    $this->calculate_str = '{amount}*' . $this->data['koef'];
                    $this->full_name_str = '{amount}*' . $this->data['koef'];
                }
            }
            $this->calculate_str = '(' . $this->calculate_str . ')';
            if (!empty($this->data['round']) && !empty($this->data['round_to'])) {
                $this->full_name_str = '(' . $this->full_name_str . ')~' . $this->data['round_to'] . '' . ((!empty($this->data['round_type'])) ? '+' : '') . '';
                $this->data['full_name'] = '(' . $this->data['name'] . ')<span class="rounding">~' . $this->data['round_to'] . '' . ((!empty($this->data['round_type'])) ? '+' : '') . '</span>';
            }
        }
    }

    public function calculate($amount = 0, $round = null)
    {
        $amount = floatval($amount);
        if (empty($amount)) {
            return 0.0;
        }
        $result = manufactoryMatch::calculate($this->getCalculateString($amount));

        /// Можно задать вручную
        if ($round !== null) {
            if ($round > 1) {
                $result = $this->round($result, $round);
            }
        } else {
            $result = $this->round($result);
        }

        return $result;
    }

    protected function getCalculateString($amount = 0)
    {
        $str = str_replace($this->amount_pattern, $amount, $this->calculate_str);

        return $str;
    }

    public function getFormula()
    {
        return $this->formula;
    }

    public function getId()
    {
        return $this->data['id'];
    }

    public function getType()
    {
        return $this->data['type'];
    }

    public function getName()
    {
        return $this->data['name'];
    }

    public function getFullName()
    {
        if (!empty($this->data['full_name'])) {
            return $this->data['full_name'];
        }

        return $this->data['name'];

    }

    public function getCalculateName($amount = 0)
    {
        if ($amount == 0) {
            $amount = 'Стоимость';
        }

        return str_replace($this->amount_pattern, $amount, $this->full_name_str);
    }

    public function isFormula()
    {
        return $this->is_formula;
    }

    public function isStored()
    {
        if (preg_match('/save/', $this->getType())) {
            return true;
        }

        return false;
    }

    public function getUpdateTime()
    {
        return $this->data['update_datetime'];
    }

    /*
     * Проверяет были ли изменения в продуктах и материалах
     * если були , то цены хранимые можно будет обновить до последних изменений
     * */
    public function isUpdate()
    {
        if ($this->isStored()) {
            $models_pool = manufactoryModelsPool::getInstance();
            $materials_model = $models_pool->get('Material');
            $update_materials = $materials_model->select('id')->where("update_datetime > '" . $this->getUpdateTime() . "'")->fetchAll('id');
            if (!empty($update_materials)) {
                return true;
            } else {
                $product_model = $models_pool->get('Product');
                $update_products = $product_model->select('id')->where("update_datetime > '" . $this->getUpdateTime() . "'")->fetchAll('id');
                if (!empty($update_products)) {
                    return true;
                }
            }
        }

        return false;
    }

    public function isRound()
    {
        if (!empty($this->data['round']) && !empty($this->data['round_to'])) {
            return true;
        }

        return false;
    }

    public function round($amount = 0, $round = null)
    {
        if ($round === null) {
            $round = $this->data['round_to'];
        }
        if (!empty($this->data['round']) && !empty($round)) {
            if (!empty($this->data['round_type'])) {
                $result = $round * ceil($amount / $round); //в большую сторону принудительно
            } else {
                $result = $round * round($amount / $round); // авто
            }
        } else {
            $result = round(floatval($amount), 2);
        }

        return $result;
    }

    public function save($data = array())
    {
        if (empty($data['koef'])) {
            foreach ($data['formula'] as $v) {
                $data['koef'][] = $v;
            }
            $data['koef'] = serialize($data['koef']);
        } else {
            $data['koef'] = $data['koef'];
        }
        if (!empty($data['round_to'])) {
            if (isset($data['round']) && !empty($data['round'])) {
                $data['round'] = 1;
            } else {
                $data['round'] = 0;
            }
        } else {
            $data['round'] = 0;
        }
        $data['round_to'] = intval($data['round_to']);
        if (isset($data['round_type']) && !empty($data['round_type'])) {
            $data['round_type'] = 1;
        } else {
            $data['round_type'] = 0;
        }
        if ($data['type'] == 'value') {
            $data['koef'] = '1';
        }
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('MarginProduct');
        $old_data = $model->getById($this->getId());

        if (!empty($this->data['id'])) {
            $model->updateById($this->data['id'], $data);
            $id = $this->data['id'];
            $this->data = $model->getById($id);
            $this->setData();
            if (($data['koef'] != $old_data['koef']) && !$this->isStored()) {
                $this->recalculate();
            }
        } else {
            $id = $model->insert($data);
            $this->data = $model->getById($id);
            $this->setData();
            $this->updateDatetime();
        }


        return $id;
    }

    public function updateDatetime($datetime = '')
    {
        if (empty($datetime)) {
            $datetime = date("Y-m-d H:i:s");
        }
        $data = array();
        $data['update_datetime'] = $datetime;
        // save data
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('MarginProduct');
        $model->updateById($this->getId(), $data);
    }

    public function recalculate()
    {
        $recalc = new manufactoryRecalculatePrices();
        ///$recalc->recalculateMaterialsCoefficients();
        $error = array();
        $recalc->recalculatePrice($this->getId(), $error);

    }

    public function delete()
    {
        // Удаляем установленые сохраненные цены для товарных предложений
        $models_pool = manufactoryModelsPool::getInstance();
        $offer_price_model = $models_pool->get('ProductSkuPrice');

        $offer_price_model->deleteByField('price_id', $this->getId());
        // Удаляем саму цену
        $price_model = $models_pool->get('MarginProduct');
        $price_model->deleteById($this->getId());

        return true;
    }


    public function offsetExists($offset)
    {
        if (isset($this->data[$offset])) {
            return true;
        }

        return false;
    }

    public function offsetGet($offset)
    {
        if (isset($this->data[$offset])) {
            return $this->data[$offset];
        }

        return null;
    }

    public function offsetSet($offset, $value)
    {

        $this->data[$offset] = $value;

    }

    public function offsetUnset($offset)
    {

        $this->data[$offset] = null;

    }

}