<?php

class manufactoryPriceDelimiter extends manufactoryPrice implements ArrayAccess
{
    protected $data = array();
    protected $amount_pattern = '{amount}';
    protected $calculate_str = '';
    protected $full_name_str;
    protected $is_formula = false;
    protected $formula = array();


    public function getOfferPrice($offer_obj)
    {
        return new manufactoryProductSkuPriceDelimiter($offer_obj, $this->getId());
    }

    protected function setData()
    {
        $this->calculate_str = '';
        $this->full_name_str = '';
        $this->data['full_name'] = $this->data['name'];
    }


    public function calculate($amount = 0, $round = null)
    {

        return 0.0;
    }

    protected function getCalculateString($amount = 0)
    {
        return '';
    }

    public function getFormula()
    {
        return $this->formula;
    }

    public function getId()
    {
        return $this->data['id'];
    }

    public function getType()
    {
        return $this->data['type'];
    }

    public function getName()
    {
        return $this->data['name'];
    }

    public function getFullName()
    {
        if (!empty($this->data['full_name'])) {
            return $this->data['full_name'];
        }

        return $this->data['name'];

    }

    public function getCalculateName($amount = 0)
    {
        return '';
    }

    public function isFormula()
    {
        return false;
    }

    public function isStored()
    {
        return false;
    }

    public function getUpdateTime()
    {
        return $this->data['update_datetime'];
    }

    /*
     * Проверяет были ли изменения в продуктах и материалах
     * если були , то цены хранимые можно будет обновить до последних изменений
     * */
    public function isUpdate()
    {
        return false;
    }

    public function isRound()
    {
        return false;
    }

    public function round($amount = 0, $round = null)
    {
        return 0;
    }

    public function save($data = array())
    {
        if (empty($data['koef'])) {
            foreach ($data['formula'] as $v) {
                $data['koef'][] = $v;
            }
            $data['koef'] = serialize($data['koef']);
        } else {
            $data['koef'] = $data['koef'];
        }
        if (!empty($data['round_to'])) {
            if (isset($data['round']) && !empty($data['round'])) {
                $data['round'] = 1;
            } else {
                $data['round'] = 0;
            }
        } else {
            $data['round'] = 0;
        }
        $data['round_to'] = intval($data['round_to']);
        if (isset($data['round_type']) && !empty($data['round_type'])) {
            $data['round_type'] = 1;
        } else {
            $data['round_type'] = 0;
        }
        if ($data['type'] == 'value') {
            $data['koef'] = '1';
        }
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('MarginProduct');
        if (!empty($this->data['id'])) {
            $model->updateById($this->data['id'], $data);
            $id = $this->data['id'];
        } else {
            $id = $model->insert($data);
            $this->data = $model->getById($id);
            $this->setData();
            $this->updateDatetime();
        }

        return $id;
    }

    public function updateDatetime($datetime = '')
    {
        if (empty($datetime)) {
            $datetime = date("Y-m-d H:i:s");
        }
        $data = array();
        $data['update_datetime'] = $datetime;
        // save data
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('MarginProduct');
        $model->updateById($this->getId(), $data);
    }

    public function delete()
    {
        // Удаляем установленые сохраненные цены для товарных предложений
        $models_pool = manufactoryModelsPool::getInstance();
        $offer_price_model = $models_pool->get('ProductSkuPrice');

        $offer_price_model->deleteByField('price_id', $this->getId());
        // Удаляем саму цену
        $price_model = $models_pool->get('MarginProduct');
        $price_model->deleteById($this->getId());

        return true;
    }


    public function offsetExists($offset)
    {
        if (isset($this->data[$offset])) {
            return true;
        }

        return false;
    }

    public function offsetGet($offset)
    {
        if (isset($this->data[$offset])) {
            return $this->data[$offset];
        }

        return null;
    }

    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        $this->data[$offset] = null;
    }

}