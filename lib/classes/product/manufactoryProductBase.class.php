<?php

class manufactoryProductBase extends manufactoryEntityProduct implements ArrayAccess
{
    protected $change_costs = false;
    protected $storage_objects = array(
        'costs'    => 'manufactoryProductCosts',
        'features' => 'manufactoryProductFeatures',
        'skus'     => 'manufactoryProductSkus',
        'prices'   => 'manufactoryProductPrices',

    );

    public function getType()
    {
        return $this->data['type_id'];
    }

    public function getPrices()
    {
        return $this->getData('prices');
    }

    public function init($data)
    {
        if (is_array($data) && isset($data['id'])) {
            $this->data = $data;
        } elseif (is_numeric($data) && intval($data) > 0) {
            $model = $this->getModel('Product');
            $data = $model->getById($data);
            if (!empty($data)) {
                $this->data = $data;
            }
        }
        foreach ($this->storage_objects as $id => $class) {
            if (class_exists($class)) {
                $this->storage[$id] = new $class($this);
            }
        }
    }

    public function getMinMaxPrice()
    {
        $costs = array();
        foreach ($this->getSkus() as $v) {
            $costs[] = $v->getCost();
        }
        if (count($costs) > 1) {
            sort($costs);

            return array('min' => round(array_shift($costs), 2), 'max' => round(array_pop($costs), 2));
        } else {
            $cost = round(array_shift($costs), 2);

            return array('min' => $cost, 'max' => $cost);
        }
    }

    public function getExcludedCombinations()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $product_type_model = $models_pool->get('ProductType');
        $product_type = $product_type_model->getById($this->getType());

        return $product_type->getExcludedCombinations();
    }

    public function recalculatePrices($prices = array())
    {
        if (empty($prices)) {
            $prices = manufactoryPricesPool::geByType('generate');
        }
        if (is_array($prices) && !empty($prices)) {
            foreach ($this->getSkus() as $offer) {
                $offer->recalculatePrices($prices);
            }
        }
    }

    public function recalculatePrice($id)
    {
        $prices = array();
        if (is_object($id)) {
            $prices[$id->getId()] = $id;
        } elseif (is_numeric($id)) {
            $prices[$id->getId()] = manufactoryPricesPool::get($id);
        }
        if (!empty($prices)) {
            return $this->recalculatePrices($prices);
        } else {
            return false;
        }
    }

    public function getCost()
    {
        $price = 0.0;
        foreach ($this->getCosts() as $cost_type) {
            $price += $cost_type->getPrice();
        }

        return $price;
    }

    public function getFeatures()
    {
        return $this->getData('features');
    }

    public function getSkus()
    {
        return $this->getData('skus');
    }

    public function updateProductDatetime($datetime = '')
    {
        if (empty($datetime)) {
            $datetime = date("Y-m-d H:i:s");
        }
        $data = array();
        $data['update_datetime'] = $datetime;
        // save data
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('Product');
        $model->updateById($this->getId(), $data);
    }

    public function save($data)
    {
        // save product data
        $storage_data = array();
        foreach ($this->storage_objects as $k => $item) {
            if (array_key_exists($k, $data)) {
                $storage_data[$k] = $data[$k];
                unset($data[$k]);
            }
        }
        $model = $this->getModel('Product');
        if ($this->getId()) {
            $model->updateById($this->getId(), $data);
            $p_id = $this->getId();
        } else {
            $data['update_datetime'] = $data['create_datetime'] = date("Y-m-d H:i:s");
            $p_id = $model->insert($data);
        }
        $this->setData($model->getById($p_id));
        /// сохранение после создания
        $this->setData($storage_data);
        foreach ($this->storage as $object) {
            if (!method_exists($object, 'save')) {
                throw new waException(get_class($object) . ' not method save');
            }
            $object->save();
        }
        wa()->event('backend_product_save', $this);

        if ($this->change_costs) {
            // Если состав изменился меняем дату обновления продута
            $this->updateProductDatetime();
            ///  а также пересчитываем все нехранимые цены, хранимые обновим в самой цене
            $prices = manufactoryPricesPool::geByType('generate');
            $this->recalculatePrices($prices);
        }
    }

    public function delete()
    {
        // Удаляем предложения продукта
        foreach ($this->getSkus() as $v) {
            $v->delete();
        }

        $models_pool = manufactoryModelsPool::getInstance();
        $features_model = $models_pool->get('ProductFeatures');
        $features_model->deleteByEntity($this->getId());

        $product_materials = $models_pool->get('ProductMaterials');
        $product_materials->deleteByEntity($this->getId());
        // Удаляем сам продукт
        $model = $models_pool->get('Product');
        $model->deleteById($this->getId());

        return true;
    }

    public function duplicate()
    {
        $duplicate = $this->getFactory('Products')->getType($this->getData('type_id'))->getProduct(null);
        // save product data
        $data = $this->data;
        $data['name'] = $data['name'] . ' (' . date("Y_m_d H:i:s") . ')';
        unset($data['id']);
        $costs_types = $this->getData('costs');
        //costs[compositematerials][16]
        foreach ($costs_types as $type => $costs) {
            foreach ($costs as $id => $v) {
                $data['costs'][$type][$v->getId()] = $v->getQuantity();
            }

        }
        // features[2][38] = 38
        $features = $this->getData('features');
        foreach ($features as $feature) {
            $f_id = $feature->getFeatureId();
            foreach ($feature->getValues() as $value) {
                $data['features'][$f_id][$value['id']] = $value['id'];
            }
        }
        $model = new manufactoryProductSkuModel();
        $skus = $model->getByEntity($this->getId());
        foreach ($skus as $sku) {
            /// unserialize fix sku index
            $unserialized = @unserialize($sku['sku_data']);
            if ($sku['sku_data'] === 'b:0;' || $unserialized !== false) {
                $data['skus'][$sku['id']] = $unserialized;
            }
        }

        return $duplicate->save($data);
    }

    public function offsetExists($offset)
    {
        if (isset($this->data[$offset])) {
            return true;
        }

        return false;
    }

    public function offsetGet($offset)
    {
        if (isset($this->data[$offset])) {
            return $this->data[$offset];
        }

        return null;
    }

    public function offsetSet($offset, $value)
    {

        $this->data[$offset] = $value;

    }

    public function offsetUnset($offset)
    {
        $this->data[$offset] = null;
    }
}