<?php

class manufactoryProductSku extends manufactoryEntity
{
    /* Данные самого предложения */
    protected $data = array();
    /* Объект продукта */
    protected $product = null;
    /* Характеристики предложения */
    protected $features = null;
    /*  Себестоимость предложения, формируется из стоимости материалов */
    protected $cost = 0.0;
    /* Идентификатор характеристик предложения sku_index или для произвольного предложения массив характеристик */
    protected $sku_data = null;
    protected $custom = false;

    public function __construct($product = null, $data = null)
    {


        // Получаем продукт
        $this->setProduct($product);
        /// Получаем данные оффера
        if (is_array($data) && !empty($data)) {
            $this->setData($data);
        } elseif (is_numeric($data)) {
            $model = $this->getModel('ProductSku');
            $of_d = $model->getById($data);
            if (is_array($of_d) && !empty($of_d)) {
                $this->setProduct($of_d['product_id']);
                $this->setData($of_d);
            }
        }
    }

    public function setData($data)
    {
        $this->data = $data;
        if (!empty($this->data)) {
            $this->setSkuData($this->data['sku_data']);
        }
    }

    public function getCosts()
    {
        $return = array();
        foreach ($this->product->getCosts() as $cost_type => $costs) {
            foreach ($costs as $id => $product_cost) {
                $return[$cost_type][$id] = new manufactoryProductSkuCost($product_cost, $this);
            }
        }

        return $return;
    }

    public function getCost()
    {
        $price = 0.0;
        foreach ($this->getCosts() as $cost_type_id => $costs) {
            foreach ($costs as $cost) {
                $price += (float)$cost->getPrice();
            }
        }

        return $price;
    }

    public function getType()
    {
        return $this->data['type'];
    }

    public function isCustom()
    {
        if ($this->custom) {
            return true;
        }

        return false;
    }

    public function getSkuData()
    {
        return $this->sku_data;
    }

    public function setSkuData($data)
    {
        /// unserialize fix sku index
        $unserialized = @unserialize($data);
        if ($data === 'b:0;' || $unserialized !== false) {
            $this->sku_data = $unserialized;
            $this->custom = true;
        } else {
            $this->sku_data = $data;
        }
    }

    public function getFeatures()
    {
        if ($this->features == null) {
            if ($this->isCustom()) {
                $this->features = $this->sku_data;
            } else {
                $this->features = array();
                foreach ($this->getModel('ProductSkuFeatures')->getByOffer($this->getId()) as $v) {
                    $this->features[$v['feature_id']] = $v['value_id'];
                }
            }
        }

        return $this->features;
    }

    public function setProduct($product = null)
    {
        if (is_object($product)) {
            $this->product = $product;
        } elseif (intval($product) > 0) {
            if (!empty($product)) {
                $this->product = wa('manufactory')->getConfig()->getFactory('Products')->get($product);
            }
        }
    }

    public function getProduct()
    {
        if (is_object($this->product)) {
            return $this->product;
        }

        return false;
    }

    public function rename()
    {
        $features = $this->getProduct()->getFeatures();
        $name = '';
        $_fe = $this->getFeatures();
        foreach ($features as $feature_id => $data) {
            if (array_key_exists($feature_id, $_fe)) {
                if ($this->isCustom()) {
                    $data = $_fe[$feature_id];
                    if (is_array($data) && !empty($data) &&
                        isset($data['value']) && is_numeric($data['value'])
                        && isset($data['unit']) && !empty($data['unit'])
                    ) {
                        $name .= $data['value'] . ' x ';
                    }
                } else {
                    $val = $features[$feature_id]->getValueData($_fe[$feature_id]);
                    $name .= $val['value'] . ' x ';
                }
            }
        }
        $name = rtrim($name, ' x ');

        $model = manufactoryModelsPool::getInstance()->get('ProductSku');
        $model->updateById($this->getId(), array('name' => $name));

    }

    public function getPrice($price_id)
    {
        return $this->getProduct()->getPrices()->getSkuPrice($price_id, $this);
    }

    public function recalculatePrice($id)
    {
        $this->getPrice($id)->recalculate();
    }

    public function recalculatePrices($ids = array())
    {
        $prices = array();
        if (empty($ids)) {
            $prices = manufactoryPricesPool::geByType('generate');
        } else {
            foreach ($ids as $id => $price) {
                if (is_object($price)) {
                    $prices[$id] = $price;
                } else {
                    $prices[$id] = manufactoryPricesPool::get($id);
                }
            }
        }
        foreach ($prices as $price_id => $obj_price) {
            $this->recalculatePrice($price_id);
        }
    }


    public function delete()
    {
        // Удаляем сохраненные цены предложения
        $models_pool = manufactoryModelsPool::getInstance();
        $prices_model = $models_pool->get('ProductSkuPrice');
        $prices_model->deleteByField(array(
            'offer_id' => $this->getId(),
        ));
        // Удаляем характеристики предложения
        $features_model = $models_pool->get('ProductSkuFeatures');
        $features_model->deleteByField(array(
            'offer_id' => $this->getId(),
        ));

        // Удаляем само предложение
        $model = $models_pool->get('ProductSku');
        $model->deleteById($this->getId());
    }

    public function save($data, $features_values = null)
    {
        $model = $this->getModel('ProductSku');
        if (isset($data['id']) && !empty($data['id'])) {
            $data['update_datetime'] = date("Y-m-d H:i:s");
            $model->updateById($data['id'], $data);
            $id = $data['id'];
        } else {
            $data['update_datetime'] = $data['create_datetime'] = date("Y-m-d H:i:s");
            $id = $model->insert($data);
        }
        $new_data = $model->getById($id);
        $this->setData($new_data);
        if (!empty($id) && !empty($features_values)) {
            $model_features = $this->getModel('ProductSkuFeatures');
            $model_features->deleteByOffer($id);
            $features = $model_features->getByOffer($id);
            foreach ($features_values as $feature_id => $value_id) {
                if (!isset($features[$feature_id])) {
                    $feat_data = array(
                        'offer_id'   => $id,
                        'feature_id' => $feature_id,
                        'value_id'   => $value_id,
                    );
                    $model_features->insert($feat_data);

                } elseif ($features[$feature_id]['value_id'] != $value_id) {
                    $feat_data = array(
                        'value_id' => $value_id,
                    );
                    $model_features->updateByField(array(
                        'offer_id'   => $id,
                        'feature_id' => $feature_id),
                        $feat_data);
                }
                unset($features[$feature_id]);
            }
            // Удаляем старые которых уже нет
            foreach ($features as $feature) {
                $model_features->deleteByField(array(
                        'offer_id'   => $id,
                        'feature_id' => $feature['feature_id'])
                );
            }
        }
        $this->recalculatePrices();


    }
}