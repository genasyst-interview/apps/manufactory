<?php

class manufactoryProductFeature
{

    protected $feature = null;
    protected $values = array();
    protected $id = 0;

    public function __construct($id, $values)
    {
        $this->values = $values;
        $this->id = $id;
    }

    public function getFeatureId()
    {
        return $this->id;
    }

    public function getFeature()
    {
        if (empty($this->feature)) {
            $this->feature = manufactoryFeaturesPool::getInstance()->get($this->getFeatureId());
        }

        return $this->feature;
    }

    public function getName()
    {
        return $this->getFeature()->getName();
    }

    public function getValueData($id)
    {
        return $this->getFeature()->getValueData($id);
    }

    public function getValues()
    {
        $values = array();
        foreach ($this->values as $id => $v) {
            $values[$id] = $this->getValueData($id);
        }

        return $values;
    }

}