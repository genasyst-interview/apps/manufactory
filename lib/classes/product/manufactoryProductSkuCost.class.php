<?php

class manufactoryProductSkuCost
{

    protected $product_cost = null;
    protected $sku = null;

    public function __construct($product_cost, $sku)
    {
        $this->product_cost = $product_cost;
        $this->sku = $sku;

    }

    public function getName()
    {
        return $this->product_cost->getName();
    }

    public function getProductCost()
    {
        return $this->product_cost;
    }

    public function getPrice()
    {
        return $this->product_cost->getPrice($this->sku->getFeatures());
    }

    public function getCalculateString()
    {
        return $this->product_cost->getCalculateString($this->sku->getFeatures());
    }

    public function getCoefficient()
    {
        return $this->product_cost->getCoefficient($this->sku->getFeatures());
    }

    public function __call($name, $arguments)
    {
        if (is_callable(array($this->product_cost, $name))) {
            if ($name == 'getSkuCosts') {
                return $this->product_cost->getSkuCosts($this->sku);
            } else {
                return call_user_func_array(array($this->product_cost, $name), $arguments);
            }
        }
    }
}