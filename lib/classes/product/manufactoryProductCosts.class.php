<?php

/**
 * Class manufactoryProductCosts
 */
class manufactoryProductCosts extends manufactoryEntityCosts
{
    public function init()
    {
        // Получаем типы расходов
        $data = $this->getFactory('Costs')->getTypes();
        foreach ($data as $type_id => $type) {
            $this->data[$type_id] = $type->getEntityData($this->getEntity());
        }


    }

    public function getEntityType()
    {
        // TODO: Implement getEntityType() method.
    }

    public function setData($data = array(), $key = null)
    {
        foreach ($this->getData() as $type => $storage) {
            if (array_key_exists($type, $this->data) && array_key_exists($type, $data)) {
                $storage->setData($data[$type]);
            } else {
                $storage->setData(array());
            }
        }
    }

    public function save()
    {
        foreach ($this->getData() as $storage) {
            $storage->save();
        }
    }

    public function delete()
    {
        foreach ($this->getData() as $storage) {
            $storage->delete();
        }
    }

    public function getData($key = null)
    {
        if ($key == null) {
            /*$costs = array();
            foreach ($this->data as $type_id => $type)  {
                $costs[$type_id] = $type->getEntityData($this->getEntity());
            }
            foreach ($costs as $cost_type) {
                foreach ($cost_type as $v) {
                   // var_dump($v->getName().'='.$v->getCost());
                   // var_dump($v->getName().'='.$v->getCost());
                }
            }
            return $costs;*/
            return $this->data;
        } elseif (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }

        return false;
    }

    public function getPrice($entity = null)
    {
        if ($entity == null) {
            $entity = $this->getEntity();
        }
        foreach ($this->getCosts($entity) as $cost_type) {
            $cost_type->getPrice();
        }
    }
}