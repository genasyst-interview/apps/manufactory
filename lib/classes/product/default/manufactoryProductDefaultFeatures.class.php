<?php

class manufactoryProductDefaultFeatures extends manufactoryEntityProductStorage
{

    protected $product_id = 0;
    protected $model = 'ProductFeatures';

    public function getEntityId()
    {
        return $this->getEntity()->getId();
    }

    public function getFeature($id)
    {
        return manufactoryFeaturesPool::getInstance()->get($id);
    }

    public function init()
    {
        $features_data = $this->getModel()->getByEntity($this->getEntityId());
        foreach ($features_data as $v) {
            $this->data[$v['feature_id']] = $v['value_id'];
        }
        foreach ($this->data as $id => &$value) {
            $value = new manufactoryProductDefaultFeature($id, $value);
        }
        unset($value);
    }

    public function offsetExists($offset)
    {
        if (isset($this->data[$offset])) {
            return true;
        }

        return false;
    }

    public function offsetGet($offset)
    {
        if (isset($this->data[$offset])) {
            return $this->data[$offset];
        }

        return null;
    }

    public function offsetSet($offset, $value)
    {

        $this->data[$offset] = $value;

    }

    public function offsetUnset($offset)
    {
        $this->data[$offset] = null;
    }
}