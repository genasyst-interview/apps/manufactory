<?php

class manufactoryProductDefaultSkus extends manufactoryEntityProductStorage
{
    protected $model = 'ProductSku';
    // Данные для сохранения новых произвольных артикулов
    protected $save_data = array();

    public function init()
    {
        $model = $this->getModel('ProductSku');
        $data = $model->getByEntity($this->getEntityId());
        $this->data = array();
        foreach ($data as $sku_id => $sku) {
            $this->data[$sku_id] = new manufactoryProductSku($this->getEntity(), $sku);
        }
    }

    public function setData($data = array(), $key = null)
    {
        $this->save_data = $data;
    }

    public function save()
    {
        $features = $this->getEntity()->getData('features');
        $model = $this->getModel();

    }


}