<?php

class manufactoryProductDefaultFeature
{

    protected $feature = null;
    protected $data = array();
    protected $id = 0;

    public function __construct($id, $data)
    {
        $this->data = $data;
        $this->id = $id;
    }

    public function getFeatureId()
    {
        return $this->id;
    }

    public function getFeature()
    {
        if (empty($this->feature)) {
            $this->feature = manufactoryFeaturesPool::getInstance()->get($this->getFeatureId());
        }

        return $this->feature;
    }

    public function getName()
    {
        return $this->getFeature()->getName();
    }

    public function getValue()
    {
        return $this->getFeature()->getValueData($this->data);
    }

    public function getValueData($id)
    {
        return $this->getFeature()->getValueData($id);
    }

    public function getFeatureValues()
    {
        return $this->getFeature()->getValues();
    }


}