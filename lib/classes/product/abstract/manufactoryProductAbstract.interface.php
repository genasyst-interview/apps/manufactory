<?php

interface manufactoryProductAbstractInterface extends manufactoryEntityProductInterface
{
    // protected $change_costs = false;
    // protected $storage_objects = array();
    public function getType();

    public function init($data);

    public function getMinMaxPrice();

    public function recalculatePrices($prices = array());

    public function recalculatePrice($id);

    // Storage Objects
    public function getPrices();

    public function getFeatures();

    public function getSkus();

    public function updateProductDatetime($datetime = '');

    public function setChangeCosts($flag);

    public function isChangeCosts();

    public function save($data);

    public function delete();

    public function duplicate();

}