<?php

interface manufactoryProductMultipleInterface extends manufactoryProductAbstractInterface
{

    public function getExcludedCombinations();
}