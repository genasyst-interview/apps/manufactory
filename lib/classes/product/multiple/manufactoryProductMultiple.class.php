<?php

class manufactoryProductMultiple extends manufactoryProductAbstract implements manufactoryProductMultipleInterface
{
    public function getExcludedCombinations()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $product_type_model = $models_pool->get('ProductType');
        $product_type = $product_type_model->getById($this->getType());

        return $product_type->getExcludedCombinations();
    }

    public function duplicate()
    {
        $duplicate = $this->getFactory('Products')->getType($this->getData('type_id'))->getProduct(null);
        // save product data
        $data = $this->data;
        $data['name'] = $data['name'] . ' (' . date("Y_m_d H:i:s") . ')';
        unset($data['id']);
        $costs_types = $this->getData('costs');
        //costs[compositematerials][16]
        foreach ($costs_types as $type => $costs) {
            foreach ($costs as $id => $v) {
                $data['costs'][$type][$v->getId()] = $v->getQuantity();
            }

        }
        // features[2][38] = 38
        $features = $this->getData('features');
        foreach ($features as $feature) {
            $f_id = $feature->getFeatureId();
            foreach ($feature->getValues() as $value) {
                $data['features'][$f_id][$value['id']] = $value['id'];
            }
        }
        $model = new manufactoryProductSkuModel();
        $skus = $model->getByEntity($this->getId());
        foreach ($skus as $sku) {
            /// unserialize fix sku index
            $unserialized = @unserialize($sku['sku_data']);
            if ($sku['sku_data'] === 'b:0;' || $unserialized !== false) {
                $data['skus'][$sku['id']] = $unserialized;
            }
        }

        return $duplicate->save($data);
    }

}