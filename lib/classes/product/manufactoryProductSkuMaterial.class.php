<?php

class manufactoryProductSkuMaterial
{

    protected $product_material = null;
    protected $sku_data = null;

    public function __construct($product_material, $sku_data)
    {
        $this->product_material = $product_material;
        $this->sku_data = $sku_data;

    }

    public function getPrice()
    {
        return $this->product_material->getPrice($this->sku_data);
    }

    public function getCalculateString()
    {
        return $this->product_material->getCalculateString($this->sku_data);
    }

    public function getCoefficient()
    {
        return $this->product_material->getCoefficient($this->sku_data);
    }

    public function __call($name, $arguments)
    {
        if (method_exists($this->product_material, $name)) {
            return call_user_func_array(array($this->product_material, $name), $arguments);
        }
    }

}