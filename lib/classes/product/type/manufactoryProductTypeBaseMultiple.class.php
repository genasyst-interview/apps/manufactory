<?php

class manufactoryProductTypeBaseMultiple extends manufactoryProductTypeBase
{
    public function getProduct($data)
    {
        return new manufactoryProductMultiple($data);
    }

    public function getExcludedCombinations()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('ProductTypeExcludedCombinations');
        $indexes = $model->getByType($this->getId());
        foreach ($indexes as &$index) {
            $index['name'] = manufactoryFeaturesSkusGenerate::getSkuIndexName($index['index']);
            $index['features'] = manufactoryFeaturesSkusGenerate::getSkuIndexFeatures($index['index']);
        }
        unset($index);

        return $indexes;
    }

    public function getName()
    {
        return 'Многоартикульный продукт';
    }

    public function getDescription()
    {
        return 'Данный тип продукта позволяет добавить произвольные артикулы со своими составами затрат';
    }

    public function getFeatures()
    {

    }

    public function getCosts()
    {

    }
}