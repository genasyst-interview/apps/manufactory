<?php

class manufactoryProductType extends manufactoryEntity implements manufactoryProductTypeInterface, ArrayAccess
{
    protected $data = array();
    protected $base_type = null;

    public function __construct($data)
    {
        $this->setData($data);

    }

    /**
     * @return manufactoryProductTypeBase
     */
    protected function getBaseType()
    {
        return $this->base_type;
    }

    public function setData($data)
    {
        $this->data = $data;
        if (!empty($this->data['base_type'])) {
            $this->base_type = manufactoryProductTypesBase::getInstance()->getType($this->data['base_type']);
        } else {
            $this->base_type = manufactoryProductTypesBase::getInstance()->getType('default');
        }
    }

    public function getDescription()
    {
        if (isset($this->data['description'])) {
            return $this->data['description'];
        }

        return '';
    }

    public function getProduct($data)
    {
        return $this->getBaseType()->getProduct($data);
    }

    public function getId()
    {
        if (isset($this->data['id'])) {
            return $this->data['id'];
        }

        return false;
    }

    public function getName()
    {
        return $this->data['name'];
    }

    public function getFeatures()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('ProductTypesFeatures');
        $features_ids = $model->getByType($this->getId());
        $features = array();
        foreach ($features_ids as $v) {
            $features[$v['feature_id']] = manufactoryFeaturesPool::getInstance()->get($v['feature_id']);
        }

        return $features;
    }

    public function __call($name, $arguments)
    {
        if (method_exists($this->base_type, $name)) {
            return call_user_func_array(array($this->getBaseType(), $name), $arguments);
        }
    }

    public function offsetExists($offset)
    {
        if (isset($this->data[$offset])) {
            return true;
        }

        return false;
    }

    public function offsetGet($offset)
    {
        if (isset($this->data[$offset])) {
            return $this->data[$offset];
        }

        return null;
    }

    public function offsetSet($offset, $value)
    {

        $this->data[$offset] = $value;

    }

    public function offsetUnset($offset)
    {
        $this->data[$offset] = null;
    }

    public function delete()
    {

    }

    public function getExcludedCombinations()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('ProductTypeExcludedCombinations');
        $indexes = $model->getByType($this->getId());
        foreach ($indexes as &$index) {
            $index['name'] = manufactoryFeaturesSkusGenerate::getSkuIndexName($index['index']);
            $index['features'] = manufactoryFeaturesSkusGenerate::getSkuIndexFeatures($index['index']);
        }
        unset($index);

        return $indexes;
    }

    public function save($data)
    {
        if (!isset($data['excluded_combinations']) || !is_array($data['excluded_combinations'])) {
            $data['excluded_combinations'] = array();
        }
        $this->saveExcludedCombinations($data['excluded_combinations']);
    }

    public function saveExcludedCombinations($indexes)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('ProductTypeExcludedCombinations');
        $delete_indexes = $model->getByType($this->getId());
        foreach ($indexes as $id => $index) {
            if (is_array($index)) {
                $sku_index = manufactoryFeaturesSkusGenerate::getSkuByFeatures($index);
                if (isset($delete_indexes[$sku_index])) {
                    unset($delete_indexes[$sku_index]);
                } else {
                    $sku_data = array(
                        'product_type_id' => $this->getId(),
                        'index'           => $sku_index,
                    );
                    $model->insert($sku_data);
                }
            } else {
                if (isset($delete_indexes[$id])) {
                    unset($delete_indexes[$id]);
                }
            }
        }
        foreach ($delete_indexes as $index) {
            $model->deleteByField(array(
                'product_type_id' => $this->getId(),
                'index'           => $index['index'],
            ));
        }
    }
}