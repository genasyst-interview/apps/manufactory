<?php

class manufactoryProductTypeBaseDefault extends manufactoryProductTypeBase
{
    public function getProduct($data)
    {
        return new manufactoryProductDefault($data);
    }

    public function getName()
    {
        return 'Одноартикульный продукт';
    }

    public function getDescription()
    {
        return 'Данный тип продукта применим для продукта в определенными параметрами ,не имеющий вариаций.';
    }
} 