<?php

class manufactoryProductSkus extends manufactoryEntityProductStorage
{
    protected $model = 'ProductSku';
    // Данные для сохранения новых произвольных артикулов
    protected $save_data = array();

    public function init()
    {
        $model = $this->getModel('ProductSku');
        $data = $model->getByEntity($this->getEntityId());
        $this->data = array();
        foreach ($data as $sku_id => $sku) {
            $this->data[$sku_id] = new manufactoryProductSku($this->getEntity(), $sku);
        }
    }

    public function setData($data = array(), $key = null)
    {
        $this->save_data = $data;
    }

    protected function saveCustomSkus()
    {
        $skus = array();
        if (!empty($this->save_data) && is_array($this->save_data)) {
            foreach ($this->save_data as $temp_id => $features) {
                $full_features = 0;
                $sku_name = '';
                if (!empty($features)) {
                    foreach ($features as $feature_id => $value) {
                        if (is_array($value) && !empty($value) &&
                            isset($value['value']) && is_numeric($value['value'])
                            && isset($value['unit']) && !empty($value['unit'])
                        ) {
                            $full_features++;
                            $sku_name .= $value['value'] . ' ' . $value['unit'] . ', ';
                        }
                    }
                    $sku_name = rtrim($sku_name, ',');
                    if (count($features) == $full_features) {
                        $skus[$temp_id]['sku_data'] = serialize($features);
                        $skus[$temp_id]['name'] = $sku_name;
                    }
                }
            }

            if (!empty($skus)) {
                foreach ($skus as $value) {
                    $sku_data = array(
                        'product_id' => $this->getEntityId(),
                        'sku_data'   => $value['sku_data'],
                        'name'       => $value['name'],
                    );
                    $offer = new manufactoryProductSku($this->getEntity());
                    $offer->save($sku_data);
                }
            }
        }
    }

    public function save()
    {
        $features = $this->getEntity()->getData('features');
        $skus_features = wao(new manufactoryProductSkusGenerate($features->getData()))->generate();
        $model = $this->getModel();
        $skus_delete = $model->getByEntity($this->getEntityId(), 'sku_data');
        $sku_indexes = array();
        // #bug неоптимальная ненужная строка
        foreach ($this->getEntity()->getExcludedCombinations() as $index_arr) {
            $sku_indexes[$index_arr['index']] = 1;
        }
        foreach ($skus_features as $sku_data => $features_values) {
            if (array_key_exists($sku_data, $skus_delete)) {
                if ($this->getEntity()->isChangeCosts(STDIN)) {
                    $offer = new manufactoryProductSku($this->getEntity(), $skus_delete[$sku_data]);
                    $offer->recalculatePrices();
                }
                unset($skus_delete[$sku_data]);
            } else {
                $name = '';
                foreach ($features_values as $f_id => $val_id) {
                    $vv = $features->getFeature($f_id)->getValueData($val_id);
                    $name .= $vv['value'] . ' x ';
                }

                $name = rtrim($name, ' x ');
                /// add
                $offer_data = array(
                    'product_id' => $this->getEntityId(),
                    'sku_data'   => $sku_data,
                    'name'       => $name,
                );
                /// Если нет в исключения комбинаций сохраняем
                if (!array_key_exists($sku_data, $sku_indexes)) {
                    $off = new manufactoryProductSku($this->getEntity());
                    $off->save($offer_data, $features_values);
                }
            }
        }
        /// Удаляем старые артикулы размеров, не произвольные
        foreach ($skus_delete as $sku) {
            if (!@unserialize($sku['sku_data'])) {
                $offer = new manufactoryProductSku($this->getEntity(), $sku);
                $offer->delete();
            }

        }
        // Сохраняем произвольные размеры
        $this->saveCustomSkus();
    }


}