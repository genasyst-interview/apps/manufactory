<?php

class manufactoryProductOfferPrice
{
    protected $offer = null;
    protected $price_id = 0;

    public function __construct($offer, $price_id)
    {
        $this->offer = $offer;
        $this->price_id = $price_id;
    }

    public function calculate()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('ProductOfferPrice');
        $price = $model->getByField(array(
            'offer_id' => $this->offer->getId(),
            'price_id' => $this->price_id,
        ));
        if (!empty($price) && isset($price['price'])) {
            return floatval($price['price']);
        } else {
            if ($this->_getPrice()->getType() == 'value') {
                return '';
            } else {
                $this->savePrice($this->_getPrice()->calculate($this->offer->getCost()));
                $price = $model->getByField(array(
                    'offer_id' => $this->offer->getId(),
                    'price_id' => $this->price_id,
                ));
                if (!empty($price) && isset($price['price'])) {
                    return floatval($price['price']);
                } else {
                    return 0.0;
                }
            }

        }
        /*  if($this->_getPrice()->getType() == 'value' || preg_match('/save/i', $this->_getPrice()->getType())) {
  
              $model = $models_pool->get('ProductOfferPrice');
              $price = $model->getByField(array(
                  'offer_id'=> $this->offer->getId(),
                  'price_id'=> $this->price_id,
              ));
              if(!empty($price) && isset($price['price'])) {
                  return floatval($price['price']);
              } else {
                  return 0.0;
              }
          } elseif(strpos('save',$price->getType())!==false) {
  
          } else {
              return  $this->_getPrice()->calculate($this->offer->getCost());
          }*/
    }

    public function recalculate()
    {

        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('ProductOfferPrice');

        $price = $this->_getPrice()->calculate($this->offer->getCost());
        $offer_price = $model->getByField(array(
            'offer_id' => $this->offer->getId(),
            'price_id' => $this->price_id,
        ));
        if (!empty($offer_price)) {
            $model->updateById($offer_price['id'], array('price' => $price));
            $offer_price['price'] = $price;
        } else {
            $model->insert(array(
                'price'    => $price,
                'offer_id' => $this->offer->getId(),
                'price_id' => $this->price_id,
            ));
        }
    }

    protected function _getPrice()
    {
        return manufactoryPricesPool::get($this->price_id);
    }

    public function getCalculateName()
    {
        return manufactoryPricesPool::get($this->price_id)->getCalculateName($this->offer->getCost());
    }

    public function savePrice($price)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('ProductOfferPrice');
        $prices = $model->getByField(array(
            'offer_id' => $this->offer->getId(),
            'price_id' => $this->price_id,
        ));
        if (!empty($prices)) {
            return $model->updateById($prices['id'], array('price' => $price));
        } else {
            return $model->insert(array(
                'price'    => $price,
                'offer_id' => $this->offer->getId(),
                'price_id' => $this->price_id,
            ));
        }

    }

    public function __call($name, $arguments)
    {
        if (method_exists(manufactoryPricesPool::get($this->price_id), $name)) {
            return call_user_func_array(array(manufactoryPricesPool::get($this->price_id), $name), $arguments);
        }
    }
}