<?php

abstract class manufactoryProductStorageAbstract implements ArrayAccess, IteratorAggregate, Countable
{
    protected $entity = null;
    protected $data = array();

    public function __construct(manufactoryEntityProduct $entity)
    {
        $this->setEntity($entity);
        $this->init();
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function setEntity(manufactoryEntityProduct $entity)
    {
        $this->entity = $entity;
    }

    abstract public function init();

    public function getData($key = null)
    {
        if ($key == null) {
            return $this->data;
        }
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }

        return null;
    }

    public function save()
    {

    }

    public function getIterator()
    {
        // TODO: Implement getIterator() method.
    }

    public function offsetExists($offset)
    {
        if (array_key_exists($offset, $this->data)) {
            return true;
        }

        return false;
    }

    public function offsetGet($offset)
    {
        if (array_key_exists($offset, $this->data)) {
            return $this->data[$offset];
        }

        return null;
    }

    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) {
            unset($this->data[$offset]);
        }
    }

    public function count()
    {
        return count($this->data);
    }

    function __call($name, $arguments)
    {
        waLog::log('Вызван несуществующий метод ' . $name . '. Доступные методы: ' . var_export(get_class_methods($this), true));
    }
}