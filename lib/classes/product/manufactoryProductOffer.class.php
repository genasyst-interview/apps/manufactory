<?php

class manufactoryProductOffer
{
    /* Данные самого предложения */
    protected $data = array();
    /* Объект продукта */
    protected $product = null;
    /* Характеристики предложения */
    protected $features = array();
    /*  Себестоимость предложения, формируется из стоимости материалов */
    protected $cost = 0.0;
    /* Идентификатор характеристик предложения sku_index или для произвольного предложения массив характеристик */
    protected $sku_data = null;

    public function __construct($product = null, $data = null)
    {


        // Получаем продукт
        $this->setProduct($product);
        /// Получаем данные оффера
        if (is_array($data) && !empty($data)) {
            $this->setData($data);
        } elseif (is_numeric($data)) {
            $model = manufactoryModelsPool::getInstance()->get('ProductOffer');
            $of_d = $model->getById($data);
            if (is_array($of_d) && !empty($of_d)) {
                $this->setProduct($of_d['product_id']);
                $this->setData($of_d);
            }
        }
    }

    public function setData($data)
    {
        $this->data = $data;
        if (!empty($this->data)) {
            $this->setSkuData($this->data['sku_data']);
        }
        /// Если офер сохранен и есть его продукт
        if ($this->getId() && $this->getProduct()) {
            // получаем значения характеристик
            /* $models_pool = manufactoryModelsPool::getInstance();
             $model_feat = $models_pool->get('ProductOfferFeatures');

             foreach ($model_feat->getByOffer($this->getId()) as $v) {
                 $this->features[$v['feature_id']] = $v['value_id'];
             }*/

            ///  $this->features = $this->getFeatures();
            /// получаем себестоимость по цене материалов

            foreach ($this->getCosts() as $cost_type_id => $costs) {
                // if($cost_type_id=='materials') {
                foreach ($costs as $cost) {
                    $this->cost += (float)$cost->getPrice();
                }
                // }
            }
            /*  var_dump($this->sku_data);
              var_dump($mp);
              var_dump($this->cost);
              var_dump($cp);
  
              var_dump($pp);
              echo "\n\n\n";*/
        }
    }

    public function getCosts()
    {
        $return = array();
        foreach ($this->product->getCosts() as $cost_type => $costs) {
            foreach ($costs as $cost) {
                $return[$cost_type][$cost->getId()] = new manufactoryProductSkuCost($cost, $this);
            }
        }

        return $return;
    }

    public function getId()
    {
        if (isset($this->data['id'])) {
            return $this->data['id'];
        }

        return false;
    }

    public function getName()
    {
        return $this->data['name'];
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function getType()
    {
        return $this->data['type'];
    }

    public function isCustom()
    {
        if ($this->getType() == 'custom') {
            return true;
        }

        return false;
    }

    public function getSkuData()
    {
        return $this->sku_data;
    }

    public function setSkuData($data)
    {
        /// unserialize fix sku index
        $unserialized = @unserialize($data);
        if ($data === 'b:0;' || $unserialized !== false) {
            $this->sku_data = unserialize($data);
        } else {
            $this->sku_data = $data;
        }
    }

    public function getFeatures()
    {
        if ($this->isCustom()) {
            $features = $this->sku_data;
        } else {
            $models_pool = manufactoryModelsPool::getInstance();
            $model = $models_pool->get('ProductOfferFeatures');
            $features = array();
            foreach ($model->getByOffer($this->getId()) as $v) {
                $features[$v['feature_id']] = $v['value_id'];
            }
        }

        return $features;
    }

    public function getMaterials()
    {
        $materials = array();
        foreach ($this->product->getMaterials() as $id => $product_material) {
            $materials[$id] = new manufactoryProductOfferMaterial($product_material, $this->sku_data);
        }

        return $materials;
    }

    public function setProduct($product = null)
    {
        if (is_object($product)) {
            $this->product = $product;
        } elseif (intval($product) > 0) {
            // Получаем продукт
            $models_pool = manufactoryModelsPool::getInstance();
            $product_model = $models_pool->get('Product');
            $product = $product_model->getById($product);
            if (!empty($product)) {
                $this->product = wa('manufactory')->getConfig()->getFactory('product')->get($product);
            }
        }
    }

    public function getProduct()
    {
        if (is_object($this->product)) {
            return $this->product;
        }

        return false;
    }

    public function rename()
    {
        $features = $this->getProduct()->getFeatures();
        $name = '';
        $_fe = $this->getFeatures();
        foreach ($features as $feature_id => $data) {
            if (array_key_exists($feature_id, $_fe)) {
                if ($this->isCustom()) {
                    $data = $_fe[$feature_id];
                    if (is_array($data) && !empty($data) &&
                        isset($data['value']) && is_numeric($data['value'])
                        && isset($data['unit']) && !empty($data['unit'])
                    ) {
                        $name .= $data['value'] . ' x ';
                    }
                } else {
                    $val = $features[$feature_id]->getValue($_fe[$feature_id]);
                    $name .= $val['value'] . ' x ';
                }
            }
        }
        $name = rtrim($name, ' x ');
        waLog::dump($name);
        $model = manufactoryModelsPool::getInstance()->get('ProductOffer');
        $model->updateById($this->getId(), array('name' => $name));

    }

    public function getPrice($price_id)
    {
        $price = manufactoryPricesPool::get($price_id);

        return $price->getOfferPrice($this);
    }

    public function recalculatePrice($id)
    {
        $this->getPrice($id)->recalculate();
    }

    public function recalculatePrices($ids = array())
    {
        $prices = array();
        if (empty($ids)) {
            $prices = manufactoryPricesPool::geByType('generate');
        } else {
            foreach ($ids as $id => $price) {
                if (is_object($price)) {
                    $prices[$id] = $price;
                } else {
                    $prices[$id] = manufactoryPricesPool::get($id);
                }
            }
        }
        foreach ($prices as $price_id => $obj_price) {
            $this->recalculatePrice($price_id);
        }
    }


    public function delete()
    {
        // Удаляем сохраненные цены предложения
        $models_pool = manufactoryModelsPool::getInstance();
        $prices_model = $models_pool->get('ProductOfferPrice');
        $prices_model->deleteByField(array(
            'offer_id' => $this->getId(),
        ));
        // Удаляем характеристики предложения
        $features_model = $models_pool->get('ProductOfferFeatures');
        $features_model->deleteByField(array(
            'offer_id' => $this->getId(),
        ));

        // Удаляем само предложение
        $model = $models_pool->get('ProductOffer');
        $model->deleteById($this->getId());
    }

    public function save($data, $features_values = null)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('ProductOffer');
        if (isset($data['id']) && !empty($data['id'])) {
            $data['update_datetime'] = date("Y-m-d H:i:s");
            $model->updateById($data['id'], $data);
            $id = $data['id'];
        } else {
            $data['update_datetime'] = $data['create_datetime'] = date("Y-m-d H:i:s");
            $id = $model->insert($data);
        }
        $new_data = $model->getById($id);
        $this->setData($new_data);
        if (!empty($id) && !empty($features_values)) {
            $model_features = $models_pool->get('ProductOfferFeatures');
            $model_features->deleteByOffer($id);
            $features = $model_features->getByOffer($id);
            foreach ($features_values as $feature_id => $value_id) {
                if (!isset($features[$feature_id])) {
                    $feat_data = array(
                        'offer_id'   => $id,
                        'feature_id' => $feature_id,
                        'value_id'   => $value_id,
                    );
                    $model_features->insert($feat_data);

                } elseif ($features[$feature_id]['value_id'] != $value_id) {
                    $feat_data = array(
                        'value_id' => $value_id,
                    );
                    $model_features->updateByField(array(
                        'offer_id'   => $id,
                        'feature_id' => $feature_id),
                        $feat_data);
                }
                unset($features[$feature_id]);
            }
            // Удаляем старые которых уже нет
            foreach ($features as $feature) {
                $model_features->deleteByField(array(
                        'offer_id'   => $id,
                        'feature_id' => $feature['feature_id'])
                );
            }
        }
        $this->recalculatePrices();


    }
}