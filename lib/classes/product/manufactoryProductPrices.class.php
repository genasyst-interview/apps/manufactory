<?php

/**
 * Class manufactoryProductCosts
 */
class manufactoryProductPrices extends manufactoryEntityProductStorage
{
    protected $model = 'ProductSkuPrice';

    public function init()
    {
        if ($this->getEntityId() > 0) {
            $model = $this->getModel();
            $sql = "SELECT s.id AS sku_id,sp.price_id,sp.price FROM manufactory_product_sku s
  LEFT JOIN manufactory_product_offer_price sp ON s.id = sp.offer_id
  WHERE s.product_id = " . $this->getEntityId();
            $prices = $model->query($sql)->fetchAll();
            foreach ($prices as $v) {
                $this->data[$v['sku_id']][$v['price_id']] = floatval($v['price']);
            }
        }
    }

    public function getEntityType()
    {
        // TODO: независит от типа
    }

    public function setData($data = array(), $key = null)
    {

    }

    public function save()
    {

    }

    public function getData($key = null)
    {
        if ($key == null) {

            return $this->data;
        } elseif (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }

        return false;
    }

    public function setSkuPrice($price_id, $sku = null)
    {
        return new manufactoryProductSkuPrice($this, $sku, $price_id);
    }

    public function getSkuPrice($price_id, $sku = null)
    {
        return new manufactoryProductSkuPrice($this, $sku, $price_id);
    }

    public function delete()
    {
        /* $skus = $this->getEntity()->getSkus();
          $ids = array_keys($skus);
          waLog::dump($ids);
          parent::delete();*/
    }
}