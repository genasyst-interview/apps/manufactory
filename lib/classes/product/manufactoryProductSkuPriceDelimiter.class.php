<?php

class manufactoryProductSkuPriceDelimiter
{
    protected $offer = null;
    protected $price_id = 0;

    public function __construct($offer, $price_id)
    {
        $this->offer = $offer;
        $this->price_id = $price_id;
    }

    public function calculate()
    {
        return '';
    }

    public function recalculate()
    {
        return '';
    }

    protected function _getPrice()
    {
        return manufactoryPricesPool::get($this->price_id);
    }

    public function getCalculateName()
    {
        return '';

    }

    public function savePrice($price)
    {

    }

    public function __call($name, $arguments)
    {
        if (method_exists(manufactoryPricesPool::get($this->price_id), $name)) {
            return call_user_func_array(array(manufactoryPricesPool::get($this->price_id), $name), $arguments);
        }
    }
}