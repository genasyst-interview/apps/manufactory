<?php

class manufactoryProductMaterial
{
    protected $id = 0;
    protected $quantity = 1;

    public function __construct($id, $quantity)
    {
        $this->id = $id;
        $this->quantity = $quantity;
    }

    public function getCost()
    {
        return manufactoryMaterialsPool::get($this->id)->getCost();
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return manufactoryMaterialsPool::get($this->id)->getName();
    }

    public function getType()
    {
        return manufactoryMaterialsPool::get($this->id)->getType();
    }

    public function getPrice($sku_data)
    {

        $size_price = manufactoryMaterialsPool::get($this->id)->getPrice($sku_data);

        return $size_price * $this->quantity;
    }

    public function getCalculateString($sku_data)
    {
        $size_str = manufactoryMaterialsPool::get($this->id)->getCalculateString($sku_data);

        return '(' . $size_str . ')*' . $this->quantity;
    }

    public function getCoefficient($sku_data)
    {
        return manufactoryMaterialsPool::get($this->id)->getCoefficient($sku_data);
    }
}
