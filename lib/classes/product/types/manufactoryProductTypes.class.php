<?php

class manufactoryProductTypes extends manufactorySingletone
{
    protected static $_instance = null;

    protected $types = array();

    protected function __construct()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $product_type_model = $models_pool->get('ProductType');
        $this->types = $product_type_model->getAll('id');
    }

    public function get()
    {

        return $this->types;
    }

    public function getType($id)
    {
        if (array_key_exists($id, $this->types)) {
            return $this->types[$id];
        }

        return null;
    }
}