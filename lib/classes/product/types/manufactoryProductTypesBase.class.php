<?php

class manufactoryProductTypesBase extends manufactorySingletone
{
    protected static $_instance = null;

    protected $types = array();

    protected function __construct()
    {
        $types = array(
            'default' => new manufactoryProductTypeBaseDefault(),
        );
        wa('manufactory')->event('product_base_types', $types);
        foreach ($types as $type_id => $type) {
            if ($type instanceof manufactoryProductTypeBase) {
                $this->types[strtolower($type_id)] = $type;
            }
        }
    }

    public function get()
    {
        return $this->types;
    }

    public function getType($id)
    {
        $id = strtolower($id);
        if (array_key_exists($id, $this->types)) {
            return $this->types[$id];
        }
    }
}