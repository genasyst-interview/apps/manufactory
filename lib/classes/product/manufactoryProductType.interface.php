<?php

interface  manufactoryProductTypeInterface
{
    public function getProduct($data);

    public function getName();

    public function getDescription();
}