<?php

class manufactoryProductSkuPrice extends manufactoryClass
{

    protected $product_prices = null;
    protected $sku = null;
    protected $price_id = 0;

    public function __construct(manufactoryProductPrices $product_prices, $sku, $price_id)
    {
        $this->product_prices = $product_prices;
        $this->sku = $sku;
        $this->price_id = $price_id;
    }

    protected function getProductPrices()
    {
        return $this->product_prices;
    }

    public function calculate()
    {
        $sku_prices = $this->getProductPrices()->getData($this->sku->getId());
        if ($sku_prices && array_key_exists($this->price_id, $sku_prices)) {
            return $sku_prices[$this->price_id];
        } else {
            if ($this->_getPrice()->getType() == 'value') {
                return '';
            } else {
                $price = $this->_getPrice()->calculate($this->sku->getCost());
                $this->savePrice($price);

                return floatval($price);

            }
        }
    }

    public function recalculate()
    {
        $price = $this->_getPrice()->calculate($this->sku->getCost());
        $this->savePrice($price);
    }

    protected function _getPrice()
    {
        return manufactoryPricesPool::get($this->price_id);
    }

    public function getCalculateName()
    {
        return manufactoryPricesPool::get($this->price_id)->getCalculateName($this->sku->getCost());
    }

    public function savePrice($price)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('ProductSkuPrice');
        $prices = $model->getByField(array(
            'offer_id' => $this->sku->getId(),
            'price_id' => $this->price_id,
        ));
        if (!empty($prices)) {
            return $model->updateById($prices['id'], array('price' => $price));
        } else {
            return $model->insert(array(
                'price'    => $price,
                'offer_id' => $this->sku->getId(),
                'price_id' => $this->price_id,
            ));
        }

    }

    public function __call($name, $arguments)
    {
        if (method_exists(manufactoryPricesPool::get($this->price_id), $name)) {
            return call_user_func_array(array(manufactoryPricesPool::get($this->price_id), $name), $arguments);
        }
    }
}