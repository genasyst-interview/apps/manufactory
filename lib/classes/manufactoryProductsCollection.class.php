<?php

/*
 * Жалкое подобие коллекции, нужно в основном для сбора дополнительной информации хранилищ продукта */

class manufactoryProductsCollection
{

    protected $hash;
    protected $info = array();


    protected $prepared = false;


    protected $fields = array();
    protected $where;
    protected $having = array();
    protected $count;
    protected $order_by = 'p.id DESC';
    protected $group_by;
    protected $joins;
    protected $join_index = array();


    protected $models = array();

    public function __construct($hash = '')
    {
        $this->setHash($hash);
    }


    protected function setHash($hash)
    {
        if (is_array($hash)) {
            $hash = '/id/' . implode(',', $hash);
        }
        if (substr($hash, 0, 1) == '#') {
            $hash = substr($hash, 1);
        }
        $this->hash = trim($hash, '/');
        if ($this->hash == 'all') {
            $this->hash = '';
        }
        $this->hash = explode('/', $this->hash, 2);
    }

    protected function prepare($add = false)
    {
        if (!$this->prepared || $add) {
            $type = $this->hash[0];

            if ($type) {
                $method = strtolower($type) . 'Prepare';
                if (method_exists($this, $method)) {
                    $this->$method(isset($this->hash[1]) ? $this->hash[1] : '');
                } else {

                }
            } else {

            }

            if ($this->prepared) {
                return;
            }
            $this->prepared = true;
        }
    }

    protected function categoryPrepare($id)
    {

    }

    /**
     * Returns expression for SQL
     *
     * @param string $op    - operand ==, >=, etc
     * @param string $value - value
     *
     * @return string
     */
    protected function getExpression($op, $value)
    {
        $model = $this->getModel();
        switch ($op) {
            case '>':
            case '>=':
            case '<':
            case '<=':
            case '!=':
                return " " . $op . " '" . $model->escape($value) . "'";
            case "^=":
                return " LIKE '" . $model->escape($value, 'like') . "%'";
            case "$=":
                return " LIKE '%" . $model->escape($value, 'like') . "'";
            case "*=":
                return " LIKE '%" . $model->escape($value, 'like') . "%'";
            case "==":
            case "=":
            default:
                return " = '" . $model->escape($value) . "'";
        }
    }

    /**
     * Returns GROUP BY clause.
     *
     * @return string
     */
    protected function _getGroupBy()
    {
        if ($this->group_by) {
            return " GROUP BY " . $this->group_by;
        } else {
            return "";
        }
    }

    public function orderBy($field, $order = 'ASC')
    {
        if (is_array($field)) {
            if (count($field) > 1) {
                list($field, $order) = $field;
            } else {
                $field = $field[0];
            }
        }
        if (strtolower(trim($order)) == 'desc') {
            $order = 'DESC';
        } else {
            $order = 'ASC';
        }
        $field = trim($field);
        if ($field) {

            return $this->order_by = 'p.' . $field . " " . $order;

        }
    }

    /**
     * Returns ORDER BY clause.
     *
     * @return string
     */
    protected function _getOrderBy()
    {
        if ($this->order_by) {
            return " ORDER BY " . $this->order_by;
        } else {
            return "";
        }
    }

    /**
     * Returns product selection SQL query
     *
     * @return string
     */
    public function getSQL()
    {
        $this->prepare();
        $sql = "FROM manufactory_product p";

        if ($this->joins) {
            foreach ($this->joins as $join) {
                $alias = isset($join['alias']) ? $join['alias'] : '';
                if (isset($join['on'])) {
                    $on = $join['on'];
                } else {
                    $on = "p.id = " . ($alias ? $alias : $join['table']) . ".product_id";
                }
                $sql .= (isset($join['type']) ? " " . $join['type'] : '') . " JOIN " . $join['table'] . " " . $alias . " ON " . $on;
            }
        }
        $where = $this->where;

        if ($where) {
            $sql .= " WHERE " . implode(" AND ", $where);
        }

        return $sql;
    }

    /**
     * Returns number of products included in collection.
     *
     * @return int
     */
    public function count()
    {
        if ($this->count !== null) {
            return $this->count;
        }
        $sql = $this->getSQL();

        if ($this->having) {
            $sql .= $this->_getGroupBy();
            $sql .= " HAVING " . implode(' AND ', $this->having);
            $sql = "SELECT COUNT(*) FROM (SELECT p.* " . $sql . ") AS t";
        } else {
            $sql = "SELECT COUNT(" . ($this->joins ? 'DISTINCT ' : '') . "p.id) " . $sql;
        }
        $count = (int)$this->getModel()->query($sql)->fetchField();


        return $this->count = $count;
    }

    /**
     * Returns array of products included in collection.
     *
     * @param string   $fields List of product properties, comma-separated, to be included in returned array
     * @param int      $offset Initial position in returned product array, 0 means first product in collection
     * @param int|bool $limit  Maximum product limit.
     *                         If a Boolean value is specified, then $escape = $limit and $limit = null
     *                         If no value is specified, then $limit = 0.
     *                         If no value is specified and $offset is non-zero, then $limit = $offset and $offset = 50
     * @param bool     $escape Whether product names and urls must be escaped using htmlspecialchars() function, defaults to true
     *
     * @return array Array of collection products' sub-arrays
     */
    public function getProducts($fields = "*", $offset = 0, $limit = null, $escape = true)
    {
        if (is_bool($limit)) {
            $escape = $limit;
            $limit = null;
        }
        if ($limit === null) {
            if ($offset) {
                $limit = $offset;
                $offset = 0;
            } else {
                $limit = 50;
            }
        }
        if ($this->is_frontend && $fields == '*') {
            $fields .= ',frontend_url';
        }
        $split_fields = array_map('trim', explode(',', $fields));
        if (in_array('frontend_url', $split_fields) && !in_array('*', $split_fields)) {
            if ($dependent_fields = array_diff(array('url', 'category_id',), $split_fields)) {
                $fields .= ',' . implode(',', $dependent_fields);
            }
        }

        $sql = $this->getSQL();

        // for dynamic set
        if ($this->hash[0] == 'set' && !empty($this->info['id']) && $this->info['type'] == shopSetModel::TYPE_DYNAMIC) {
            $this->count();
            if ($offset + $limit > $this->count) {
                $limit = $this->count - $offset;
            }
        }

        $sql = "SELECT " . ($this->joins && !$this->group_by ? 'DISTINCT ' : '') . $this->getFields($fields) . " " . $sql;
        $sql .= $this->_getGroupBy();
        if ($this->having) {
            $sql .= " HAVING " . implode(' AND ', $this->having);
        }
        $sql .= $this->_getOrderBy();
        $sql .= " LIMIT " . ($offset ? $offset . ',' : '') . (int)$limit;

        $data = $this->getModel()->query($sql)->fetchAll('id');
        if (!$data) {
            return array();
        }

        return $data;
    }
}


class shopProductsCollection
{
    protected $hash;
    protected $info = array();


    protected $prepared = false;


    protected $fields = array();
    protected $where;
    protected $having = array();
    protected $count;
    protected $order_by = 'p.id DESC';
    protected $group_by;
    protected $joins;
    protected $join_index = array();


    protected $models = array();

    /**
     * Creates a new product collection.
     *
     * @param string|array $hash    Product selection conditions. Examples:
     *                              array(12,23,34) or 'id/12,23,34' — explicitly specified product ids
     *                              'related/cross_selling/12' — cross-selling items for product with id = 12
     *                              'related/upselling/23' — upselling items for product with id = 23
     *                              'category/208' — search by category id
     *                              'search/query=style' — search results by query 'style'
     *                              'search/tag=style' — search results by tag 'style'
     *                              'tag/style' — alternative form of search by tag
     *                              'search/type_id=1' — search results by any field of shop_product table; e.g., type_id
     *                              'type/2' — search by type_id
     *                              'search/name=SuperProduct' — search by 'name' field (exact match)
     *                              'search/color.value_id=6' — search by value with id=6 of 'checkboxes'-type feature with code 'color'
     * @param array        $options Extra options:
     *                              'filters'    => whether products must be filtered according to GET request conditions
     *                              'product'    => shopProduct object to select upselling items for; for upselling-type collections only
     *                              'conditions' => upselling items selection conditions; for upselling-type collections only
     *                              'params'     => whether extra product params must be included in collection products
     *                              'absolute'   => whether absolute product image URLs must be returned for collection products
     */
    public function __construct($hash = '')
    {
        $this->setHash($hash);
    }


    protected function setHash($hash)
    {
        if (is_array($hash)) {
            $hash = '/id/' . implode(',', $hash);
        }
        if (substr($hash, 0, 1) == '#') {
            $hash = substr($hash, 1);
        }
        $this->hash = trim($hash, '/');
        if ($this->hash == 'all') {
            $this->hash = '';
        }
        $this->hash = explode('/', $this->hash, 2);
    }

    protected function prepare($add = false)
    {
        if (!$this->prepared || $add) {
            $type = $this->hash[0];

            if ($type) {
                $method = strtolower($type) . 'Prepare';
                if (method_exists($this, $method)) {
                    $this->$method(isset($this->hash[1]) ? $this->hash[1] : '');
                } else {

                }
            } else {

            }

            if ($this->prepared) {
                return;
            }
            $this->prepared = true;
        }
    }


    protected function toFloat($value)
    {
        if (strpos($value, ',') !== false) {
            $value = str_replace(',', '.', $value);
        }

        return str_replace(',', '.', (double)$value);
    }


    protected function categoryPrepare($id)
    {

        $category_model = $this->getModel('category');
        $category = $category_model->getById($id);

        $this->info = $category;

        // category not found
        if (!$this->info) {
            $this->where[] = '0';

            return;
        }

        $this->info['hash'] = 'category';
        if ($this->is_frontend) {
            $this->info['frontend_url'] = wa()->getRouteUrl('shop/frontend/category', array(
                'category_url' => waRequest::param('url_type') == 1 ? $category['url'] : $category['full_url'],
            ), true);
        } else {
            $frontend_urls = $category_model->getFrontendUrls($id);
            if ($frontend_urls) {
                $this->info['frontend_url'] = $frontend_urls[0];
                $this->info['frontend_urls'] = $frontend_urls;
            }
        }

        if ($auto_title) {
            $this->addTitle($this->info['name']);
        }

        if (!waRequest::get('sort')) {
            if (!empty($this->info['sort_products'])) {
                $tmp = explode(' ', $this->info['sort_products']);
                if (!isset($tmp[1])) {
                    $tmp[1] = 'DESC';
                }
                if ($tmp[0] == 'count') {
                    $this->fields[] = 'IF(p.count IS NULL, 1, 0) count_null';
                    $this->order_by = 'count_null ' . $tmp[1] . ', p.count ' . $tmp[1];
                } elseif ($tmp[0] == 'stock_worth') {
                    $this->fields[] = 'IFNULL(p.count, 0)*p.price AS stock_worth';
                    $this->order_by = 'stock_worth ' . $tmp[1];
                } else {
                    $this->order_by = 'p.' . $this->info['sort_products'];
                }
            }
        }


        if ($this->info['type'] == shopCategoryModel::TYPE_STATIC) {
            $alias = $this->addJoin('shop_category_products');
            if (true
                /* && wa()->getEnv() == 'frontend'*/
                && $this->info['include_sub_categories']
            ) {
                $this->info['subcategories'] = $category_model->descendants($this->info, true)->where('type = ' . shopCategoryModel::TYPE_STATIC)->fetchAll('id');
                $descendant_ids = array_keys($this->info['subcategories']);
                if ($descendant_ids) {
                    $this->where[] = $alias . ".category_id IN(" . implode(',', $descendant_ids) . ")";
                }
            } else {
                $this->where[] = $alias . ".category_id = " . (int)$id;
            }
            if ((empty($this->info['sort_products']) && !waRequest::get('sort')) || waRequest::get('sort') == 'sort') {
                $this->order_by = $alias . '.sort ASC';
            }
        } else {
            $hash = $this->hash;
            $this->setHash('/search/' . $this->info['conditions']);
            $this->prepare(false, false);
            $info = $this->info;
            while ($info['parent_id'] /* && $this->info['conditions']*/) {
                $info = $category_model->getById($info['parent_id']);
                if ($info['type'] == shopCategoryModel::TYPE_DYNAMIC) {
                    $this->setHash('/search/' . $info['conditions']);
                    $this->prepare(true, false);
                } else {
                    $alias = $this->addJoin('shop_category_products');
                    if ($info['include_sub_categories']) {
                        $subcategories = $category_model->descendants($info, true)->where('type = ' . shopCategoryModel::TYPE_STATIC)->fetchAll('id');
                        $descendant_ids = array_keys($subcategories);
                        if ($descendant_ids) {
                            $this->where[] = $alias . ".category_id IN(" . implode(',', $descendant_ids) . ")";
                        }
                    } else {
                        $this->where[] = $alias . ".category_id = " . (int)$info['id'];
                    }

                    break;
                }
            }
            $this->setHash(implode('/', $hash));
        }
    }


    /**
     * Returns expression for SQL
     *
     * @param string $op    - operand ==, >=, etc
     * @param string $value - value
     *
     * @return string
     */
    protected function getExpression($op, $value)
    {
        $model = $this->getModel();
        switch ($op) {
            case '>':
            case '>=':
            case '<':
            case '<=':
            case '!=':
                return " " . $op . " '" . $model->escape($value) . "'";
            case "^=":
                return " LIKE '" . $model->escape($value, 'like') . "%'";
            case "$=":
                return " LIKE '%" . $model->escape($value, 'like') . "'";
            case "*=":
                return " LIKE '%" . $model->escape($value, 'like') . "%'";
            case "==":
            case "=":
            default:
                return " = '" . $model->escape($value) . "'";
        }
    }


    /**
     * Returns GROUP BY clause.
     *
     * @return string
     */
    protected function _getGroupBy()
    {
        if ($this->group_by) {
            return " GROUP BY " . $this->group_by;
        } else {
            return "";
        }
    }

    /**
     * Changes ORDER BY clause of product selection query.
     *
     * @param string|array $field Name of field in 'shop_product' table. Alternative value options:
     *                            'sort' (ignored)
     *                            'rand()' or 'RAND()' — sets 'RAND()' condition for ORDER BY clause
     *                            array($field, $order) — in this case $order parameter is ignored
     * @param string       $order 'ASC' or 'DESC' modifier
     *
     * @return string New ORDER BY clause or empty string (for 'rand()' $field value)
     */
    public function orderBy($field, $order = 'ASC')
    {
        if (is_array($field)) {
            if (count($field) > 1) {
                list($field, $order) = $field;
            } else {
                $field = $field[0];
            }
        }
        if (strtolower(trim($order)) == 'desc') {
            $order = 'DESC';
        } else {
            $order = 'ASC';
        }
        $field = trim($field);
        if ($field) {

            return $this->order_by = 'p.' . $field . " " . $order;

        }
    }

    /**
     * Returns ORDER BY clause.
     *
     * @return string
     */
    protected function _getOrderBy()
    {
        if ($this->order_by) {
            return " ORDER BY " . $this->order_by;
        } else {
            return "";
        }
    }

    /**
     * Returns product selection SQL query
     *
     * @return string
     */
    public function getSQL()
    {
        $this->prepare();
        $sql = "FROM manufactory_product p";

        if ($this->joins) {
            foreach ($this->joins as $join) {
                $alias = isset($join['alias']) ? $join['alias'] : '';
                if (isset($join['on'])) {
                    $on = $join['on'];
                } else {
                    $on = "p.id = " . ($alias ? $alias : $join['table']) . ".product_id";
                }
                $sql .= (isset($join['type']) ? " " . $join['type'] : '') . " JOIN " . $join['table'] . " " . $alias . " ON " . $on;
            }
        }
        $where = $this->where;

        if ($where) {
            $sql .= " WHERE " . implode(" AND ", $where);
        }

        return $sql;
    }

    /**
     * Returns number of products included in collection.
     *
     * @return int
     */
    public function count()
    {
        if ($this->count !== null) {
            return $this->count;
        }
        $sql = $this->getSQL();

        if ($this->having) {
            $sql .= $this->_getGroupBy();
            $sql .= " HAVING " . implode(' AND ', $this->having);
            $sql = "SELECT COUNT(*) FROM (SELECT p.* " . $sql . ") AS t";
        } else {
            $sql = "SELECT COUNT(" . ($this->joins ? 'DISTINCT ' : '') . "p.id) " . $sql;
        }
        $count = (int)$this->getModel()->query($sql)->fetchField();


        return $this->count = $count;
    }

    /**
     * Returns array of products included in collection.
     *
     * @param string   $fields List of product properties, comma-separated, to be included in returned array
     * @param int      $offset Initial position in returned product array, 0 means first product in collection
     * @param int|bool $limit  Maximum product limit.
     *                         If a Boolean value is specified, then $escape = $limit and $limit = null
     *                         If no value is specified, then $limit = 0.
     *                         If no value is specified and $offset is non-zero, then $limit = $offset and $offset = 50
     * @param bool     $escape Whether product names and urls must be escaped using htmlspecialchars() function, defaults to true
     *
     * @return array Array of collection products' sub-arrays
     */
    public function getProducts($fields = "*", $offset = 0, $limit = null, $escape = true)
    {
        if (is_bool($limit)) {
            $escape = $limit;
            $limit = null;
        }
        if ($limit === null) {
            if ($offset) {
                $limit = $offset;
                $offset = 0;
            } else {
                $limit = 50;
            }
        }
        if ($this->is_frontend && $fields == '*') {
            $fields .= ',frontend_url';
        }
        $split_fields = array_map('trim', explode(',', $fields));
        if (in_array('frontend_url', $split_fields) && !in_array('*', $split_fields)) {
            if ($dependent_fields = array_diff(array('url', 'category_id',), $split_fields)) {
                $fields .= ',' . implode(',', $dependent_fields);
            }
        }

        $sql = $this->getSQL();

        // for dynamic set
        if ($this->hash[0] == 'set' && !empty($this->info['id']) && $this->info['type'] == shopSetModel::TYPE_DYNAMIC) {
            $this->count();
            if ($offset + $limit > $this->count) {
                $limit = $this->count - $offset;
            }
        }

        $sql = "SELECT " . ($this->joins && !$this->group_by ? 'DISTINCT ' : '') . $this->getFields($fields) . " " . $sql;
        $sql .= $this->_getGroupBy();
        if ($this->having) {
            $sql .= " HAVING " . implode(' AND ', $this->having);
        }
        $sql .= $this->_getOrderBy();
        $sql .= " LIMIT " . ($offset ? $offset . ',' : '') . (int)$limit;

        $data = $this->getModel()->query($sql)->fetchAll('id');
        if (!$data) {
            return array();
        }

        return $data;
    }

    /**
     * @param string $name
     *
     * @return shopProductModel
     */
    protected function getModel($name = 'Product')
    {
        $models_pool = manufactoryModelsPool::getInstance();
        if (!isset($this->models[$name])) {
            $this->models[$name] = $models_pool->get(ucfirst($name));
        }

        return $this->models[$name];
    }


    /**
     * Returns various useful information about collection.
     *
     * @return array
     */
    public function getInfo()
    {
        if (empty($this->info)) {
            $this->prepare();
        }
        if (!isset($this->info['hash'])) {
            $this->info['hash'] = $this->hash[0];
        }

        return $this->info;
    }


    /**
     * Adds a GROUP BY clause to product selection query.
     *
     * @param string $clause
     *
     * @return self
     */
    public function groupBy($clause)
    {
        $this->group_by = $clause;

        return $this;
    }

    /**
     * Adds a WHERE condition to product selection query.
     *
     * @param string $condition Additional WHERE condition; WHERE keyword must not be specified
     *
     * @return self
     */
    public function addWhere($condition)
    {
        $this->where[] = $condition;

        return $this;
    }

    /**
     * Adds a simple JOIN clause to product selection query.
     *
     * @param string|array $table Table name to be used in JOIN clause.
     *                            Alternatively an associative array may be specified containing values for all method parameters.
     *                            In this case $on and $where parameters are ignored.
     * @param string       $on    ON condition FOR JOIN, must not include 'ON' keyword
     * @param string       $where WHERE condition for SELECT, must not include 'WHERE' keyword
     *
     * @return string Specified table's alias to be used in SQL query
     */
    public function addJoin($table, $on = null, $where = null)
    {
        if (is_array($table)) {
            if (isset($table['on'])) {
                $on = $table['on'];
            }
            if (isset($table['where'])) {
                $where = $table['where'];
            }
            $table = $table['table'];
        }
        $t = explode('_', $table);
        $alias = '';
        foreach ($t as $tp) {
            if ($tp == 'shop') {
                continue;
            }
            $alias .= substr($tp, 0, 1);
        }

        if (!$alias) {
            $alias = $table;
        }

        if (!isset($this->join_index[$alias])) {
            $this->join_index[$alias] = 1;
        } else {
            $this->join_index[$alias]++;
        }
        $alias .= $this->join_index[$alias];

        $join = array(
            'table' => $table,
            'alias' => $alias,
        );
        if ($on) {
            $join['on'] = str_replace(':table', $alias, $on);
        }
        $this->joins[] = $join;
        if ($where) {
            $this->where[] = str_replace(':table', $alias, $where);
        }

        return $alias;
    }

    public function getHash()
    {
        return $this->hash;
    }


}
