<?php

class manufactoryPricesPool extends manufactoryObjectPool
{
    protected static $model_name = 'MarginProduct';
    protected static $obj_class_name = 'manufactoryPrice';

    protected static $storage = array();
    protected static $model = null;

    public static function get($id = null)
    {
        if (!empty($id)) {
            if (isset(static::$storage[$id])) {
                return static::$storage[$id];
            } else {
                $models_pool = manufactoryModelsPool::getInstance();
                $model = $models_pool->get(static::$model_name);
                $data = $model->getById($id);
                if (is_object($data)) {
                    static::$storage[$id] = $data;

                    return static::$storage[$id];
                }
                if (!empty($data) && !empty(static::$obj_class_name) && class_exists(static::$obj_class_name)) {
                    $className = static::$obj_class_name;
                    if ($data['type'] == 'value') {
                        static::$storage[$id] = new manufactoryPriceValue($data);
                    } elseif ($data['type'] == 'delimiter') {
                        static::$storage[$id] = new manufactoryPriceDelimiter($data);
                    } else {
                        static::$storage[$id] = new $className($data);
                    }

                    return static::$storage[$id];
                }

                return null;
            }
        } else {
            return static::$storage;
        }
    }

    public static function geByType($type = 'generate')
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get(static::$model_name);

        $sql = "SELECT * FROM " . $model->getTableName() . " WHERE type LIKE '" . $model->escape($type, 'like') . "%'";

        $data = $model->query($sql)->fetchAll('id');
        $return = array();
        if (is_array($data) && !empty($data)) {
            if (class_exists(static::$obj_class_name)) {
                $className = static::$obj_class_name;
                foreach ($data as $k => $v) {
                    if (!isset(static::$storage[$k])) {
                        if ($v['type'] == 'value') {
                            static::$storage[$k] = new manufactoryPriceValue($v);
                        } elseif ($v['type'] == 'delimiter') {
                            static::$storage[$k] = new manufactoryPriceDelimiter($v);
                        } else {
                            static::$storage[$k] = new $className($v);
                        }

                    }
                    $return[$k] = static::$storage[$k];
                }
            }
        }

        return $return;
    }

    public static function getAll($type_value = true)
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get(static::$model_name);
        $data = $model->getAll('id');
        $prices = array();
        if (is_array($data) && !empty($data)) {
            if (class_exists(static::$obj_class_name)) {
                $className = static::$obj_class_name;
                foreach ($data as $k => $v) {
                    if ($type_value || $v['type'] != 'value') {
                        if (!isset(static::$storage[$k])) {
                            if ($v['type'] == 'value') {
                                static::$storage[$k] = new manufactoryPriceValue($v);
                            } elseif ($v['type'] == 'delimiter') {
                                static::$storage[$k] = new manufactoryPriceDelimiter($v);
                            } else {
                                static::$storage[$k] = new $className($v);
                            }
                        }
                        $prices[$k] = static::$storage[$k];
                    }
                }
            }
        }

        return $prices;

    }

}