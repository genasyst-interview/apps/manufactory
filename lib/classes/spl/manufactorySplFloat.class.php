<?php

class manufactorySplFloat extends SplFloat
{
    const __default = 0.0;

    public function __construct($val, $strict = true)
    {
        if (is_string($val)) {
            $val = str_replace(',', '.', $val);
        }
        if (!is_numeric($val)) {
            $val = self::__default;
        } else {
            $val = floatval($val);
        }
        parent::__construct($val, $strict);
    }
}