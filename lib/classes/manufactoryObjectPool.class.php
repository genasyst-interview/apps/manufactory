<?php

class manufactoryObjectPool
{
    /* 
     protected static $storage = array();
     protected static $model = null;
     protected static $obj_class_name = null;
     protected static $model_name = '';
    */
    public static function getInstance()
    {
        if (static::$_instance == null) {
            static::$_instance = new static();
        }

        return static::$_instance;
    }

    public static function get($id = null)
    {
        if (!empty($id)) {
            if (isset(static::$storage[$id])) {
                return static::$storage[$id];
            } else {
                $models_pool = manufactoryModelsPool::getInstance();
                $model = $models_pool->get(static::$model_name);
                $data = $model->getById($id);
                if (is_object($data)) {
                    static::$storage[$id] = $data;

                    return static::$storage[$id];
                }
                if (!empty($data) && !empty(static::$obj_class_name) && class_exists(static::$obj_class_name)) {
                    $className = static::$obj_class_name;
                    static::$storage[$id] = new $className($data);

                    return static::$storage[$id];
                }

                return null;
            }
        } else {
            return static::$storage;
        }
    }

    public static function getAll()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get(static::$model_name);
        $data = $model->getAll('id');
        if (is_array($data) && !empty($data)) {
            if (class_exists(static::$obj_class_name)) {
                $className = static::$obj_class_name;
                foreach ($data as $k => $v) {
                    if (!isset(static::$storage[$k])) {
                        static::$storage[$k] = new $className($v);
                    }
                }
            }
        }

        return static::$storage;
    }

}