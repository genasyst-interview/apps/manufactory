<?php

class manufactoryFeature extends manufactoryEntity implements ArrayAccess, manufactoryFeatureCustomInterface
{
    protected $data = array();
    /*
     * значения характеристики

     * */
    protected $values = array();

    public function __construct($data)
    {
        if (is_array($data)) {
            $this->setData($data);
        } elseif (is_numeric($data) && intval($data) > 0) {
            $this->setData($this->getModel('Feature')->getById($data));
        }
        if (!empty($this->data)) {
            $this->init();
        }
    }

    protected function init()
    {
        $this->values = $this->getModel('FeatureValue')->getByFeature($this->getId());
    }

    public function getGroupsValues()
    {
        return manufactoryFeaturesPool::getInstance()->getFeatureGroups($this->getId());
    }

    public function getValues($group_id = null)
    {
        if ($group_id != null) {
            // TODO: надо будет возможно сразу сгенерировать значения по группам
            $values = array();
            foreach ($this->values as $v) {
                if ($v['group_id'] == $group_id) {
                    $values[$v['id']] = $v;
                }
            }

            return $values;
        }

        return $this->values;
    }

    /*
     * @return array()
     * */
    public function getUnits()
    {
        $units = manufactoryFeaturesPool::getInstance()->getTypeUnits($this->getType());

        return $units;
    }

    public function getBaseUnit()
    {
        return $this->data['unit'];
    }

    public function getValueData($id)
    {
        if (array_key_exists($id, $this->values)) {
            return $this->values[$id];
        }

        return false;
    }

    public function getValuesByGroup($group_id = null)
    {
        return $this->getValues($group_id);
    }

    public function getType()
    {
        return $this->data['type'];
    }

    public function getTypeName()
    {
        return manufactoryFeaturesPool::getInstance()->getTypeName($this->data['type']);
    }

    public function getProductTypes()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $product_type_model = $models_pool->get('ProductType');
        $model = $models_pool->get('ProductTypesFeatures');
        $types_ids = $model->getByField('feature_id', $this->getId());
        $types = array();
        foreach ($types_ids as $v) {
            $types [$v['type_id']] = $product_type_model->getById($v['type_id']);
        }

        return $types;
    }

    public function offsetExists($offset)
    {
        if (isset($this->data[$offset])) {
            return true;
        }

        return false;
    }

    public function offsetGet($offset)
    {
        if (isset($this->data[$offset])) {
            return $this->data[$offset];
        }

        return null;
    }

    public function offsetSet($offset, $value)
    {

        $this->data[$offset] = $value;

    }

    public function offsetUnset($offset)
    {
        $this->data[$offset] = null;
    }

    public function delete()
    {

    }

    public function save($data)
    {

    }
}