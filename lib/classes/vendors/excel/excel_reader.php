<?
/** Error reporting
 *
 * error_reporting(E_ALL);
 * ini_set('display_errors', TRUE);
 * ini_set('display_startup_errors', TRUE);
 * date_default_timezone_set('Europe/London');
 *
 *
 *
 * Создаем функцию подсчета колонок по размерам и записываем в переменную $ max_column
 *
 * Затем пишуем функцию  цен, в которой передаем список цен массивом,
 * цены с массивом закидывать в следуюшую функцию:
 *
 * Необходимо написать отдельную функцию подсчета при условии цен в качестве массива,
 * туда передавать цену размера и цены ( отдельная функция после подсчета по размерам, независимая)
 *
 * прогоняем переданные категории  в функцию циклом с передачей всех параметров (колонки, цены, название)
 *
 * Обрабатываем в функции  товары категории с параметрами и ценами, передаем товар и все параметры функции товара
 *
 * Функция товара принимает (Колонки, название, размеры, цены)
 * создаем функцию формирования товара, если размеры, то одна функция, если без размеров, другая...
 */
session_start();
include($_SERVER['DOCUMENT_ROOT'] . "/admin/functions/config.php");
include($_SERVER['DOCUMENT_ROOT'] . "/admin/class/auth_class.php");
$user = new Auth;
if (!$user->check_valid_user()) {
    Header("Location: /admin/?message=notauthorized&referrer=" . urlencode($_SERVER['REQUEST_URI']));
    exit();
}
require_once($_SERVER['DOCUMENT_ROOT'] . '/admin/modules/product-pricing/Excel.class.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/admin/modules/excel/PHPExcel.php');
$Prices = new Excel();

if (empty($_POST)) {
    require_once($_SERVER['DOCUMENT_ROOT'] . SMART_TOOLS_PATH . '/tpl/admin_header.php');
    include($_SERVER['DOCUMENT_ROOT'] . "/admin/modules/product-pricing/menu_top.php");

    $objPHPExcel = new PHPExcel();
    $objPHPExcel = PHPExcel_IOFactory::load($_SERVER['DOCUMENT_ROOT'] . "/admin/modules/excel/price1.xlsx");
    /*$lists = $objPHPExcel->getSheetCount();
    for($i=0;$i<$lists;$i++)
    {
        $objPHPExcel->setActiveSheetIndex($i);
        $return[] = $objPHPExcel->getActiveSheet()->toArray();
    }
    var_dump($return);*/

    $list = $objPHPExcel->getActiveSheet()->toArray();
    $rows_name = array(
        0  => 'A', 1 => 'B', 2 => 'C', 3 => 'D', 4 => 'E',
        5  => 'F', 6 => 'G', 7 => 'H', 8 => 'I', 9 => 'J',
        10 => 'K', 11 => 'L', 12 => 'M',
    );
    //var_dump($list);
    echo '<form action="' . $_SERVER['PHP_SELF'] . '" method="post">
	<table cellpadding="0" cellspacing="0" border="0" rules="all" >';
    foreach ($list as $row => $column) {
        echo '<tr>';
        foreach ($column as $kc => $vc) {
            echo '<td>';
            echo '<input type="text" name="data[' . $row . '][' . $kc . ']" value="' . $vc . '">';
            echo '</td>';
        }
        echo '</tr>';
    }
    echo '<input type="submit" value="ok"></form></table>';

    require_once($_SERVER['DOCUMENT_ROOT'] . SMART_TOOLS_PATH . '/tpl/admin_footer.php');
} else {
    echo "<pre>";
    var_dump($_POST);
    echo "<pre>";
}


?>