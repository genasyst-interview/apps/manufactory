<?php

/**
 * Class manufactoryEntity
 * Объект какой - то сущности с данными,
 * по сути объект из бд имеющий массиы данных и возможностью изменения и удаления
 */
abstract class manufactoryEntity extends manufactoryClass
{

    /**
     * @var array
     */
    protected $data = array();

    /**
     * @param $data
     *
     * @return void
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function getData($name = null)
    {
        if (is_null($name)) {
            return $this->data;
        }
        if (array_key_exists($name, $this->data)) {
            return $this->data[(string)$name];
        }

        return null;
    }

    public function getName()
    {
        return $this->data['name'];
    }

    public function getId()
    {
        if (isset($this->data['id'])) {
            return $this->data['id'];
        }

        return false;
    }

    abstract public function delete();

    abstract public function save($data);
}