<?php

/**
 * Class manufactoryEntityProduct
 */
abstract class manufactoryEntityProduct extends manufactoryEntity implements manufactoryEntityProductInterface
{
    protected $change_costs = false;
    protected $storage = array();
    protected $storage_objects = array();
    const ENTITY_TYPE = 'product';

    public function __construct($data)
    {
        $this->init($data);
    }

    public function setChangeCosts($flag)
    {
        $this->change_costs = (bool)$flag;
    }

    public function isChangeCosts()
    {
        return $this->change_costs;
    }

    public function getEntityType()
    {
        return static::ENTITY_TYPE;
    }

    abstract public function init($data);

    public function getData($name = null)
    {
        if (array_key_exists($name, $this->storage)) {
            return $this->storage[$name];
        } elseif (array_key_exists($name, $this->storage_objects)) {
            $class_name = $this->storage_objects[$name];
            $this->storage[$name] = new $class_name($this);

            return $this->storage[$name];
        } elseif (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        } else {
            return null;
        }
    }

    public function setData($data)
    {
        if (is_array($data) && !empty($data)) {
            foreach ($data as $name => $value) {
                if (array_key_exists($name, $this->storage)) {
                    $storage = $this->storage[$name];
                    $storage->setData($value);
                } elseif (array_key_exists($name, $this->storage_objects)) {
                    $class_name = $this->storage_objects[$name];
                    $this->storage[$name] = new $class_name($this);
                    $storage = $this->storage[$name];
                    $storage->setData($value);
                } else {
                    $this->data[$name] = $value;
                }
            }
        }
    }

    public function getCosts()
    {
        return $this->getData('costs');
    }

    public function getCost()
    {
        $price = 0.0;
        foreach ($this->getCosts() as $cost_type) {
            $price += $cost_type->getPrice();
        }

        return $price;
    }

    abstract public function getFeatures();
}