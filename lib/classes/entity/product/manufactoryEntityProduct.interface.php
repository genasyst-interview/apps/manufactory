<?php

/**
 * Interface manufactoryEntityProductInterface
 */
interface manufactoryEntityProductInterface
{
    public function getEntityType();

    public function getCosts();

    public function getCost();

    public function save($data);
}