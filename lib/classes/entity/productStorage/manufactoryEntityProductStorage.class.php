<?php

abstract class manufactoryEntityProductStorage extends manufactoryClass implements manufactoryEntityProductStorageInterface
{
    protected $entity = null; // manufactoryEntityProduct
    protected $model = ''; // manufactoryEntityProductStorageModel
    protected $data = array();
    protected $change_data = false;

    public function __construct(manufactoryEntityProduct $entity)
    {
        $this->setEntity($entity);
        $this->init();

    }

    protected function setChangeData($flag)
    {
        $this->change_data = (bool)$flag;
    }

    public function isChangeData()
    {
        return $this->change_data;
    }

    abstract public function init();

    /**
     * @return manufactoryEntityProduct
     */
    public function getEntity()
    {
        return $this->entity;
    }

    public function getEntityId()
    {
        return $this->getEntity()->getId();
    }

    public function setEntity(manufactoryEntityProduct $entity)
    {
        $this->entity = $entity;
    }

    public function getData($key = null)
    {
        if ($key == null) {
            return $this->data;
        }
        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }

        return null;
    }

    public function setData($data = array(), $key = null)
    {
        if ($key == null) {
            return $this->data = $data;
        } else {
            $this->data[$key] = $data;
        }
    }

    /* public function getModel()
     {
         return parent::getModel($this->model);
     }*/
    protected function getModel($name = null)
    {
        if (empty($name)) {
            $name = $this->model;
        }

        return parent::getModel($name);
    }

    public function save()
    {
        $change = $this->getModel()->setByEntity($this->getEntityId(), $this->getData());
        if ($change) {
            $this->setChangeData($change);
        }
    }

    public function delete()
    {
        $change = $this->getModel()->deleteByEntity($this->getEntityId());
        if ($change) {
            $this->setChangeData($change);
        }
    }

    public function getIterator()
    {
        return new ArrayIterator($this->getData());
    }

    public function offsetExists($offset)
    {
        if (array_key_exists($offset, $this->data)) {
            return true;
        }

        return false;
    }

    public function offsetGet($offset)
    {
        if (array_key_exists($offset, $this->data)) {
            return $this->getData($offset);
        }

        return null;
    }

    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) {
            unset($this->data[$offset]);
        }
    }

    public function count()
    {
        return count($this->data);
    }

    function __call($name, $arguments)
    {
        waLog::log('Вызван несуществующий метод ' . $name . '. Доступные методы: ' . var_export(get_class_methods($this), true));
    }
}