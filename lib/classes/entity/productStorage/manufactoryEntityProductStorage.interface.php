<?php

/**
 * Интерфейс для хранения данных каких-то продуктовых сущностей
 * (товары, товарные предложения, полуфабрикаты, возможно сборные материалы)
 * Interface manufactoryEntityProductStorageInterface
 */
interface manufactoryEntityProductStorageInterface extends ArrayAccess, IteratorAggregate, Countable
{
    public function __construct(manufactoryEntityProduct $entity);

    public function getEntity();

    public function setEntity(manufactoryEntityProduct $entity);

    public function init();

    public function getData($key = null);

    public function save();

    //public function getModel($name);

}