<?php

/**
 * Интерфейс для моделей хранения данных каких-то продуктовых сущностей
 * (товары, товарные предложения, полуфабрикаты, возможно сборные материалы)
 * Interface manufactoryEntityProductStorageModel
 */
interface manufactoryEntityProductStorageModelInterface
{
    /**
     * Получение данных для товарной сущности
     * Отдает массив расходов для сущности (продукт, предложение продукта)
     *
     * @param $id
     * @param $storage_id_column
     *
     * @return array (cost_id => data,.....)
     *
     */
    public function getByEntity($id, $storage_id_column = null);

    /**
     * Запись данных для товарной сущности
     *
     * @param integer $id   Идентификатор товарной сущности
     * @param array   $data Массив данных в формате cost_id => array(data), cost_id => data,...
     *
     * @return boolean
     */
    public function setByEntity($id, $data = array());

    /**
     * Удаление данных из товарной сущности
     *
     * @param $id Идентификатор товарной сущности
     *
     * @return mixed
     */
    public function deleteByEntity($id);


}