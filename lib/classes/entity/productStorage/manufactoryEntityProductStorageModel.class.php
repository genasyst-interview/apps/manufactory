<?php

/**
 * Абстрактная модель для хранения каких-то данных  для продукта или товарного предложения
 * Class manufactoryEntityProductStorageModel
 */
abstract class manufactoryEntityProductStorageModel extends manufactorySortableModel implements manufactoryEntityProductStorageModelInterface
{
    /**
     * Название колонки сущности (entity_id, product_id, offer_id)
     * @var string
     */
    protected $entity_id_column = 'entity_id';
    protected $storage_id_column = 'id';

    public function getByEntity($id, $storage_id_column = null)
    {
        if ($storage_id_column == null) {
            $storage_id_column = $this->storage_id_column;
        }

        return $this->getByField($this->entity_id_column, $id, $storage_id_column);
    }

    public function deleteByEntity($id)
    {
        return $this->deleteByField(array(
            $this->entity_id_column => $id,
        ));
    }
}