<?php

/**
 * Interface manufactoryEntityCostsInterface
 */
interface  manufactoryEntityCostsInterface extends IteratorAggregate, ArrayAccess, Countable
{

    /**
     * manufactoryIProductCosts constructor.
     *
     * @param $entity
     */
    /* function __construct(manufactoryEntityProduct $entity);*/
    function __construct(manufactoryEntityProduct $entity);

    /**
     * @param $entity
     *
     * @return mixed
    protected function  setEntity($entity);
     */
    /**
     * @param $entity
     *
     * @return mixed
    protected function  getEntity();
     */

    /* Возвращает массив расходов */
    /**
     * @return mixed
     */
    public function getData($key = null);
    /* Общая сумма расходов из всех типов в зависимости от конкретного тов. предложения */
    /**
     * @param $entity
     *
     * @return mixed
     */
    function getPrice();

    public function getEntityType();

}