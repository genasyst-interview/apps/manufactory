<?php

/**
 * Абстрактная модель для хранения данных расходов для продукта или товарного предложения
 * Class manufactoryCostEntityModel
 */
abstract class manufactoryEntityCostsModel extends manufactoryEntityProductStorageModel implements manufactoryEntityCostsModelInterface
{
    /**
     * Название колонки сущности (entity_id, product_id, offer_id)
     * @var string
     */
    protected $entity_id_column = 'entity_id';
    protected $storage_id_column = 'cost_id';

    public function setByEntity($id, $data = array())
    {
        $delete_data = $this->getByEntity($id);
        $change_data = false;

        foreach ($data as $sid => $quantity) {
            if (array_key_exists($sid, $delete_data)) {
                if ($delete_data[$sid]['quantity'] != $quantity) {
                    $this->updateByField(array(
                        $this->entity_id_column  => $id,
                        $this->storage_id_column => $sid,
                    ), array('quantity' => $quantity));
                    $change_data = true;
                }
                unset($delete_data[$sid]);
            } else {
                $change_data = true;
                $_data = array(
                    $this->entity_id_column  => $id,
                    $this->storage_id_column => $sid,
                    'quantity'               => $quantity,
                );
                $this->insert($_data);
            }
        }
        foreach ($delete_data as $delete_row) {
            $this->deleteByField(
                $delete_row
            );
        }

        return $change_data;
    }

    public function getByCost($id, $all = null)
    {
        if ($all == null) {
            $all = $this->entity_id_column;
        }

        return $this->getByField(array(
            $this->storage_id_column => $id,
        ), $all);
    }

    public function deleteByCost($id)
    {
        return $this->deleteByField($this->storage_id_column, $id);
    }
}