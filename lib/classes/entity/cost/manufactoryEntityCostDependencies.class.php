<?php

/**
 * Class manufactoryEntity
 * Объект какой - то сущности с данными,
 * по сути объект из бд имеющий массиы данных и возможностью изменения и удаления
 */
abstract class manufactoryEntityCostDependencies extends manufactoryClass
{
    protected $id = 0;
    protected $entity_cost = null;

    public function __construct($entity_cost)
    {
        $this->setEntityCost($entity_cost);
    }

    public function setEntityCost($entity_cost)
    {
        $this->entity_cost = $entity_cost;
        $this->id = $entity_cost->getId();
    }

    public function getId()
    {
        return $this->id;
    }

    abstract public function getProducts();

    abstract public function getDependencies();

    abstract public function deleteDependencies();

    abstract public function recalculateDependencies();
}