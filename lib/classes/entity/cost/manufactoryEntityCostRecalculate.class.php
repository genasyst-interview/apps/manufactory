<?php

/**
 * Class manufactoryEntity
 * Объект какой - то сущности с данными,
 * по сути объект из бд имеющий массиы данных и возможностью изменения и удаления
 */
abstract class manufactoryEntityCostRecalculate extends manufactoryClass
{
    protected $dependencies = array();
    protected $entity = null;
    protected $products = null;

    public function __construct($entity)
    {
        $this->setEntity($entity);
        $this->setDependencies();
        wa('manufactory')->event('cost_recalculate', $this);
    }

    abstract public function setDependencies();

    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function getEntityId()
    {
        return $this->entity->getId();
    }

    public function getEntityType()
    {
        return $this->getEntity()->getEntityType();
    }

    public function setDependency($dependency)
    {
        $this->dependencies[] = $dependency;
    }

    public function getProducts()
    {
        if ($this->products == null) {
            $this->products = array();
            foreach ($this->dependencies as $dependency) {
                $this->products = $this->products + $dependency->getProducts();
            }
        }

        return $this->products;
    }

    public function recalculateDependencies()
    {
        foreach ($this->dependencies as $dependency) {
            $dependency->recalculateDependencies();
        }
    }

    public function recalculateProducts()
    {
        foreach ($this->getProducts() as $id => $v) {
            $product = $this->getFactory('Products')->get($id);
            $product->recalculatePrices();
        }
    }

    public function run()
    {
        $this->getProducts();
        $this->recalculateDependencies();
        $this->recalculateProducts();
    }
}