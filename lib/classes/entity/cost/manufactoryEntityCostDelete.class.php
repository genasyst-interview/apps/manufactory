<?php

/**
 * Class manufactoryEntity
 * Объект какой - то сущности с данными,
 * по сути объект из бд имеющий массиы данных и возможностью изменения и удаления
 */
abstract class manufactoryEntityCostDelete extends manufactoryClass
{
    protected $dependencies = array();
    protected $entity = null;
    protected $products = null;

    public function __construct(manufactoryCost $entity)
    {
        $this->setEntity($entity);
        $this->setDependencies();
        wa('manufactory')->event('cost_delete', $this);

    }

    abstract public function setDependencies();

    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function getEntityId()
    {
        return $this->entity->getId();
    }

    public function getEntityType()
    {
        return $this->getEntity()->getEntityType();
    }

    public function setDependency($dependency)
    {
        $this->dependencies[] = $dependency;
    }

    public function deleteDependencies()
    {
        foreach ($this->dependencies as $dependency) {
            $dependency->deleteDependencies();
        }
    }

    public function recalculateDependencies()
    {
        foreach ($this->dependencies as $dependency) {
            $dependency->recalculateDependencies();
        }
    }

    public function getProducts()
    {
        if ($this->products == null) {
            $this->products = array();
            foreach ($this->dependencies as $dependency) {
                $this->products = $this->products + $dependency->getProducts();
            }
        }

        //waLog::dump($this->products);
        return $this->products;
    }

    public function recalculateProducts()
    {
        foreach ($this->getProducts() as $id => $v) {
            $product = manufactoryProducts::getById($id);
            $product->recalculatePrices();
        }
    }

    public function deleteEntity()
    {
        $this->getEntityModel()->deleteById($this->getEntityId());
    }

    abstract public function getEntityModel();

    public function run()
    {

        $this->getProducts();
        $this->deleteDependencies();
        $this->recalculateDependencies();
        $this->recalculateProducts();

        $this->deleteEntity();
    }
}