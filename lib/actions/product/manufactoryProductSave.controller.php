<?php

class manufactoryProductSaveController extends waJsonController
{

    public function execute()
    {
        $id = waRequest::post('id', 0, waRequest::TYPE_INT);
        if (!empty($id)) {
            $product = wa('manufactory')->getConfig()->getFactory('Products')->get($id);
        } else {
            $product = new manufactoryProduct(null);
        }

        $data = waRequest::post();
        $category = new manufactoryCategoryProducts($data['category_id']);

        if ($category->getRights() < 2) {
            $this->errors[] = 'Нет прав на редактирование в категории  "' . $category->getName() . '" c id[' . $category->getId() . ']!  Обратитесь к администратору приложения!';
        } else {
            $product->save($data);
            $this->response['id'] = $product->getId();
            $this->response['category_id'] = $product->getData('category_id');
        }
    }
}