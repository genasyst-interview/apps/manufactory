<?php

class manufactoryProductSkusRenameAction extends waViewAction
{

    // TODO: Удалить и сделать более оптимизированное переименование
    public function execute()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('Product');
        $products = $model->getAll('id');
        foreach ($products as $product) {
            $product = wa('manufactory')->getConfig()->getFactory('Products')->get($product['id']);
            $offers = $product->getSkus();
            foreach ($offers as $offer) {
                $offer->rename();
            }
        }

    }
}