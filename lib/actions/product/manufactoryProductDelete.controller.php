<?php

class manufactoryProductDeleteController extends waJsonController
{


    public function execute()
    {
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);
        if (!empty($id)) {
            $product = manufactoryProducts::getById($id);

            $product->delete();
        }
    }
}