<?php

class manufactoryProductSkuDeleteController extends waJsonController
{


    public function execute()
    {
        $offer_id = waRequest::post('offer_id');
        if (!empty($offer_id)) {
            $models_pool = manufactoryModelsPool::getInstance();
            $model = $models_pool->get('ProductSku');
            $offer_data = $model->getById($offer_id);
            if (is_array($offer_data) && !empty($offer_data)) {
                $offer = new manufactoryProductSku(null, $offer_data);
                $offer->delete();
            }
        }
    }
}