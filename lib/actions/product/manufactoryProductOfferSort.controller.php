<?php

class manufactoryProductOfferSortController extends waJsonController
{


    public function execute()
    {
        $id = waRequest::post('id');
        $after_id = waRequest::post('after_id');
        if (!empty($id)) {
            $model = manufactoryModelsPool::getInstance()->get('ProductOffer');
            $model->move($id, $after_id);
        }
    }
}