<?php

class manufactoryProductTypeSaveController extends waController
{


    public function execute()
    {
        $id = waRequest::post('id', 0, waRequest::TYPE_INT);
        if (!empty($id)) {
            $models_pool = manufactoryModelsPool::getInstance();
            $product_type_model = $models_pool->get('ProductType');
            $product_type = $product_type_model->getById($id);

            $data = waRequest::post();

            $product_type->save($data);
            // $this->redirect('?module=product&action=types&&id='.$id);
        } else {

        }

    }
}