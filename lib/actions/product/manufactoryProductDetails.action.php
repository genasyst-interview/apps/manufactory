<?php

class manufactoryProductDetailsAction extends waViewAction
{


    public function execute()
    {
        $id = waRequest::get('id');
        if (!empty($id)) {
            $Pricing = new manufactoryPricing();
            $Pricing->setPrices();

            $prices = $Pricing->getPrices();
            $def_price = wa()->getUser()->getRights('manufactory', 'contact_price');
            if (array_key_exists($def_price, $prices)) {
                $default_price = $prices[$def_price];
                $this->view->assign('prices', $default_price);
            } else {
                foreach ($prices as $price) {
                    if ($price->getType() != 'delimiter') {
                        $default_price = $price;
                    }
                }
            }
            $this->view->assign('default_price', $default_price);
            $product = wa('manufactory')->getConfig()->getFactory('Products')->get($id);
            $this->view->assign('prices', $prices);
            $this->view->assign('product', $product);
        }

    }
}