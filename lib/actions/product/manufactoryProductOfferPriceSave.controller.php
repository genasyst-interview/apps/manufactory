<?php

class manufactoryProductOfferPriceSaveController extends waJsonController
{


    public function execute()
    {
        $offer_id = waRequest::post('offer_id');
        $price_id = waRequest::post('price_id');
        $price = waRequest::post('price');
        if (isset($_POST['offer_id']) && isset($_POST['price_id'])) {

            $price = manufactoryPricesPool::get($price_id)->calculate(floatval($price));
            /* $price = $offer->getPrice($_POST['price_id']);
            var_dump($price);
             exit;
             $price->savePrice($_POST['price']);
             echo json_encode($price->calculate());*/
            $models_pool = manufactoryModelsPool::getInstance();
            $model = $models_pool->get('ProductOfferPrice');
            $offer_price = $model->getByField(array(
                'offer_id' => intval($offer_id),
                'price_id' => intval($price_id),
            ));
            if (!empty($offer_price)) {
                $model->updateById($offer_price['id'], array('price' => $price));
                $offer_price['price'] = $price;
            } else {
                $id = $model->insert(array(
                    'price'    => $price,
                    'offer_id' => intval($offer_id),
                    'price_id' => intval($price_id),
                ));
                $offer_price = $model->getById($id);
            }

            if (!empty($offer_price)) {
                $this->response = $offer_price;
            } else {
                $this->errors[] = 'ет предложения!';
            }

        }
    }
}