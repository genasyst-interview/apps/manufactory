<?php

class manufactoryProductEditAction extends waViewAction
{


    public function execute()
    {
        $this->setLayout(new manufactoryProductsLayout());

        $id = waRequest::get('id');
        if (!empty($id)) {
            $product = wa('manufactory')->getConfig()->getFactory('Products')->get($id);
            $category = new manufactoryCategoryProducts($product->getData('category_id'));
            if ($category->getRights() < 2) {
                throw new waException('Нет прав на редактирование!');
            }
            $this->view->assign('product', $product);
            $this->getResponse()->setTitle($product->getName());
        } else {
            $this->getResponse()->setTitle('Новый продукт');
        }
        $this->view->assign('backend_product_edit', wa('manufactory')->event('backend_product_edit', $product,
            array('costs', 'costs_menu', 'features', 'costs_type')
        ));
        $Pricing = new manufactoryPricing();
        $Pricing->setPrices();
        $Pricing->setCategories();
        $categories = $Pricing->getCategories();
        $this->view->assign('categories', $categories);
        $category = waRequest::get('category', 0, waRequest::TYPE_INT);
        $this->view->assign('category', $category);
        $features = manufactoryFeaturesPool::getInstance()->getAll();
        $this->view->assign('features', $features);
        $prices = $Pricing->getPrices();
        $def_price = wa()->getUser()->getRights('manufactory', 'contact_price');
        if (array_key_exists($def_price, $prices)) {
            $default_price = $prices[$def_price];
            $this->view->assign('prices', $default_price);
        } else {
            foreach ($prices as $price) {
                if ($price->getType() != 'delimiter') {
                    $default_price = $price;
                }
            }
        }
        $this->view->assign('default_price', $default_price);

        $this->view->assign('Pricing', $Pricing);
    }
}