<?php

class manufactoryProductOffersRenameAction extends waViewAction
{


    public function execute()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('Product');
        $products = $model->getAll('id');
        foreach ($products as $product) {
            $product = manufactoryProducts::getById($product['id']);
            $offers = $product->getOffers();
            foreach ($offers as $offer) {
                $offer->rename();
            }
        }

    }
}