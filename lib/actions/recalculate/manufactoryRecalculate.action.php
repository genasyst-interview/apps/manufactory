<?php

class manufactoryRecalculateAction extends waViewAction
{

    public function execute()
    {
        $this->setLayout(new manufactoryDefaultLayout());
        if (waRequest::get('materials') == 1) {
            $this->materials();
        } elseif (waRequest::get('staging') == 1) {
            $this->staging();
        }


    }

    public function materials()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('Material');
        $materials = $model->getAll('id');
        foreach ($materials as $v) {
            $material = new manufactoryMaterial($v['id']);
            $material->regenerateSkusFeatures();
        }
    }

    public function staging()
    {
        /*$models_pool = manufactoryModelsPool::getInstance();
        $Product = $models_pool->get('Product');
        $ProductMaterials = $models_pool->get('ProductMaterials');
        $products = $Product->getAll();
        foreach ($products as $product_data) {
            $materials = $ProductMaterials->getByProduct($product_data['id'], 'material_id');
            $__materials = $ProductMaterials->stagingModel()->getByProduct($product_data['id'],'material_id');
            
            foreach ($__materials as $mat_id=>$v) {
                if(!array_key_exists($mat_id, $materials)) {
                    $ProductMaterials->insert($__materials[$mat_id]);
                }
            }
        }*/
    }

}
