<?php

class manufactoryCostsSidebarAction extends waViewAction
{
    public function execute()
    {
        $params = null;
        $backend_sidebar = wa('manufactory')->event('backend_costs', $params, array('sidebar'));
        $this->view->assign('backend_costs',
            $backend_sidebar
        );
    }
}