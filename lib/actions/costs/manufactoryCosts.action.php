<?php

class manufactoryCostsAction extends waViewAction
{
    public function execute()
    {
        $this->setLayout(new manufactoryCostsLayout());
        $id = waRequest::get('category');
        if (!empty($id)) {
            $category = new manufactoryCategoryProducts($id);
            $prices = new manufactoryMarginsProduct();
            $this->view->assign('prices', $prices);
            $this->view->assign('category', $category);
            $this->view->assign('products', $category->getProducts());
        }
    }
}