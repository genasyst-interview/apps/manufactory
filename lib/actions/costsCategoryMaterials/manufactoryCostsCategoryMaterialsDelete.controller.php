<?php

class manufactoryCostsCategoryMaterialsDeleteController extends waJsonController
{


    public function execute()
    {
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);

        $models_pool = manufactoryModelsPool::getInstance();
        $CategoryMaterialsModel = $models_pool->get('CategoryMaterials');
        $product_materials = $models_pool->get('ProductMaterials');
        $material_model = $models_pool->get('Material');

        $materials = manufactoryHelper::getMaterials($id);
        if (!empty($materials)) {
            $materials_ids = array();
            foreach ($materials as $v) {
                $materials_ids[] = $v->getId();
            }
            $products = $product_materials->getByField('cost_id', $materials_ids, 'product_id');
            if (!empty($products)) {
                foreach ($products as $pv) {
                    $product = manufactoryProducts::getById($pv['product_id']);
                    $materials = $product->getMaterials();
                    foreach ($materials_ids as $mid) {
                        unset($materials[$mid]);
                    }
                    foreach ($materials as $mid => $mobj) {
                        $materials[$mid] = $mobj->getQuantity();
                    }
                    $product->saveMaterials($materials);
                    $product->setOffers();
                    $product->updateProductDatetime();
                    ///  а также пересчитываем все нехранимые цены, хранимые обновим в самой цене
                    $product->recalculatePrices();
                }
            }
            // Удаляем сами материалы
            foreach ($materials_ids as $mid) {
                $material_model->deleteById($mid);
            }
        }
        // Удаляем саму категорию
        $CategoryMaterialsModel->deleteById($id);

    }
}