<?php

class manufactoryCostsCategoryMaterialsAction extends waViewAction
{

    public function execute()
    {
        if (waRequest::method() == 'post') {
            $this->sort();
        }
        $this->setLayout(new manufactoryCostsLayout());

        $Pricing = new manufactoryPricing();
        $this->view->assign('Pricing', $Pricing);
    }

    public function sort()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CategoryMaterials');

        $sort = waRequest::post('sort');
        foreach ($sort as $k => $v) {
            $model->updateById($k, array('sort' => $v));
        }
    }
}
