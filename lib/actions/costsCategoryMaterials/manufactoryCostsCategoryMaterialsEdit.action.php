<?php

class manufactoryCostsCategoryMaterialsEditAction extends waViewAction
{

    public function execute()
    {
        $this->setLayout(new manufactoryCostsLayout());
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);
        $category = false;

        $Pricing = new manufactoryPricing();
        $this->view->assign('Pricing', $Pricing);
        if (!empty($id)) {
            $models_pool = manufactoryModelsPool::getInstance();
            $model = $models_pool->get('CategoryMaterials');
            $category = $model->getById($id);
        }
        $this->view->assign('category', $category);
    }

}
