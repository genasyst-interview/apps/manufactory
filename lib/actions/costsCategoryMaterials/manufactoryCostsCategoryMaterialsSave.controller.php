<?php

class manufactoryCostsCategoryMaterialsSaveController extends waJsonController
{


    public function execute()
    {
        $id = waRequest::post('id', 0, waRequest::TYPE_INT);

        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CategoryMaterials');
        $data = waRequest::post();
        $data['name'] = trim($data['name']);
        if (!empty($id)) {
            $model->updateById($id, $data);
            $this->response['id'] = $id;
        } else {
            unset($data['id']);
            $this->response['id'] = $model->insert($data);
        }
    }
}