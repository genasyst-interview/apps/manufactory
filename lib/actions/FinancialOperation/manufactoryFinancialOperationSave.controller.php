<?php

class manufactoryFinancialOperationSaveController extends waJsonController
{
    public function execute()
    {
        $data = waRequest::post();

        $FOClass = new manufactoryFinancialOperations();
        if (!is_numeric($data['amount'])) {
            $this->errors[] = 'Введена некорректная сумма!';

            return;
        } else {
            $data['amount'] = round($data['amount'], 2);
        }
        $id = $FOClass->saveOperation($data, $this->errors);
        if ($id) {
            $FOModel = $FOClass->getFinancialOperationModel();
            $operation = $FOModel->getById($id);
            $operation['create_date'] = date('d.m.Y', strtotime($operation['create_datetime']));
            $operation['operation_date'] = date('d.m.Y', strtotime($operation['operation_datetime']));
            /// BALANCES
            $balances = $FOClass->getBalances($operation['payment_type']);
            /// Category BALANCES
            $FOCModel = $FOClass->getFinancialOperationsCategoryModel($operation['type'], $operation['payment_type']);
            $FOCBModel = $FOClass->getFinancialOperationsCategoryBalanceModel($operation['type'], $operation['payment_type']);
            $category_id = $operation['category_id'];
            $categories_balances = array();
            while (true) {
                $category = $FOCModel->getById($category_id);
                $categories_balances[$category_id] = $FOCBModel->getCategoryBalance($category_id);
                if ($category['parent_category_id'] > 0) {
                    $category_id = $category['parent_category_id'];
                } else {
                    break;
                }
            }
            $balances['categories'] = $categories_balances;
            $this->response['operation'] = $operation;
            $this->response['balances'] = $balances;
            /// CONTACT DATA
            $contacts = array();
            $FOModel->setPaymentType($operation['payment_type']);
            foreach ($balances['contacts'] as $id => $v) {
                $contacts[$id]['amount'] = $v['amount'];
                $contacts[$id]['operations'] = $FOModel->getLastContactOperations($id);
            }
            $this->response['contacts'] = $contacts;
        } else {
            $this->errors[] = 'Ошибка! Не удалось сохранить операцию!';

            return;
        }
    }

    protected function getCategoriesBalances($id)
    {
        $FOCModel = new manufactoryFinancialOperationsCategoryModel();
        $category = $FOCModel->getById($id);
        $categories = array();
        $stop = false;
        $FOBModel = $FOClass->getFinancialOperationsCategoryBalanceModel($category['financial_operations_type'], $category['financial_operations_payment_type']);
        while ($stop) {
            $category = $FOCModel->getById($id);
            if ($category['parent_category_id'] > 0) {

            } else {

            }
        }

    }

}
