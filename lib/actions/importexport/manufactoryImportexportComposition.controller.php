<?php

class manufactoryImportexportCompositionController extends waViewController
{

    public function execute()
    {
        $categories = waRequest::get('categories');
        if (!empty($categories)) {
            $this->displayPrice();
        } else {
            $this->executeAction(new manufactoryImportexportCompositionAction());
        }

    }

    public function displayPrice()
    {
        require_once($this->getConfig()->getAppPath('lib/classes/vendors/excel/') . 'PHPExcel.php');
        $Pricing = new manufactoryPricing();
        $prices = waRequest::get('prices');
        $categories = waRequest::get('categories');
        if (is_array($prices)) {
            $Pricing->setPrices($prices);
        }
        if (is_array($categories)) {
            $Pricing->setCategories($categories);
        }

        $prices = $Pricing->getPrices();

        $categories = $Pricing->getCategories();
        foreach ($categories as $k => &$v) {
            $v['products'] = $Pricing->getCategoryProducts($k);
        }
        unset($v);


// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $rows_name = array(
            0  => 'A', 1 => 'B', 2 => 'C', 3 => 'D', 4 => 'E',
            5  => 'F', 6 => 'G', 7 => 'H', 8 => 'I', 9 => 'J',
            10 => 'K', 11 => 'L', 12 => 'M', 13 => 'N', 14 => 'O',
            15 => 'P', 16 => 'Q', 17 => 'R', 18 => 'S', 19 => 'T',
            20 => 'U', 21 => 'V', 22 => 'W', 23 => 'X', 24 => 'Y',
            25 => 'Z',
        );
        $description_price = '';

        foreach ($categories as $v) {
            $description_price .= $v['name'];
            $lists[] = $v['id'];
        }
// Настромка описания документа
        $objPHPExcel->getProperties()->setCreator($_SERVER['SERVER_NAME'])
            ->setLastModifiedBy("MaZaHaKa")
            ->setTitle("Прайс Avalonian.ru")
            ->setSubject("Прайс Avalonian.ru")
            ->setDescription($description_price)
            ->setKeywords($description_price)
            ->setCategory("Prices");


// Генерируем контент
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);
        $split_lists = waRequest::get('lists');
        $type = waRequest::get('type');
        $line = 1;
        foreach ($lists as $kl => $vl) {
            $category = $categories[$vl];
            $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);
            if (empty($split_lists)) {
                if ($kl == 0) {
                    // Устанавливаем первый лист
                    $objPHPExcel->setActiveSheetIndex(0);
                    $objPHPExcel->getActiveSheet()->setTitle('Весь прайс');
                }
            } else {
                if ($kl == 0) {
                    // Устанавливаем первый лист
                    $objPHPExcel->setActiveSheetIndex($kl);
                } else {
                    $objPHPExcel->createSheet();
                    $objPHPExcel->setActiveSheetIndex($kl);
                }
                $line = 1;

                $objPHPExcel->getActiveSheet()->setTitle($category['name']);

            }

            $products = $Pricing->getCategoryProducts($vl);
            if (!empty($products)) {
                foreach ($products as $product) {
                    $offers = $product->getSkus();
                    foreach ($offers as $offer) {
                        // Строка имени продукта на всю ширину
                        $name = $offer->getProduct()->getName() . ' ' . $offer->getName();
                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, $name);
                        $objPHPExcel->getActiveSheet()->getStyle($rows_name[0] . $line)->getAlignment()->setHorizontal(
                            PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $objPHPExcel->getActiveSheet()->getStyle($rows_name[0] . $line)->getFont()->setBold(true);
                        $objPHPExcel->getActiveSheet()->getStyle($rows_name[0] . $line)->getFont()->setSize(20);
                        $objPHPExcel->getActiveSheet()->mergeCells($rows_name[0] . $line . ':' . $rows_name[5] . '' . $line . '');
                        $line++;

                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, 'Название материала');
                        $objPHPExcel->getActiveSheet()->getStyle($rows_name[0] . $line)->getFont()->setBold(true);

                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, 'Стоимость материала');
                        $objPHPExcel->getActiveSheet()->getStyle($rows_name[1] . $line)->getFont()->setBold(true);

                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[2] . $line, 'Коэффициент материала');
                        $objPHPExcel->getActiveSheet()->getStyle($rows_name[2] . $line)->getFont()->setBold(true);

                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[3] . $line, 'Количество материала');
                        $objPHPExcel->getActiveSheet()->getStyle($rows_name[3] . $line)->getFont()->setBold(true);

                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[4] . $line, 'Расчет материала');
                        $objPHPExcel->getActiveSheet()->getStyle($rows_name[4] . $line)->getFont()->setBold(true);

                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[5] . $line, 'Итоговая стоимость материала');
                        $objPHPExcel->getActiveSheet()->getStyle($rows_name[5] . $line)->getFont()->setBold(true);
                        $line++;
                        foreach ($offer->getCosts() as $ctype => $costs) {
                            foreach ($costs as $cid => $cost) {
                                if (is_callable(array($cost->getProductCost(), 'getCosts')) && $type == 1) {

                                    $subcosts = $cost->getSkuCosts();
                                    foreach ($subcosts as $skcost => $scost) {
                                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, $scost->getName());
                                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, $scost->getCost());
                                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[2] . $line, $scost->getCoefficient());
                                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[3] . $line, $scost->getQuantity());
                                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[4] . $line, $scost->getCalculateString());
                                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[5] . $line, round($scost->getPrice(), 2));
                                        $line++;
                                    }
                                } else {
                                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, $cost->getName());
                                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, $cost->getCost());
                                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[2] . $line, $cost->getCoefficient());
                                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[3] . $line, $cost->getQuantity());
                                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[4] . $line, $cost->getCalculateString());
                                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[5] . $line, round($cost->getPrice(), 2));
                                    $line++;
                                }

                            }
                        }

                        if (!empty($_REQUEST['prices'])) {
                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, 'Название цены');
                            $objPHPExcel->getActiveSheet()->getStyle($rows_name[0] . $line)->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->mergeCells($rows_name[0] . $line . ':' . $rows_name[1] . $line);

                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[2] . $line, 'Расчет');
                            $objPHPExcel->getActiveSheet()->getStyle($rows_name[2] . $line)->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->mergeCells($rows_name[2] . $line . ':' . $rows_name[3] . $line);

                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[4] . $line, 'Итог');
                            $objPHPExcel->getActiveSheet()->getStyle($rows_name[4] . $line)->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->mergeCells($rows_name[4] . $line . ':' . $rows_name[5] . $line);
                            $line++;

                            foreach ($prices as $ck => $cv) {
                                $price = $offer->getPrice($ck);
                                $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, $price->getName());
                                $objPHPExcel->getActiveSheet()->mergeCells($rows_name[0] . $line . ':' . $rows_name[1] . $line);
                                $objPHPExcel->getActiveSheet()->setCellValue($rows_name[2] . $line, $price->getCalculateName());
                                $objPHPExcel->getActiveSheet()->mergeCells($rows_name[2] . $line . ':' . $rows_name[3] . $line);
                                $objPHPExcel->getActiveSheet()->setCellValue($rows_name[4] . $line, $price->calculate());
                                $objPHPExcel->getActiveSheet()->mergeCells($rows_name[4] . $line . ':' . $rows_name[5] . $line);
                                $line++;
                            }

                        }
                        // Ставим пустую строку
                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, '');
                        $line++;
                    }

                }
            }


            foreach ($rows_name as $rv) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($rv)->setAutoSize(true);
            }
        }

//$filename = $_GET['name'].'-'.time().'.xlsx';
        $filename = $_GET['name'] . '.xlsx';
        header("Content-Description: File Transfer");
        header("Pragma: public");
        header("Expires: 0");
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=" . $filename . "");
        header('Cache-Control: max-age=0');
// Сохраняем в формате Excel 2007
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
// Сохраняем на сервер
        $objWriter->save($filename);
// Отдаем в браузер
        $objWriter->save('php://output');
    }
}
