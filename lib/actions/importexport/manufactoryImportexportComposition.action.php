<?php

class manufactoryImportexportCompositionAction extends waViewAction
{

    public function execute()
    {
        $this->setLayout(new manufactoryImportexportLayout());
        $categories = waRequest::post('categories');
        $Pricing = new manufactoryPricing();
        if (!empty($categories)) {
            $Pricing->setCategories($categories);
            $Pricing->setPrices();


            $pricingGroups = $Pricing->getPricingGroups();
            $categories = $Pricing->getCategories();
            foreach ($categories as $k => &$v) {
                $v['products'] = $Pricing->getCategoryProducts($k);
            }
            unset($v);
            $prices = new manufactoryMarginsProduct();
            $this->view->assign('prices', $prices);
            $this->view->assign('pricing_groups', $pricingGroups);
            $this->view->assign('categories', $categories);
        } else {

        }
        $prices = new manufactoryMarginsProduct();
        $this->view->assign('prices', $prices);
        $this->view->assign('Pricing', $Pricing);
    }
}
