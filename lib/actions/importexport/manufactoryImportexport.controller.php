<?php

class manufactoryImportexportController extends waViewController
{

    public function execute()
    {
        $this->setLayout(new manufactoryImportexportLayout());
        $this->executeAction(new manufactoryImportexportPriceAction());
    }
}
