<?php

class manufactoryImportexportOnecController extends waViewController
{

    public function execute()
    {
        $categories = waRequest::get('categories');
        if (!empty($categories)) {
            $this->displayPrice();
        } else {
            $this->executeAction(new manufactoryImportexportOnecAction());
        }


    }

    public function displayPrice()
    {
        require_once($this->getConfig()->getAppPath('lib/classes/vendors/excel/') . 'PHPExcel.php');
        $Pricing = new manufactoryPricing();
        $prices = waRequest::get('prices');
        $categories = waRequest::get('categories');
        if (is_array($prices)) {
            $Pricing->setPrices($prices);
        }
        if (is_array($categories)) {
            $Pricing->setCategories($categories);
        }

        $prices = $Pricing->getPrices();

        $categories = $Pricing->getCategories();
        $sizes = waRequest::get('sizes');
        foreach ($categories as $k => &$v) {
            $v['products'] = $Pricing->getCategoryProducts($k);
        }
        unset($v);

        $type = waRequest::get('type');
// Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $rows_name = array(
            0  => 'A', 1 => 'B', 2 => 'C', 3 => 'D', 4 => 'E',
            5  => 'F', 6 => 'G', 7 => 'H', 8 => 'I', 9 => 'J',
            10 => 'K', 11 => 'L', 12 => 'M', 13 => 'N', 14 => 'O',
            15 => 'P', 16 => 'Q', 17 => 'R', 18 => 'S', 19 => 'T',
            20 => 'U', 21 => 'V', 22 => 'W', 23 => 'X', 24 => 'Y',
            25 => 'Z',
        );
        $description_price = '';

        foreach ($categories as $v) {
            $description_price .= $v['name'];
            $lists[] = $v['id'];
        }
// Настромка описания документа
        $objPHPExcel->getProperties()->setCreator($_SERVER['SERVER_NAME'])
            ->setLastModifiedBy("MaZaHaKa")
            ->setTitle("Прайс Avalonian.ru")
            ->setSubject("Прайс Avalonian.ru")
            ->setDescription($description_price)
            ->setKeywords($description_price)
            ->setCategory("Prices");


// Генерируем контент
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);

        if ($type == 0) {
            $line = 1;
            foreach ($lists as $kl => $vl) {
                $category = $categories[$vl];
                $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);
                if ($kl == 0) {
                    // Устанавливаем первый лист
                    $objPHPExcel->setActiveSheetIndex(0);
                    $objPHPExcel->getActiveSheet()->setTitle('Весь прайс');
                }
                $products = $Pricing->getCategoryProducts($vl);
                if (!empty($products)) {
                    foreach ($products as $product) {
                        $offers = $product->getSkus();
                        foreach ($offers as $offer) {
                            if (!isset($sizes[$offer->getSkuData()])) {
                                continue;
                            }

                            // Строка имени продукта на всю ширину
                            $name = $offer->getProduct()->getName() . ' ' . $offer->getName();
                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, $name);
                            $objPHPExcel->getActiveSheet()->getStyle($rows_name[0] . $line)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                            $line++;

                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, 'Номенклатура');
                            $objPHPExcel->getActiveSheet()->getStyle($rows_name[0] . $line)->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, 'Количество');
                            $objPHPExcel->getActiveSheet()->getStyle($rows_name[1] . $line)->getFont()->setBold(true);
                            $line++;
                            foreach ($offer->getCosts() as $ctype => $costs) {
                                foreach ($costs as $cid => $cost) {
                                    if (is_callable(array($cost->getProductCost(), 'getCosts'))) {

                                        $subcosts = $cost->getSkuCosts();
                                        foreach ($subcosts as $skcost => $scost) {
                                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, $scost->getName());
                                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, round($scost->getCoefficient() * $scost->getQuantity(), 3));
                                            $line++;
                                        }
                                    } else {
                                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, $cost->getName());
                                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[2] . $line, round($cost->getCoefficient() * $cost->getQuantity(), 3));

                                        $line++;
                                    }

                                }
                            }
                            if (!empty($_REQUEST['prices'])) {
                                $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, 'Название цены');
                                $objPHPExcel->getActiveSheet()->getStyle($rows_name[0] . $line)->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, 'Итог');
                                $objPHPExcel->getActiveSheet()->getStyle($rows_name[1] . $line)->getFont()->setBold(true);
                                $line++;

                                foreach ($prices as $ck => $cv) {
                                    $price = $offer->getPrice($ck);
                                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, $price->getName());
                                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, $price->calculate());
                                    $line++;
                                }
                            }
                            // Ставим пустую строку
                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, '');
                            $line++;
                        }

                    }
                }


                foreach ($rows_name as $rv) {
                    $objPHPExcel->getActiveSheet()->getColumnDimension($rv)->setAutoSize(true);
                }
            }
        } elseif ($type == 1) {
            $line = 1;

            foreach ($lists as $kl => $vl) {
                $category = $categories[$vl];
                $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);
                if ($kl == 0) {
                    // Устанавливаем первый лист
                    $objPHPExcel->setActiveSheetIndex(0);
                    $objPHPExcel->getActiveSheet()->setTitle('Весь прайс');
                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, 'Название');
                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, 'Размер');
                    $objPHPExcel->getActiveSheet()->getStyle($rows_name[0] . $line)->getFont()->setBold(true);
                    $objPHPExcel->getActiveSheet()->getStyle($rows_name[1] . $line)->getFont()->setBold(true);
                    $line_index = 2;
                    foreach ($prices as $ck => $cv) {
                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[$line_index] . $line, $cv->getName());
                        $objPHPExcel->getActiveSheet()->getStyle($rows_name[$line_index] . $line)->getFont()->setBold(true);
                        $line_index++;
                    }
                    $line++;
                }
                $products = $Pricing->getCategoryProducts($vl);
                if (!empty($products)) {

                    foreach ($products as $product) {
                        $offers = $product->getSkus();
                        foreach ($offers as $offer) {
                            /* if(!isset($_GET['sizes'][$offer->getSizeId()])) {
                                 continue;
                             }*/
                            if (!isset($sizes[$offer->getSkuData()])) {
                                continue;
                            }

                            // Строка имени продукта на всю ширину
                            /* $size = SizesPool::get($offer->getSizeId());*/
                            $name = $offer->getProduct()->getName();
                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, $name);
                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, $offer->getName());
                            $line_index = 2;

                            if (!empty($_REQUEST['prices'])) {
                                foreach ($prices as $ck => $cv) {
                                    $price = $offer->getPrice($ck);
                                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[$line_index] . $line, $price->calculate());
                                    $line_index++;
                                }
                            }
                            $line++;
                            // Ставим пустую строку
                            //$objPHPExcel->getActiveSheet()->setCellValue($rows_name[0].$line,'');
                            //$line++;
                        }

                    }
                }


                foreach ($rows_name as $rv) {
                    $objPHPExcel->getActiveSheet()->getColumnDimension($rv)->setAutoSize(true);
                }
            }
        }

//$filename = $_GET['name'].'-'.time().'.xlsx';
        $filename = $_GET['name'] . '.xlsx';
        header("Content-Description: File Transfer");
        header("Pragma: public");
        header("Expires: 0");
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=" . $filename . "");
        header('Cache-Control: max-age=0');
// Сохраняем в формате Excel 2007
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
// Сохраняем на сервер
        $objWriter->save($filename);
// Отдаем в браузер
        $objWriter->save('php://output');

    }
}
