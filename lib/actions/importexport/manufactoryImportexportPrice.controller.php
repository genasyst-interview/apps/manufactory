<?php

class manufactoryImportexportPriceController extends waViewController
{

    public function execute()
    {
        $categories = waRequest::get('categories');
        if (!empty($categories)) {
            $this->displayPrice();
        } else {
            $this->executeAction(new manufactoryImportexportPriceAction());
        }


    }

    public function displayPrice()
    {
        require_once($this->getConfig()->getAppPath('lib/classes/vendors/excel/') . 'PHPExcel.php');
        $Pricing = new manufactoryPricing();
        $prices = waRequest::get('prices');
        $categories = waRequest::get('categories');
        if (is_array($prices)) {
            $Pricing->setPrices($prices);
        }
        if (is_array($categories)) {
            $Pricing->setCategories($categories);
        }

        $prices = $Pricing->getPrices();

        $categories = $Pricing->getCategories();
        foreach ($categories as $k => &$v) {
            $v['products'] = $Pricing->getCategoryProducts($k);
        }
        unset($v);

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();
        $rows_name = array(
            0  => 'A', 1 => 'B', 2 => 'C', 3 => 'D', 4 => 'E',
            5  => 'F', 6 => 'G', 7 => 'H', 8 => 'I', 9 => 'J',
            10 => 'K', 11 => 'L', 12 => 'M', 13 => 'N', 14 => 'O',
            15 => 'P', 16 => 'Q', 17 => 'R', 18 => 'S', 19 => 'T',
            20 => 'U', 21 => 'V', 22 => 'W', 23 => 'X', 24 => 'Y',
            25 => 'Z',
        );
        $description_price = '';

        foreach ($categories as $v) {
            $description_price .= $v['name'];
            $lists[] = $v['id'];
        }

        // Настромка описания документа
        $objPHPExcel->getProperties()->setCreator($_SERVER['SERVER_NAME'])
            ->setLastModifiedBy("MaZaHaKa")
            ->setTitle("Прайс Avalonian.ru")
            ->setSubject("Прайс Avalonian.ru")
            ->setDescription($description_price)
            ->setKeywords($description_price)
            ->setCategory("Prices");


        // Генерируем контент
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);
        $split_lists = waRequest::get('lists');
        $type = waRequest::get('type');
        $line = 1;
        foreach ($lists as $kl => $vl) {
            $category = $categories[$vl];
            $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(10);
            if (empty($split_lists)) {
                if ($kl == 0) {
                    // Устанавливаем первый лист
                    $objPHPExcel->setActiveSheetIndex(0);
                    $objPHPExcel->getActiveSheet()->setTitle('Весь прайс');
                    if ($type == 0) {
                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, 'Категория');
                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, 'Название');
                        $row_key = 2;
                        foreach ($prices as $v) {
                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[$row_key] . $line, $v->getName());
                            $row_key++;
                        }
                    }
                    $line++;
                }
            } else {
                if ($kl == 0) {
                    // Устанавливаем первый лист
                    $objPHPExcel->setActiveSheetIndex($kl);
                } else {
                    $objPHPExcel->createSheet();
                    $objPHPExcel->setActiveSheetIndex($kl);
                    $objPHPExcel->getActiveSheet()->setTitle($category['name']);
                }
                $line = 1;
                if ($type == 0) {
                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, 'Категория');
                    $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, 'Название');
                    $row_key = 2;
                    foreach ($prices as $v) {
                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[$row_key] . $line, $v->getName());
                        $row_key++;
                    }
                }
                $line++;
                $objPHPExcel->getActiveSheet()->setTitle($category['name']);

            }
            $products = $Pricing->getCategoryProducts($vl);
            if (!empty($products)) {
                foreach ($products as $product) {
                    $offers = $product->getSkus();
                    if ($type == 0) {
                        foreach ($offers as $offer) {
                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, $category['name']);
                            $name = $offer->getProduct()->getName() . ' ' . $offer->getName();
                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, $name);
                            $row_key = 2;
                            foreach ($prices as $v) {
                                $price = $offer->getPrice($v['id']);
                                $objPHPExcel->getActiveSheet()->setCellValue($rows_name[$row_key] . $line, $price->calculate());
                                $row_key++;
                            }
                            $line++;
                        }
                    } else {
                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, 'Категория');
                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, $category['name']);
                        $objPHPExcel->getActiveSheet()->getStyle($rows_name[1] . $line)->getAlignment()->setHorizontal(
                            PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $objPHPExcel->getActiveSheet()
                            ->mergeCells($rows_name[1] . $line . ':' . $rows_name[count($offers)] . $line);
                        $line++;

                        $offer_prices = array();
                        $offer_data = array();
                        foreach ($offers as $offer) {
                            $name = $offer->getName();
                            $offer_data[$offer->getId()] = $name;
                            foreach ($prices as $v) {
                                $price = $offer->getPrice($v->getId());
                                $offer_prices[$v->getId()][$offer->getId()] = $price->calculate();
                            }
                        }
                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, 'Название');
                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[1] . $line, $product->getName());
                        $objPHPExcel->getActiveSheet()->getStyle($rows_name[1] . $line)->getAlignment()->setHorizontal(
                            PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                        $objPHPExcel->getActiveSheet()
                            ->mergeCells($rows_name[1] . $line . ':' . $rows_name[count($offers)] . $line);
                        $line++;

                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, 'Размер');
                        $row_key = 1;
                        foreach ($offer_data as $offer_key => $offer_name) {
                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[$row_key] . $line, $offer_name);
                            $row_key++;
                        }
                        $line++;
                        foreach ($prices as $price_v) {
                            $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, $price_v->getName());
                            $row_key = 1;
                            foreach ($offer_prices[$price_v->getId()] as $offer_key => $of_price) {
                                $objPHPExcel->getActiveSheet()->setCellValue($rows_name[$row_key] . $line, $of_price);
                                $row_key++;
                            }
                            $line++;
                        }
                        $objPHPExcel->getActiveSheet()->setCellValue($rows_name[0] . $line, '');
                        $line++;
                    }
                }
            }
            foreach ($rows_name as $rv) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($rv)->setAutoSize(true);
            }

        }

        //$filename = $_GET['name'].'-'.time().'.xlsx';
        $filename = $_GET['name'] . '.xlsx';
        header("Content-Description: File Transfer");
        header("Pragma: public");
        header("Expires: 0");
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=" . $filename . "");
        header('Cache-Control: max-age=0');
        // Сохраняем в формате Excel 2007
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        // Сохраняем на сервер
        $objWriter->save($filename);
        // Отдаем в браузер
        $objWriter->save('php://output');
    }
}
