<?php

class manufactorySettingsMarginProductSaveController extends waJsonController
{

    public function execute()
    {
        if (waRequest::method() == 'post') {
            $data = waRequest::post();
            $data['name'] = trim($data['name']);
            if (empty($data['name'])) {
                $data['name'] = 'new_pice_' . date("Y-m-d H:i:s");
            }
            if (!empty($data['id'])) {
                $price = manufactoryPricesPool::get($data['id']);
                $id = $price->save($data);
            } else {
                $price = new manufactoryPrice();
                $models_pool = manufactoryModelsPool::getInstance();
                $model = $models_pool->get('MarginProduct');
                $sort = intval($model->select('Max(sort) as max')->fetchField('max'));
                $data['sort'] = ++$sort;
                $id = $price->save($data);

            }
            $this->response = $id;
        }
    }
}
