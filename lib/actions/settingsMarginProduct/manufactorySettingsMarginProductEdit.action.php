<?php

class manufactorySettingsMarginProductEditAction extends waViewAction
{

    public function execute()
    {

        $this->setLayout(new manufactorySettingsLayout());
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);
        $Pricing = new manufactoryPricing();
        $this->view->assign('Pricing', $Pricing);
        $price = false;
        if (!empty($id)) {
            $price = manufactoryPricesPool::get($id);
        }
        $this->view->assign('price', $price);

    }
}
