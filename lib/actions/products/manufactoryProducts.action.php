<?php

class manufactoryProductsAction extends waViewAction
{

    public function execute()
    {
        $this->setLayout(new manufactoryProductsLayout());
        $id = waRequest::get('category', 0, waRequest::TYPE_INT);
        $category = new manufactoryCategoryProducts($id);
        $prices = new manufactoryMarginsProduct();
        $this->view->assign('prices', $prices);
        $this->view->assign('category', $category);

        $def_price = wa()->getUser()->getRights('manufactory', 'contact_price');
        if (array_key_exists($def_price, $prices)) {
            $default_price = $prices[$def_price];
            $this->view->assign('prices', $default_price);
        } else {
            foreach ($prices as $price) {
                if ($price->getType() != 'delimiter') {
                    $default_price = $price;
                }
            }
        }
        $this->view->assign('default_price', $default_price);
        $this->view->assign('category_rights', $category->getRights());
        $this->view->assign('products', $category->getProducts());

    }
}
