<?php

class manufactoryProductsDeleteController extends waJsonController
{


    public function execute()
    {
        $ids = waRequest::get('product_id', array(), waRequest::TYPE_ARRAY);
        foreach ($ids as $id) {
            $product = wa('manufactory')->getConfig()->getFactory('Products')->get($id);

            $product->delete();
        }
        $this->response = $ids;
    }
} 