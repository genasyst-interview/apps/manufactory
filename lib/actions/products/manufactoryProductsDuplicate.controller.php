<?php

class manufactoryProductsDuplicateController extends waJsonController
{


    public function execute()
    {
        $ids = waRequest::get('product_id', array(), waRequest::TYPE_ARRAY);
        $return = array();
        foreach ($ids as $id) {
            $product = wa('manufactory')->getConfig()->getFactory('Products')->get($id);
            $new_id = $product->duplicate();
            $return[$id] = $new_id;
        }
        $this->response = $return;
    }
} 