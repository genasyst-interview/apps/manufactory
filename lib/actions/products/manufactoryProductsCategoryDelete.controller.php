<?php

class manufactoryProductsCategoryDeleteController extends waJsonController
{

    public function execute()
    {
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);

        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CategoryProducts');
        $data = waRequest::post();
        $data['name'] = trim($data['name']);
        if (!empty($id)) {
            $category = new manufactoryCategoryProducts($id);
            if ($category->getRights() < 2) {
                throw new waException('Нет прав на редактирование!');
            }
            $this->deleteCategoryProducts($id);
        } else {
            unset($data['id']);
            $this->response['id'] = $model->insert($data);
        }

    }

    public function deleteCategoryProducts($id)
    {
        $subcategories = manufactoryPricing::getSubCategories($id);
        if (is_array($subcategories) && !empty($subcategories)) {
            foreach ($subcategories as $cat) {
                $this->deleteCategoryProducts($cat['id']);
            }
        }
        $products = manufactoryProducts::getByCategoryId($id);
        if (!empty($products)) {
            foreach ($products as $p) {
                $p->delete();
            }
        }
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CategoryProducts');
        $model->deleteById($id);
    }
}