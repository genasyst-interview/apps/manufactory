<?php

class manufactoryProductsCategoryEditAction extends waViewAction
{

    public function execute()
    {
        $this->setLayout(new manufactoryProductsLayout());
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);
        $parent_id = waRequest::get('parent_id', 0, waRequest::TYPE_INT);
        $category = false;

        $Pricing = new manufactoryPricing();

        $this->view->assign('Pricing', $Pricing);
        if (!empty($id)) {
            $category = new manufactoryCategoryProducts($id);
            if ($category->getRights() < 2) {
                throw new waException('Нет прав на редактирование!');
            }
            $this->view->assign('parent_id', $category['parent_id']);
        } else {
            $category = new manufactoryCategoryProducts(null);
            if (!empty($parent_id)) {
                $category['parent_id'] = $parent_id;
                $parent_category = new manufactoryCategoryProducts($parent_id);
                if ($parent_category->getRights() < 2) {
                    throw new waException('Нет прав на редактирование!');
                }
            }
            $this->view->assign('parent_id', $category['parent_id']);
        }


        $this->view->assign('category', $category);
    }

}
