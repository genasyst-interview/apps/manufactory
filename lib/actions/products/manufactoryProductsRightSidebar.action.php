<?php

class manufactoryProductsRightSidebarAction extends waViewAction
{
    public function execute()
    {
        $this->view->assign('backend_products',
            $this->params['backend_products']
        );

    }
}