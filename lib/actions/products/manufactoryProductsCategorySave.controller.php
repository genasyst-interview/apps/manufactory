<?php

class manufactoryProductsCategorySaveController extends waJsonController
{


    public function execute()
    {
        $id = waRequest::post('id', 0, waRequest::TYPE_INT);

        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('CategoryProducts');
        $data = waRequest::post();
        $data['name'] = trim($data['name']);
        if (!empty($id)) {
            $model->updateById($id, $data);
            $this->response['id'] = $id;
        } else {
            unset($data['id']);
            $id = $model->add($data, $data['parent_id']);
            $model = new waContactRightsModel();
            $model->save(wa()->getUser()->getId(), 'manufactory', 'product_category.' . (string)$id, 2);
            $this->response['id'] = $id;
        }
    }
}