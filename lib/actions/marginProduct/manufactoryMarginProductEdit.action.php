<?php

class manufactoryMarginProductEditAction extends waViewAction
{

    public function execute()
    {

        if (waRequest::get('mobile') == 1) {
            $this->setLayout(new manufactoryMobileLayout());
        } else {
            if (waRequest::isMobile()) {
                $this->setLayout(new manufactoryMobileLayout());
            } else {
                $this->setLayout(new manufactoryDefaultLayout());
            }
        }
        $this->setLayout(new manufactoryDefaultLayout());
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);
        $Pricing = new manufactoryPricing();
        $this->view->assign('Pricing', $Pricing);
        $price = false;
        if (!empty($id)) {
            $price = manufactoryPricesPool::get($id);
        }
        $this->view->assign('price', $price);

    }
}
