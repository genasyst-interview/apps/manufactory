<?php

class manufactoryMarginProductUpdatePriceController extends waJsonController
{

    public function execute()
    {
        if (waRequest::method() == 'post') {
            $id = waRequest::post('id', 0, waRequest::TYPE_INT);
            $price = manufactoryPricesPool::get($id);
            if (!$price) {
                $this->errors[] = 'Неверный ID цены!';

                return;
            }
            $recalc = new manufactoryRecalculatePrices();
            $data = $recalc->recalculatePrice($id, $this->errors);
            if (!empty($this->errors)) {
                return;
            }
            $price->updateDatetime();

            $this->response = 'ok';
        }
    }
}
