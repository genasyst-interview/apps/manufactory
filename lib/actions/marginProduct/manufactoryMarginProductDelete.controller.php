<?php

class manufactoryMarginProductDeleteController extends waJsonController
{

    public function execute()
    {
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);
        $price = manufactoryPricesPool::get($id);
        $price->delete();
    }
}
