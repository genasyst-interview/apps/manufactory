<?php


class manufactorySettingsProductTypeEditAction extends waViewAction
{

    public function execute()
    {
        //$this->setLayout(new manufactorySettingsLayout());
        $id = waRequest::get('id');
        if (intval($id) > 0) {
            $type = manufactoryProductTypes::getInstance()->get($id);
            $this->view->assign('type', $type);
            $this->view->assign('settings', $type->get());
        }

    }
}