<?php


class manufactorySettingsProductTypeCostsAction extends waViewAction
{

    public function execute()
    {
        //$this->setLayout(new manufactorySettingsLayout());
        $costs = wa('manufactory')->getConfig()->getFactory('Costs')->getTypes();
        $this->view->assign('costs', $costs);
    }
}