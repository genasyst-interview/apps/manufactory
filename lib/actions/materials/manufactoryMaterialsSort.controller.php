<?php

class manufactoryMaterialsSortController extends waJsonController
{


    public function execute()
    {
        $id = waRequest::post('id');
        $after_id = waRequest::post('after_id');
        if (!empty($id)) {
            $model = manufactoryModelsPool::getInstance()->get('Material');
            $model->move($id, $after_id);
        }
    }
}