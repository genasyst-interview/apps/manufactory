<?php

class manufactoryMaterialsAction extends waViewAction
{

    public function execute()
    {
        if (waRequest::method() == 'post') {
            $this->sort();
        }
        if (waRequest::get('mobile') == 1) {
            $this->setLayout(new manufactoryMobileLayout());
        } else {
            if (waRequest::isMobile()) {
                $this->setLayout(new manufactoryMobileLayout());
            } else {
                $this->setLayout(new manufactoryDefaultLayout());
            }
        }

        $Pricing = new manufactoryPricing();
        $Pricing->setPrices();
        $this->view->assign('Pricing', $Pricing);


    }

    public function sort()
    {
        $models_pool = manufactoryModelsPool::getInstance();
        $model = $models_pool->get('Material');

        $sort = waRequest::post('sort');
        foreach ($sort as $k => $v) {
            $model->updateById($k, array('sort' => $v));
        }
    }
}
