<?php

class manufactoryFinancialOperationsCashAction extends waViewAction
{
    protected $cashExpenseFTModel = null;
    protected $cashIncomeFTModel = null;
    protected $categoryModel = null;

    protected $start_timestamp = 0;
    protected $end_timestamp = 0;

    public function execute()
    {
        if (waRequest::get('mobile') == 1) {
            $this->setLayout(new manufactoryMobileLayout());
        } else {
            if (waRequest::isMobile()) {
                $this->setLayout(new manufactoryMobileLayout());
            } else {
                $this->setLayout(new manufactoryDefaultLayout());
            }
        }


        $start_date = waRequest::get('start_date');
        $end_date = waRequest::get('end_date');
        if (strtotime($start_date)) {
            $this->start_timestamp = strtotime($start_date);
        }
        if (strtotime($end_date)) {
            $this->end_timestamp = strtotime($end_date);
        }
        $FOClass = new manufactoryFinancialOperations();
        $FOClass->recalculateBalance('income', 'cash');
        $income_data = $FOClass->getPeriodData('income', 'cash');
        $this->view->assign('income_categories', $income_data['categories']);

        $expense_data = $FOClass->getPeriodData('expense', 'cash');
        $this->view->assign('expense_categories', $expense_data['categories']);
        $balances = $FOClass->getBalances('cash');
        $this->view->assign('balances', $balances);

        $contacts = array();
        $FOModel = $FOClass->getFinancialOperationModel('', 'cash');
        foreach ($balances['contacts'] as $id => $v) {
            $contacts[$id]['amount'] = $v['amount'];
            $contacts[$id]['operations'] = $FOModel->getLastContactOperations($id);
        }
        $this->view->assign('contacts', $contacts);

    }
}
