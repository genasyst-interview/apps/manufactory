<?php

class manufactoryFinancialOperationsCategorySaveController extends waJsonController
{
    public function execute()
    {
        $data = waRequest::post();
        $catModel = new manufactoryFinancialOperationsCategoryModel();
        $id = $catModel->save($data, $this->errors);
        if (empty($this->errors)) {
            $category = $catModel->getById($id);
            $FOClass = new manufactoryFinancialOperations($category['financial_operations_type'], $category['financial_operations_payment_type']);
            $category['amount'] = $FOClass->getCategoryAmount($category['id']);
            if ($category['type'] == 'group') {
                $category['categories'] = array();
            }
            $this->response['category'] = $category;
        } else {
            return;
        }


    }
}
