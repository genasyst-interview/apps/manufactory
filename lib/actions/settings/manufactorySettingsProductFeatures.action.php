<?php


class manufactorySettingsProductFeaturesAction extends waViewAction
{

    public function execute()
    {
        $this->setLayout(new manufactorySettingsLayout());
        $Pricing = new manufactoryPricing();
        $id = waRequest::get('id');
        // Вместо всех ставим по умолчанию тип матрасы, типы товаров временно упразднены
        if ($id == 'all' || empty($id)) {
            $id = 1;
        }
        $models_pool = manufactoryModelsPool::getInstance();
        $product_type_model = $models_pool->get('ProductType');
        $product_types = $product_type_model->getAll('id');
        $product_type = false;
        if ($id == 'all' || empty($id)) {
            $features = manufactoryFeaturesPool::getInstance()->getAll();
        } else {
            $product_type = $product_type_model->getById($id);
            $features = $product_type->getFeatures();
        }
        $this->view->assign('Pricing', $Pricing);
        $this->view->assign('product_types', $product_types);
        $this->view->assign('product_type', $product_type);
        $this->view->assign('product_base_types', manufactoryProductTypesBase::getInstance()->get());
        $this->view->assign('features', $features);
    }
}