<?php


class manufactorySettingsProductTypesAction extends waViewAction
{

    public function execute()
    {
        $this->setLayout(new manufactorySettingsLayout());

        $type_id = waRequest::get('type_id', 0, waRequest::TYPE_INT);
        $this->view->assign('type_id', $type_id);

        $this->view->assign('types', manufactoryProductTypes::getInstance()->get());
        $this->view->assign('product_base_types', manufactoryProductTypesBase::getInstance()->get());
    }
}