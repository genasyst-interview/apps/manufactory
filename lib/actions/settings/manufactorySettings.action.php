<?php

class manufactorySettingsAction extends waViewAction
{
    public function execute()
    {
        $this->setLayout(new manufactorySettingsLayout());
    }
}