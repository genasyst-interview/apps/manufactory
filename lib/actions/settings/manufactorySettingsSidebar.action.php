<?php

class manufactorySettingsSidebarAction extends waViewAction
{
    public function execute()
    {
        $params = null;
        $backend_sidebar = wa('manufactory')->event('backend_settings', $params, array('sidebar'));
        $this->view->assign('backend_settings',
            $backend_sidebar
        );
    }
}