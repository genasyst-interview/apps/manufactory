<?php

class manufactoryCostsMaterialDeleteController extends waJsonController
{

    public function execute()
    {
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);
        if (!empty($id)) {
            $material = wa('manufactory')->getConfig()->getFactory('Costs')->get('materials', $id);

            if ($material->delete()) {

            } else {
                $this->errors[] = 'Материал не удален!';
            }
        }
    }
}