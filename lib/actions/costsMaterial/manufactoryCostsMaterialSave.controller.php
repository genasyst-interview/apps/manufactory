<?php

class manufactoryCostsMaterialSaveController extends waJsonController
{


    public function execute()
    {
        $id = waRequest::post('id', 0, waRequest::TYPE_INT);
        if (!empty($id)) {
            $material = new manufactoryCostMaterial($id);
        } else {
            $material = new manufactoryCostMaterial(null);
        }
        $data = waRequest::post();
        $data['name'] = trim($data['name']);
        if (!isset($data['sort'])) {
            $data['sort'] = 0;
        }
        $this->response['id'] = $material->save($data);
    }

}