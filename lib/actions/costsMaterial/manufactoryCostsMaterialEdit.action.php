<?php

class manufactoryCostsMaterialEditAction extends waViewAction
{

    public function execute()
    {
        $this->setLayout(new manufactoryCostsLayout());
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);
        $category = waRequest::get('category', 0, waRequest::TYPE_INT);
        $material = false;

        $Pricing = new manufactoryPricing();
        $this->view->assign('Pricing', $Pricing);
        if (!empty($id)) {
            $material = manufactoryMaterialsPool::get($id);
        }
        $this->view->assign('category', $category);
        $this->view->assign('material', $material);

    }
}
