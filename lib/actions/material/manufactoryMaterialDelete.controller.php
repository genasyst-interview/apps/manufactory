<?php

class manufactoryMaterialDeleteController extends waJsonController
{

    public function execute()
    {
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);
        if (!empty($id)) {
            $material = manufactoryMaterialsPool::get($id);

            if ($material->delete()) {

            } else {
                $this->errors[] = 'Материал не удален!';
            }
        }
    }
}