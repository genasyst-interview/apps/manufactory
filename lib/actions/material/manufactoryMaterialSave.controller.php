<?php

class manufactoryMaterialSaveController extends waJsonController
{


    public function execute()
    {
        $id = waRequest::post('id', 0, waRequest::TYPE_INT);
        if (!empty($id)) {
            $material = new manufactoryMaterial($id);
        } else {
            $material = new manufactoryMaterial();
        }
        $data = waRequest::post();
        $data['name'] = trim($data['name']);
        if (!isset($data['sort'])) {
            $data['sort'] = 0;
        }
        $sizes_price = $_POST['sizes_price'];
        $data['sizes_price'] = serialize($sizes_price);
        $this->response['id'] = $material->save($data);
    }
}