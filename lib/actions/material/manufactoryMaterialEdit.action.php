<?php

class manufactoryMaterialEditAction extends waViewAction
{

    public function execute()
    {
        if (waRequest::get('mobile') == 1) {
            $this->setLayout(new manufactoryMobileLayout());
        } else {
            if (waRequest::isMobile()) {
                $this->setLayout(new manufactoryMobileLayout());
            } else {
                $this->setLayout(new manufactoryDefaultLayout());
            }
        }
        $id = waRequest::get('id', 0, waRequest::TYPE_INT);
        $category = waRequest::get('category', 0, waRequest::TYPE_INT);
        $material = false;

        $Pricing = new manufactoryPricing();
        $this->view->assign('Pricing', $Pricing);
        if (!empty($id)) {
            $material = manufactoryMaterialsPool::get($id);
        }
        $this->view->assign('category', $category);
        $this->view->assign('material', $material);


    }
}
