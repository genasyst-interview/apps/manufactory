<?php

class manufactoryFeatureEditAction extends waViewAction
{


    public function execute()
    {
        $this->setLayout(new manufactoryProductsLayout());
        $id = waRequest::get('id');
        $feature = manufactoryFeaturesPool::get($id);
        $this->view->assign('feature', $feature);
        $models_pool = manufactoryModelsPool::getInstance();
        $product_type_model = $models_pool->get('ProductType');
        $product_types = $product_type_model->getAll('id');
        $this->view->assign('product_types', $product_types);
    }
}