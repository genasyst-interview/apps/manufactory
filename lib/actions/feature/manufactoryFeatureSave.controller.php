<?php

class manufactoryFeatureSaveController extends waJsonController
{


    public function execute()
    {
        $data = waRequest::post();
        $models_pool = manufactoryModelsPool::getInstance();

        $feature_model = $models_pool->get('Feature');
        $feature_value_model = $models_pool->get('FeatureValue');
        $feature_id = waRequest::post('id', 0, waRequest::TYPE_INT);
        $feature_data = array(
            'name' => $data['name'],
            'unit' => $data['unit'],
            'type' => $data['type'],
        );
        if (!empty($feature_id)) {
            $feature_model->updateById($feature_id, $feature_data);
        } else {
            $feature_id = $feature_model->insert($feature_data);
        }


        if (!empty($feature_id)) {
            $product_types = $data['product_types'];
            $types_features_model = $models_pool->get('ProductTypesFeatures');
            $types_features_model->deleteByField('feature_id', $feature_id);
            if (!empty($product_types) && is_array($product_types)) {
                foreach ($product_types as $id) {
                    $type_data = array(
                        'feature_id' => $feature_id,
                        'type_id'    => $id,
                    );
                    $types_features_model->insert($type_data);
                }
            }
            $values = $data['values'];

            foreach ($values as $id => $v) {
                $value = array(
                    'unit'     => $v['unit'],
                    'value'    => $v['value'],
                    'name'     => $v['name'],
                    'group_id' => $v['group_id'],
                    'sort'     => $v['sort'],
                );

                if (preg_match('/new/', $id)) {
                    $value['feature_id'] = $feature_id;
                    $feature_value_model->insert($value);
                } else {
                    $feature_value_model->updateById($id, $value);
                }
            }
        }
        $this->response['id'] = $feature_id;

    }
}